(function ($) {
  $(document).ready(function () {
    $('.percent-value').each(function () {
      var value = $(this).attr('data-value');
      $(this).animate({ width: value }).text(value);
    });
  });

  var AppAnalitics = {
    init: function() {
      this.eventButtons();
    },

    eventButtons: function () {
      var me = this;
      $(document).on('click', '.analytics-open', function () {
        AppUtil.animationClick($(this), 'flash', function () {
          me.show();
          $(document).trigger('OnShowAnalytics');
        });
      });


      $(document).on('click', '.analytics-close', function () {
        AppUtil.animationClick($(this), 'flash', function () {
          me.hide();
          $(document).trigger('OnHideAnalytics');
        });
      });
    },

    show: function () {
      $('.analytics-panel').show();
      $('.analytics-open').hide();
      $('.analytics-close').show();
    },

    hide: function () {
      $('.analytics-panel').hide();
      $('.analytics-close').hide();
      $('.analytics-open').show();
    }
  };
  AppAnalitics.init();
})(jQuery);