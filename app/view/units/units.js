(function($) {


  var unitItem =
    '<li class="list-group-item unit-item" data-index-unit="{indexUnit}" data-percent="{percent}">' +
      '<div class="row align-items-center h-100">' +
        '<div class="col col-2">' +
          '<img class="thumb" src="images/{thumb}">' +
        '</div>' +
        '<div class="col col-7 pl-4 col-title">' +
          '<span class="label-name theme-destacados">{name}</span>' +
          '<span class="label-title theme-text-medium">{title}</span>' +
        '</div>' +
        '<div class="col col-3 col-buttons text-right">' +
          '<div class="button-circle-unit svg-button check"><i data-feather="check"></i></div>' +
          '<div class="button-circle-unit svg-button percent">{percent}%</div>' +
          '<div class="button-circle-unit svg-button expand" data-index-unit="{indexUnit}"><i data-feather="chevron-down"></i></div>' +
          '<div class="button-circle-unit svg-button collapse" data-index-unit="{indexUnit}"><i data-feather="chevron-up"></i></div>' +
        '</div>' +
      '</div>' +
    '</li>';

  var unitEpigraphItem =
    '<li class="list-group-item epigraph" data-id-book="{idBook}" data-index-unit="{indexUnit}" data-index-epigraph="{indexEpigraph}">' +
      '<div class="row align-items-center h-100">' +
        '<div class="col col-12">' +
          '<span class="theme-list-dot">{number}</span><span class="theme-text-medium">{title}</span>' +
          '<span class="svg-button float-right"><i class="theme-text-medium" data-feather="arrow-right"></i></span>' +
        '</div>' +
      '</div>' +
    '</li>';

  var unitButtons =
    '<div class="unit-buttons row text-center color-primary" data-index-unit="{indexUnit}">' +
      '<div class="col col-4 d-inline-block text-right"><i data-feather="image"></i><span> Recursos</span></div>' +
      '<div class="col col-auto d-inline-block text-center"><i data-feather="loader"></i><span> Actividades</span></div>' +
      '<div class="col col-4 d-inline-block text-left"> <i data-feather="file-minus"></i><span> Apuntes</span></div>' +
    '</div>';

  var idBook = null;
  var Library = {
    init: function (theLibrary) {
      if (theLibrary.content && theLibrary.content.length) {
        $('.units .intro').html(theLibrary.content);
      }

      var $unitsList = $('.units-list');
      var html = '';

      idBook = theLibrary.id;
      $.each(theLibrary.elems, function (index, elem) {
        var idUnit = elem.id;
        var indexUnit = index + 1;
        html +='<ul class="list-group" data-index-unit="' + indexUnit + '">';
        html += unitItem
          .replace('{name}', 'Unidad' + (index + 1))
          .replace('{thumb}', elem.thumb)
          .replace('{title}', elem.title)
          .replace(/{percent}/g, elem.percent)
          .replace(/{indexUnit}/g, (indexUnit));

        var number = 1;
        $.each(elem.elems, function (index, elem) {
          html += unitEpigraphItem
            .replace('{number}', number++)
            .replace('{title}', elem.title)
            .replace(/{idBook}/g, idBook)
            .replace(/{indexUnit}/g, (indexUnit))
            .replace(/{indexEpigraph}/g, index);
        });
        html += unitButtons.replace(/{indexUnit}/g, (indexUnit));
        html +='</ul>';
      });
      $unitsList.append($(html));
      feather.replace();
    }
  };

  $(document).ready(function () {
    $('.btn.start').click(function () {
      $(document).trigger('OnShowView', '.view.unit');
      $(document).trigger('showEpigraph', {
        idBook: idBook,
        unit: 1,
        epigraph: 0
      })
    });

    $(document).on('click', '.units .button-circle-unit.expand', function () {
      var indexUnit = $(this).attr('data-index-unit');
      $('[ data-index-unit="' + indexUnit + '"]').addClass('expanded');
    });

    $(document).on('click', '.units .button-circle-unit.collapse', function () {
      var indexUnit = $(this).attr('data-index-unit');
      $('[ data-index-unit="' + indexUnit + '"]').removeClass('expanded');
    });

    $(document).on('click', '.list-group-item.epigraph', function () {
      var indexBook = $(this).attr('data-id-book');
      var indexUnit = parseInt($(this).attr('data-index-unit'));
      var indexEpigraph = parseInt($(this).attr('data-index-epigraph'));
      $(document).trigger('OnShowView', '.view.unit');
      $(document).trigger('showEpigraph', {
        idBook: indexBook,
        unit: indexUnit,
        epigraph: indexEpigraph
      })
    });

    $(document).on('OnShowAnalytics', function () {
      $('.units-list').hide();
    });

    $(document).on('OnHideAnalytics', function () {
      $('.units-list').show();
    });

    $('.header .thumb').css('background-image', 'url(./images/' + library[0].thumb + ')');
    $('.header .title').text(library[0].title);
    $('.header .level').text(library[0].level);

    Library.init(library[0]);
  });



})(jQuery);