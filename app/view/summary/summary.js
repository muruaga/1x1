(function ($) {

    var position = {
        idBook: '',
        book:0,
        unit: 0,
        epigraph: 0
    };
    $(document).on('showEpigraph', function (event, obj) {
        initSummary(obj.idBook, obj.unit, obj.epigraph);
    });
    $(document).ready(function () {
        feather.replace();
    });

    $(document).on('ShowSummary', function() {
        var idEpigraph = library[position.book].elems[position.unit - 1].elems[position.epigraph].id;
        setTimeout(function() {
            document.getElementById('summ-' + idEpigraph).scrollIntoView({block: 'start', behavior: 'smooth'});
        },500);
    });


    function initSummary (idBook, unit, epigraph) {
        if (position.idBook !== idBook || position.unit !== unit || position.epigraph !== epigraph) {
            if (position.idBook !== idBook) {
                var $summary = $('#summary-content');
                $summary.empty();
                for (var i = 0; i < library.length; i ++) {
                    if (library[i].id === idBook) {
                        position.book = i;
                        position.idBook= idBook;
                        $('.think').text(library[i].title);
                        for (var j = 0; j < library[i].elems.length; j++) {
                            $summary.append('<h3 id="summ-' + library[i].elems[j].id + '"> Unidad ' + (j+1) + ': ' + library[i].elems[j].title + '</h3>');
                            for (var k = 0; k < library[i].elems[j].elems.length; k++) {
                                $summary.append('<h4 id="summ-' + library[i].elems[j].elems[k].id + '">' + library[i].elems[j].elems[k].title
                                    + '<button class="button-summary" id="btn-summ-' + library[i].elems[j].elems[k].id + '"><i data-feather="arrow-left-circle"></i></button></h4>');
                                if (typeof(library[i].elems[j].elems[k].summary)!== 'undefined') {
                                    $summary.append(library[i].elems[j].elems[k].summary);
                                }
                                openEpigraph($('#btn-summ-' + library[i].elems[j].elems[k].id), idBook, j+1, k);
                            }
                        }
                        break;
                    }
                }
                feather.replace();
            }
            position.unit = unit;
            position.epigraph = epigraph;
        }
    }

    function openEpigraph($button, idBook, unit, epigraph) {
        $button.click(function(e) {
            $(document).trigger('OnShowView', '.view.unit');
            $(document).trigger('showEpigraph', {idBook: idBook, unit: unit, epigraph: epigraph});
        });
    }
})(jQuery);