(function ($) {
  var $main = null;

  $(document).ready(function () {
    $main = $('.view.note main');
    loadNotes();
  });

  $(document).on('click', '.card', function () {
    showNote($(this).data('note'));
  });

  $(document).on('click', '#NewNote', function () {
    var note = {
      id: '',
      bookmark: 1,
      title: 'Título del apunte',
      subtitle: 'Subtítulo del apunte',
      html: ''
    };
    $(document).trigger('OnShowView', ['.view.note-editor', note]);
  });

  $(document).on('OnShowView', function (event, selector) {
    if (selector === '.view.note') {
      loadNotes();
    }
  });

  $(document).on('showEpigraph', function (event, obj) {
    window.userSelection = {
      idBook: obj.idBook, unit:obj.unit, epigraph:obj.epigraph
    };
  });

  function loadNotes() {
    var cardNote =
      '<div class="card">' +
        '<div class="card-header">{1}</div>' +
        '<div class="card-body">{2}</div>' +
        '<div class="card-footer">{3}<span class="shared-icon"><i data-feather="share-2"></i></span></div>' +
      '</div>';



    $main.html('');

    $.each(library, function (index, unit) {
      $.each(unit.elems[0].elems, function (index, epigraph) {
        if (typeof epigraph.notes === 'object') {
          $.each(epigraph.notes, function (indexNote, note) {
            note.unitNum = unit.num;
            note.epigraph = epigraph;
            var $card =
              $(cardNote.replace('{1}', getHTMLHeader(note.unitNum))
                .replace('{2}', getHTMLBody(note))
                .replace('{3}', getHTMLFooter()));
            $card.data('note', note);
            $main.append($card);
          });
        }
        feather.replace();
      });
    });
  }

  function getHTMLHeader(note) {
    var unit = library[0];
    return '<span class="note-type note-' + note.bookmark + '">'  + '</span>' +
           '<span class="note-unit">Unidad ' + unit.num + '</span>';
  }

  function getHTMLBody(note) {
    return '<h2 class="note-title">' + note.title + '</h2>' +
           '<div class="note-html">' + note.subtitle + '</div>';
  }

  function getHTMLFooter() {
    return '<p class="note-date-label">Última actualización</p>' +
           '<p class="note-date-value">Miércoles 30 nombiembre</p>';
  }

  var selectedNode = null;
  function showNote(note) {
    selectedNode = note;
    $(document).trigger('OnShowView', ['.view.note-editor', selectedNode]);
  }

})(jQuery);