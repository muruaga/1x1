(function ($) {

  $(document).ready(function () {
    $('.note-editor').on('scroll', function () {
      if (window.editor !== null) {
        setTimeout(function () {
          $('medium-editor-toolbar').fadeOut();
          $(document).trigger('click');
        }, 100);
      }
    });
  });

  $(document)
    .on('OnShowView', function (event, selector, note) {
      if (selector === '.view.note-editor') {
        initEditor(note);
      }
    })
    .on('click', '.medium-insert-images', function () {
      featherReplace();
    })
    .on('click', '.medium-insert-embeds', function () {
      featherReplace();
    })
    .on('click', '.medium-insert-buttons-show', function () {
      featherReplace();
    })
    .on('click', '.medium-insert-action', function () {
      featherReplace();
    }).on('click', '.editor-toggle', function () {
      editorToggle();
    });

  function featherReplace() {
    setTimeout(function () {
      feather.replace();
    }, 100)

  }

  window.editorEnable = false;

  function editorToggle() {
    editorEnable = !editorEnable;
    if (editorEnable) {
     editor.setup();
    }
    else {
      editor.destroy()  ;
    }
  }

  function clearEditor() {
    $('.editor-unit').text('');
    $('.editor-title').text('');
    $('.editor-subtitle').text('');

    runEditor('');
  }

  function initEditor(note) {
    $('.editor-unit').text('Unidad ' + note.unitNum);
    $('.editor-title').text(note.title);
    $('.editor-subtitle').text(note.subtitle);

    runEditor(note.html);
  }

  window.editor = null;
  window.$editorText = null;
  function runEditor(html) {
    if (window.editor !== null) {
      window.editor.destroy();
      window.$editorText.remove();
    }
    window.$editorText = $('<div class="editor-text" data-ph="Texto del apunte">' + html + '</div>');
    window.$editorText.appendTo('.editor-content');

    window.editor = new MediumEditor('.editor-text', {
      toolbar: {
        /* These are the default options for the toolbar,
           if nothing is passed this is what is used */
        allowMultiParagraphSelection: true,
        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
        diffLeft: 0,
        diffTop: -10,
        firstButtonClass: 'medium-editor-button-first',
        lastButtonClass: 'medium-editor-button-last',
        relativeContainer: null,
        standardizeSelectionStart: false,
        static: false,
        /* options which only apply when static is true */
        sticky: false,
        updateOnEmptySelection: false
      },
      placeholder: {
        /* This example includes the default options for placeholder,
           if nothing is passed this is what it used */
        text: 'Texto del apunte',
        hideOnClick: true
      }
    });

    window.$editorText.mediumInsert({
      editor: window.editor,
      captionPlaceholder: 'Escriba el título para imagen (opcional)',
      addons: { // (object) Addons configuration
        images: {
          label: '<i data-feather="image"></i>',
          styles: {
            wide: {
              label: '<i data-feather="align-justify"></i>'
            },
            left: {
              label: '<i data-feather="align-left"></i>'
            },
            right: {
              label: '<i data-feather="align-right"></i>'
            },
            grid: {
              label: '<i data-feather="grid"></i>'
            }
          },
          actions: {
            remove: {
              label: '<i data-feather="trash-2"></i>'
            }
          },
          messages: {
            acceptFileTypesError: 'Este archivo no tiene un formato compatible: ',
            maxFileSizeError: 'Este archivo es demasiado grande: '
          }
        },
        embeds: {
          label: '<i data-feather="youtube"></i>',
          placeholder:'Pegue un enlace de YouTube, Vimeo, Facebook, Twitter o Instagram y presione Entrar',
          captionPlaceholder: 'Escriba el subtítulo (opcional)',
          styles: {
            wide: {
              label: '<i data-feather="align-justify"></i>'
            },
            left: {
              label: '<i data-feather="align-left"></i>'
            },
            right: {
              label: '<i data-feather="align-right"></i>'
            },
            grid: {
              label: '<i data-feather="grid"></i>'
            }
          },
          actions: {
            remove: {
              label: '<i data-feather="trash-2"></i>'
            }
          }
        }
      }
    });
  }


})(jQuery);