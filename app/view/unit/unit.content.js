(function($) {

  $(document).ready(function () {

    var activityIframes = [];
    var currentObj = {};
    var epigraphFound = null;

    var sectionIntroHTML =
      '<div class="sm-section intro">' +
        '<div class="sm-section-header">' +
          '<div class="sm-section-options">' +
            '<span class="button-circle-unit sm-content-eye"><i data-feather="eye"></i></span>' +
            '<span class="button-circle-unit sm-content-eye-off" style="display: none;"><i data-feather="eye-off"></i></span>' +
          '</div>' +
        '</div>{$content}' +
      '</div>';

    var sectionHTML =
      '<div id="{$id}" class="sm-section sm-section-content">' +
        '<div class="sm-section-header">' +
          '<div class="sm-section-options">' +
            '<span class="button-circle-unit sm-content-eye"><i data-feather="eye"></i></span>' +
            '<span class="button-circle-unit sm-content-eye-off" style="display: none;"><i data-feather="eye-off"></i></span>' +
          '</div>' +
        '</div>' +
        '<div class="sm-section-subtitle">' +
          '<span class="sm-minidot"></span>' +
          '<h2>{$title}</h2>' +
        '</div>{$content}' +
      '</div>';

    var sectionOwnerHTML =
      '<div id="{$id}" class="sm-section sm-owner-section">' +
        '<div class="sm-section-header">' +
          '<div class="sm-teacher">' +
          '<span class="sm-teacher-photo"><i data-feather="user"></i></span>' +
          'Contenido del profesor' +
          '</div>' +
          '<div class="sm-section-options">' +
            '<span class="button-circle-unit sm-content-edit"><i data-feather="edit-3"></i></span>' +
            '<span class="button-circle-unit sm-content-eye"><i data-feather="eye"></i></span>' +
            '<span class="button-circle-unit sm-content-eye-off" style="display: none;"><i data-feather="eye-off"></i></span>' +
            '<span class="button-circle-unit sm-content-remove"><i data-feather="trash-2"></i></span>' +
          '</div>' +
        '</div>' +
        '<div class="editable-title" data-ph="Título sub-epígrafe">{$title}</div>' +
        '<div class="editable-content">{$content}</div>' +
      '</div>';

    $(document).on('toggleSidebar', function () {
      for (var i = 0; i < activityIframes.length; i++) {
        initializeActivity($('#' + activityIframes[i].id), activityIframes[i].id, activityIframes[i].minHeight);
      }
    });

    $(document).on('showEpigraph', function (event, obj) {
      $('.unit-content-container .stop-edit').fadeOut();
      $('.unit-content-container .start-edit').fadeIn();
      $('.unit-content-container').removeClass('editing');
      $('.sm-owner-section-creator').remove();

      currentObj = obj;
      var $unitContentContainer = $('.unit-content-container');
      $unitContentContainer.animate({
        opacity: 0,
        scrollTop: 0
      }, 250, function() {
        showEpigraph(obj.idBook, obj.unit, obj.epigraph);
        $unitContentContainer.animate({
          opacity: 1
        }, 250);
      });
    });

    $(document).on('showSection', function (event, id) {
      document.getElementById(id).scrollIntoView({block: 'start', behavior: 'smooth'});
    });

    $(document).on('show.bs.collapse shown.bs.collapse hidden.bs.collapse', '.sm-accordion', function () {
      var $accordion = $(this);
      $accordion.find('.card').removeClass('show');
      $accordion.find('.collapse.show').parent().addClass('show');
    });

    $(document).on('click', '.sm-accordion .sm-header', function () {
      var $accordion = $(this).parent();
      $accordion.find('.card').removeClass('show');
      $accordion.find('.collapse.show').parent().addClass('show');
    });

    $(window).resize(function() {
      for (var i = 0; i < activityIframes.length; i ++) {
        var $div = $('#' + activityIframes[i].id);
        if (typeof(activityIframes[i].minHeight) !== 'undefined') {
          if (activityIframes[i].minHeight.slice(-2) === 'px') {
            $div.css('height', activityIframes[i].minHeight);
          } else {
            var percent = Number(activityIframes[i].minHeight.slice(0,-1));
            $div.css('height', parseInt((percent * $div.width())/100) + 'px');
          }
        }
      }
    });

    $(document).on('click', '.unit-content-container .start-edit', function () {
      $('.unit-content-container .start-edit').fadeOut();
      $('.unit-content-container .stop-edit').fadeIn();
      $('.unit-content-container').addClass('editing');

      $('.sm-section:not(.intro)').each( function (index) {
        var $section = $(this);
        if (index === 0) {
          insertCreatorSection($section, 'before');
        }
        insertCreatorSection($section, 'after');
      });

      setTimeout(function () {
        feather.replace();
      }, 100);

      $(document).trigger('OnStartEditUnit');
    });

    $(document).on('click', '.unit-content-container .stop-edit', function () {
      $('.unit-content-container .stop-edit').fadeOut();
      $('.unit-content-container .start-edit').fadeIn();
      $('.unit-content-container').removeClass('editing');
      $('.sm-owner-section-creator').remove();

      $('.sm-owner-section .medium-editor-element').each(function () {
        var $section = $(this);
        stopSectionEditor($section);
      });
      $(document).trigger('initTableContents', currentObj);
      $(document).trigger('OnStopEditUnit');
    });


    $(document).on('click', '.add-content', function () {
      var $sectionBefore = $(this).parent('.sm-owner-section-creator');
      var index = $sectionBefore.index();
      var newSection = {
        id: 'owner-section-' + Math.random().toString(36).slice(2),
        title: '',
        type: 'sectionOwner',
        isOwner: true,
        minutes: 0,
        content: ''
      };
      var epigraphContent = (typeof (epigraphFound.content) === 'undefined' ? 0 : 1);
      epigraphFound.elems.splice((index/2) - epigraphContent, 0, newSection);
      var $newSection = appendNewOwnerSection($sectionBefore, newSection);
      $newSection.data('elem', newSection);
      insertCreatorSection($newSection, 'after');
      feather.replace();

      saveSection($newSection);

      $(document).trigger('initTableContents', currentObj);
    });


    $(document).on('click', '.sm-content-edit', function () {
      var $section = $(this).parents('.sm-owner-section');
      if (!$section.hasClass('edit')) {
        startSectionEditor($section);
      }
      else {
        stopSectionEditor($section);
      }
    });

    $(document).on('input change', '[contenteditable]', function () {
      var $section = $(this).parents('.sm-section');
      saveSection($section);
      $(document).trigger('initTableContents', currentObj);
    });


    function saveSection($section) {
      var elem = $section.data('elem');
      if (typeof elem === 'object') {
        elem.title =  $section.find('.editable-title').html();
        if (!elem.title.length) {
          elem.title = 'Sin título';
        }
        var editor = $section.data('editor');
        if (typeof editor === 'object') {
          $section.find('.medium-insert-buttons').remove();
          elem.content = editor.getContent();
        }
      }
    }


    function startSectionEditor($section) {
      $section.addClass('edit');
      editor = EditoRunner.init($section.find('.editable-content')[0]);
      $section.data('editor', editor);
      $section.find('.editable-title').attr('contenteditable', true).focus();
    }


    function stopSectionEditor($section) {
      $section.removeClass('edit');
      saveSection($section);
      var editor = $section.data('editor');
      if (typeof editor === 'object') {
        editor.destroy();
      }
      $section.find('.editable-title').attr('contenteditable', false);
    }


    $(document).on('click', '.sm-content-eye, .sm-content-eye-off', function () {
      var $owner = $(this).parents('.sm-section');
      $owner.toggleClass('hidden');
      for (var i = 0; i < epigraphFound.elems.length; i++) {
        if (epigraphFound.elems[i].id === $owner.attr('id')) {
          epigraphFound.elems[i].visible = !$owner.hasClass('hidden');
          break;
        }
      }
    });


    $(document).on('click', '.sm-content-remove', function () {
      var $section = $(this).parents('.sm-section');
      var $next = $section.next();
      if ($next.hasClass('sm-owner-section-creator')) {
        $next.remove();
      }
      var index = $section.index();
      epigraphFound.elems.splice(index/2 - 1, 1);
      $(document).trigger('initTableContents', currentObj);
      $section.remove();
    });


    function appendNewOwnerSection($sectionBefore, sectionData) {
      var $newSection = $(
        sectionOwnerHTML
          .replace('{$id}', sectionData.id)
          .replace('{$title}', sectionData.title)
          .replace('{$content}', sectionData.content));
      $sectionBefore.after($newSection);
      return $newSection;
    }


    function insertCreatorSection($section, where) {
      var $newSection = $(
        '<div class="sm-owner-section-creator">' +
          '<span class="add-content button-circle-unit"><i data-feather="plus"></i></span>' +
        '</div>');

      if (where === 'after') {
        $section.after($newSection);
      }
      else {
        $section.before($newSection);
      }
      return $newSection;
    }


    function handleResponse(e) {
      if (e.data === '' || e.data === '{"ready":true}') return;
      var responseParams = e.data.split('|');
      var $iframe = $('#' + responseParams[0]);
      var newHeight = Number(responseParams[1]);
      for (var i = 0; i < activityIframes.length; i++) {
        if (activityIframes[i].id === responseParams[0]) {
          var minHeight = 0;
          if (typeof(activityIframes[i].minHeight) !== 'undefined') {
            if (activityIframes[i].minHeight.slice(-2) === 'px') {
              minHeight = Number(activityIframes[i].minHeight);
            } else {
              var percent = Number(activityIframes[i].minHeight.slice(0,-1));
              minHeight = ((percent * $iframe.width())/100);
            }
          }
          if (minHeight < newHeight) {
            $iframe.css('height', newHeight + 'px');
          }
        }
      }
    }

    function initializeActivity($iframe, id, minHeight) {
      if (typeof(minHeight) !== 'undefined') {
        if (minHeight.slice(-2) === 'px') {
          $iframe.css('height', minHeight);
        } else {
          var percent = Number(minHeight.slice(0,-1));
          $iframe.css('height', parseInt((percent * $iframe.width())/100) + 'px');
        }
      }
      $iframe.on('load', function(){
        this.contentWindow.postMessage('id:' + id, '*');
      });
    }

    function processEmbeds (elem) {
      if (typeof(elem.embed) === 'undefined' || elem.embed.length === 0) return elem.content;
      var processedContent = elem.content;
      var html = '';
      for (var i = 0; i < elem.embed.length; i ++) {
        var embed = elem.embed[i];
        switch (embed.type) {
          case 'activity':
          case 'interactive':
            activityIframes.push(embed);
            var addOptions = '';
            if (typeof(embed.header) !== 'undefined') {
              addOptions += '&header=' +  encodeURIComponent(embed.header);
            }
            if (typeof(embed.subheader) !== 'undefined') {
              addOptions += '&subheader=' +  encodeURIComponent(embed.subheader);
            }
            if (typeof(embed.buttonText) !== 'undefined') {
              addOptions += '&buttonText=' +  encodeURIComponent(embed.buttonText);
            }
            if (typeof(embed.icon) !== 'undefined') {
              addOptions += '&icon=' +  encodeURIComponent(embed.icon);
            }
            if (typeof(embed.linkText) !== 'undefined') {
              addOptions += '&linkText=' +  encodeURIComponent(embed.linkText);
            }
            html = '<div class="iframe-parent"><iframe id="' +embed.id + '" src="activity.html?url=' +
                encodeURIComponent(embed.url) + '&type=' + embed.type + addOptions + '" frameborder="0" scrolling="no" allowfullscreen></iframe></div>';
            break;
          case 'image':
              html = '<a class="sm-image" id="' + embed.id + '" href="' + embed.url + '" data-featherlight="image"><img alt="" title="" src="' + embed.url + '"></a>';
            break;
          case 'video':
            html =
              '<div class="sm-video" id="' + embed.id + '">' +
                '<video id="video-' + embed.id + '">' +
                  '<source type="video/mp4" src="' + embed.url + '">' +
                '</video>' +
              '</div>';
            break;
          case 'html-activity':
            html = '<div id="' + embed.id + '" class="sm-html-activity"><span class="sm-html-activity-icon"><i data-feather="loader"></i></span>' + embed.content + '</div>';
            if (embed.elems) {
              $(embed.elems).each(function (index, el) {
                html = html.replace('{' + index + '}', el.url);
              });
            }
            break;
          case 'accordion':
            html = '<div id="' + embed.id + '" class="sm-accordion accordion">';
            if (embed.elems) {
              $(embed.elems).each(function (index, el) {
                var tabID = embed.id + '-tab-' + index;
                var collapseID = embed.id + '-collapse-' + index;
                html +=
                  '<div class="card">' +
                    '<div class="card-header" role="tab" id="' + tabID + '">' +
                      '<a data-toggle="collapse" data-parent="' + embed.id  + '" href="#' + collapseID + '"  aria-expanded="true" aria-controls="'+ collapseID + '">' +
                        el.title +
                      '<span class="sm-html-activity-icon"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                    '</div>' +
                    '<div id="' + collapseID + '" class="collapse" role="tabpanel" aria-labelledby="' + tabID + '" data-parent="#' + embed.id + '">' +
                      '<div class="card-body">' + el.content + '</div>' +
                    '</div>' +
                  '</div>';
              });
            }
            html += '</div>';
            break;
        }
        //html = '<div class="sm-resource">' + html + '</div>';
        processedContent = processedContent.replace('{' + i + '}', html);
      }
      return processedContent;
    }

    function showEpigraph (idBook, unit, epigraph) {
      activityIframes = [];

      var $unitContent = $('.unit-content');
      for (var i = 0; i < library.length; i++) {
        if (library[i].id === idBook && typeof(library[i].elems[unit -1]) !== 'undefined'
            && typeof(library[i].elems[unit -1].elems[epigraph]) !== 'undefined') {
          epigraphFound = library[i].elems[unit -1].elems[epigraph];
          break
        }
      }
      if (epigraphFound !== null) {
        var $sectioHTML;
        var elem = {};
        var contentHtml = '';
        var hasPrev = false;
        var hasNext = false;
        $unitContent.empty();

        var titleHtml =
          '<div id="' + epigraphFound.id + '"> <div class="sm-section-title intro">' +
            '<span class="sm-dot">' + epigraphFound.num + '</span>' +
            '<h2>' + epigraphFound.title + '</h2>' +
          '</div></div>';
        $unitContent.append(titleHtml);
        elem = epigraphFound;

        if (elem.content) {
          contentHtml = processEmbeds(elem);
          $sectioHTML = $(sectionIntroHTML
            .replace('{$content}', contentHtml));
          $unitContent.append($sectioHTML);
        }

        var questions = [];
        for (var j = 0; j < epigraphFound.elems.length; j++) {
          elem = epigraphFound.elems[j];

          contentHtml = processEmbeds(elem);

          if (elem.type === 'sectionOwner') {
            $sectioHTML = $(sectionOwnerHTML
              .replace('{$id}', elem.id)
              .replace('{$title}', elem.title)
              .replace('{$content}', contentHtml));
          }
          else {
            $sectioHTML = $(sectionHTML
              .replace('{$id}', elem.id)
              .replace('{$title}', elem.title)
              .replace('{$content}', contentHtml));
          }
          $unitContent.append($sectioHTML);

          if (elem.learnosity_data) questions = questions.concat(elem.learnosity_data);
          $sectioHTML.data('elem', elem);
        }
        var buttonHtml = '';
        if (epigraph > 0) {
          buttonHtml += '<div id="btn-prev" class="btn-end-unit col"><p><span><i data-feather="arrow-left"></i></span>Anterior</p><h3>' +
            library[i].elems[unit -1].elems[epigraph - 1].title + '</h3></div>';
          hasPrev = true;
        } else {
          buttonHtml += '<div id="btn-prev" class="btn-end-unit col btn-blank"></div>';
        }
        buttonHtml += '<div id="btn-index" class="btn-end-unit col"><h3>Índice</h3></div>';
        if (typeof(library[i].elems[unit -1].elems[epigraph + 1]) !== 'undefined') {
          buttonHtml += '<div id="btn-next" class="btn-end-unit col"><p>Siguiente<span><i data-feather="arrow-right"></i></span></p><h3>' +
            library[i].elems[unit -1].elems[epigraph + 1].title + '</h3></div>';
          hasNext = true;
        } else {
          buttonHtml += '<div id="btn-next" class="btn-end-unit col btn-blank"></div>';
        }
        buttonHtml = '<div class="unit-nav-buttons color-primary">' + buttonHtml + '</div>';
        $unitContent.append(buttonHtml);
        if (hasPrev) {
          $('#btn-prev').click(function() {
            $(document).trigger('showEpigraph', {idBook: idBook, unit: unit, epigraph: (epigraph - 1)});
          });
        }
        if (hasNext) {
          $('#btn-next').click(function() {
            $(document).trigger('showEpigraph', {idBook: idBook, unit: unit, epigraph: (epigraph + 1)});
          });
        }
      }
      for (var i = 0; i < activityIframes.length; i++) {
        initializeActivity($('#' + activityIframes[i].id), activityIframes[i].id, activityIframes[i].minHeight);
      }

      feather.replace();
      if (questions.length > 0) {
        var service = 'questions',
            security = {
              'consumer_key': 'yis0TYCu7U9V4o7M',
              'user_id': 'demo_student'
            },
            secret = '74c5fd430cf1242a527f6223aebd42d30464be22',
            request = {
              type:       'local_practice',
              state:      'initial',
              questions:  questions
            }, options;
        var initOptions = Learnosity.init(service, security, secret, request);
        window.questionsApp = LearnosityApp.init(initOptions);
      }

      $('.sm-carousel').each(function () {
        var $smCarousel = $(this);
        var myFlipster  = $smCarousel.flipster({
          style: 'flat',
          nav: 'after',
          scrollwheel: false
        });

        $smCarousel.find('.flipster__nav__item').click(function () {
          var $link = $(this);
          myFlipster.flipster('jump', parseInt($link.text()));
        });
      });
      
      $('.sm-video video').each(function () {
        new Plyr('#' + $(this).attr('id'));
      });

      var $elem = null;
      $('.block-drag').draggable({
        start: function( event, ui ) {
          $elem = ui.helper;
        }
      });
      $('.block-drop').droppable({
        drop: function () {
          if ($elem !== null) {
            $elem.detach().appendTo($(this));
            $elem = null;
          }
        }
      });
    }

    window.addEventListener('message', handleResponse, false);

  });
})(jQuery);