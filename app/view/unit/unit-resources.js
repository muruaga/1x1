(function ($) {

  $(document).ready(function () {

    var currentUnit = null;
    var numResources = 0;
    var numImages = 0;
    var numInteractives = 0;
    var numVideos = 0;
    var numActivities = 0;

    $(document).on('showResources', function (event) {
      $('#unit-resources').addClass('show')
    });
    $(document).on('hideResources', function (event) {
      $('#unit-resources').removeClass('show')
    });
    $(document).on('showEpigraph', function (event, obj) {
      $('#unit-resources').removeClass('show');
      if (obj.unit !== currentUnit) {
        currentUnit = obj.unit;
        loadResources(obj.idBook, obj.unit);
      }
    });
    $(document).on('showSection', function (event, id) {
      $('#unit-resources').removeClass('show');
    });

    function loadResources(idBook, unit) {
      numResources = 0;
      var unitFound = null;
      var $resourcesList = $('#resources-list');
      $resourcesList.empty();
      for (var i = 0; i < library.length; i++) {
        if (library[i].id === idBook && typeof (library[i].elems[unit - 1]) !== 'undefined') {
          unitFound = library[i].elems[unit - 1];
          break;
        }
      }
      if (unitFound) {
        var imagesHtml = '';
        var interactivesHtml = '';
        var videosHtml = '';
        var activitiesHtml = '';
        var obj = {
            imagesHtml: '',
            interactivesHtml: '',
            videosHtml: '',
            activitiesHtml: ''
        };
        for (i = 0; i < unitFound.elems.length; i++) {
          obj.imagesHtml = '';
          obj.interactivesHtml = '';
          obj.videosHtml = '';
          obj.activitiesHtml = '';

          if (i === 0 && typeof (library[i].elems[unit - 1].embed) !== 'undefined') {
            obj = processEmbed(library[i].elems[unit - 1].embed);
          }
          if (i > 0 && typeof (unitFound.elems[i].embed) !== 'undefined') {
            obj = processEmbed(unitFound.elems[i].embed);
          }
          imagesHtml += obj.imagesHtml;
          interactivesHtml += obj.interactivesHtml;
          videosHtml += obj.videosHtml;
          activitiesHtml += obj.activitiesHtml;

          for (var j = 0; j < unitFound.elems[i].elems.length; j++) {
            if (typeof (unitFound.elems[i].elems[j].embed) !== 'undefined') {
              obj = processEmbed(unitFound.elems[i].elems[j].embed);
              imagesHtml += obj.imagesHtml;
              interactivesHtml += obj.interactivesHtml;
              videosHtml += obj.videosHtml;
              activitiesHtml += obj.activitiesHtml;
            }
          }
        }

        $resourcesList.append(
          '<div class="tit-resources">' +
            '<div class="big-icon"><i data-feather="image"></i></div>' +
            '<h3>Recursos.</h3>' +
            '<h4>' + unitFound.title + '</h4>' +
            '<div class="row">' +
              '<div class="col text-left">\n' +
                '<button class="btn color-light">Fitrar<i data-feather="sliders"></i></button>' +
              '</div>' +
              '<div class="col text-center input-group">' +
                '<input type="text" class="form-control search">' +
                '<span class="search-icon"><i data-feather="search"></i></span>' +
              '</div>' +
            '</div>' +
          '</div>' +
          '<div id="resource-selectors"></div>'
        );

        if (activitiesHtml !== '') {
          $resourcesList.append('<div class="tit-resource-type"><p>Actividades</p></div>' +
              '<section data-featherlight-gallery data-featherlight-filter="a">{0}</section>'.replace('{0}', activitiesHtml));
        }

        if (videosHtml !== '') {
          $resourcesList.append('<div class="tit-resource-type"><p>Vídeos</p></div>' +
              '<section data-featherlight-gallery data-featherlight-filter="a">{0}</section>'.replace('{0}', videosHtml));
        }

        if (interactivesHtml !== '') {
          $resourcesList.append('<div class="tit-resource-type"><p>Recursos interactivos</p></div>' +
              '<section data-featherlight-gallery data-featherlight-filter="a">{0}</section>'.replace('{0}', interactivesHtml));
        }

        if (imagesHtml !== '') {
            $resourcesList.append('<div class="tit-resource-type"><p>Imágenes</p></div>' +
                '<section data-featherlight-gallery data-featherlight-filter="a">{0}</section>'.replace('{0}', imagesHtml));
        }
        $('#resource-selectors').append(
          '<div id="sel-res-activity" class="resource-select"><div class="resource-icon"><i data-feather="loader"></i></div>' +
            '<div class="resource-name">Actividades (' + numActivities + ')</div>' +
          '</div>' +
          '<div id="sel-res-video" class="resource-select"><div class="resource-icon"><i data-feather="video"></i></div>' +
            '<div class="resource-name">Vídeos (' + numVideos + ')</div>' +
          '</div>' +
          '<div id="sel-res-interactive" class="resource-select"><div class="resource-icon"><i data-feather="mouse-pointer"></i></div>' +
            '<div class="resource-name">Interactivos (' + numInteractives + ')</div>' +
          '</div>' +
          '<div id="sel-res-image" class="resource-select"><div class="resource-icon"><i data-feather="image"></i></div>' +
            '<div class="resource-name">Imágenes (' + numImages + ')</div>' +
          '</div>'
        );
        $('.resource-select').click(function() {
          selectResource($(this).attr('id'));
        });
      }
      $('#num-recursos').html('(' + numResources + ')');
      feather.replace();
    }

    function selectResource(id) {
      var $selector = $('#' + id);
      var resourceType = id.substring(8);
      if ($selector.hasClass('color-primary')) {
        $('.tit-resource-type').show();
        $('.resource-card').show();
        $selector.removeClass('color-primary');
      } else {
        $('.tit-resource-type').hide();
        $('.resource-card').hide();
        $('.' + resourceType + '-card').show();
        $('.resource-select').removeClass('color-primary');
        $selector.addClass('color-primary');
      }
    }

    function processEmbed(embed) {
      var obj = {
        imagesHtml: '',
        interactivesHtml: '',
        videosHtml: '',
        activitiesHtml: ''
      };
      for (var i = 0; i < embed.length; i++) {
        numResources++;
        switch (embed[i].type) {
          case 'image':
            numImages++;
            obj.imagesHtml += '<a class="resource-card image-card" href="' + embed[i].url + '" data-featherlight="image"><img class="small-pic" src="' + embed[i].url + '" alt=""></a>';
            break;
          case 'video':
            numVideos++;
            obj.videosHtml += '<a class="resource-card video-card" href="' + embed[i].url + '" data-featherlight="iframe" data-featherlight-iframe-width="640" data-featherlight-iframe-height="480"><img class="small-pic" src="' + embed[i].thumb + '" alt=""></a>';
            break;
          case 'interactive':
          case 'activity':
            numInteractives++;
            obj.interactivesHtml += '<a class="resource-card interactive-card" href="' + embed[i].url + '" data-featherlight="iframe" data-featherlight-iframe-width="640" data-featherlight-iframe-height="480"><img class="small-pic" src="' + embed[i].thumb + '" alt=""></a>';
            break;
          case 'html-activity':
            numActivities++;
            var dataActivity = embed[i].content;
            if ((typeof(embed[i].elems) !== 'undefined') && (embed[i].elems.length > 0)) {
              for (var j = 0; j < embed[i].elems.length; j ++) {
                  dataActivity = dataActivity.replace('{' + j + '}', embed[i].elems[j].url);
              }
            }
            obj.activitiesHtml += "<a class='resource-card activity-card' href='#' data-featherlight='" +  dataActivity + "' ><div class='activity-card color-primary'><div class='activity-number'>" + embed[i].num + "</div><div class='activity-title'>" + embed[i].title + "</div></div></a>";
            break;
        }
      }
      return obj;
    }
  });
})(jQuery);