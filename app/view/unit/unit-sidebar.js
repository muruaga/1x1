(function($) {

  $(document).ready(function () {

    var sidebarOpened = true;

    var position = {
      idBook: '',
      unit: 0,
      epigraph: 0
    };

    var lastId, topMenu, menuItems, scrollItems;
    var epigraphSelected = null;
    $(document).on('showEpigraph', function (event, obj) {
      $('.current').removeClass('current');
      scrollItems = [];
      $('.unit-content').unbind('scroll');
      initTableContents(obj.idBook, obj.unit, obj.epigraph);
      setTimeout(initScroll, 300);

      epigraphSelected = getEpigraph(obj.idBook, obj.unit, obj.epigraph);
      if (epigraphSelected !== null && typeof epigraphSelected.notes === 'object') {
        $('#MenuNewNote').hide();
        $('#MenuNotes').show();
      }
      else {
        $('#MenuNewNote').show();
        $('#MenuNotes').hide();
      }

    });

    $(document).on('initTableContents', function (event, obj) {
      $('.current').removeClass('current');
      scrollItems = [];
      $('.unit-content').unbind('scroll');
      initTableContents(obj.idBook, obj.unit, obj.epigraph, true);
      setTimeout(initScroll, 300);
    });

    $(document).on ('click', '#MenuResources', function () {
      $('#unit-resources').addClass('show');
    });

    $(document).on ('click', '#MenuNotes', function () {
      var note = epigraphSelected.notes[0];
      $(document).trigger('OnShowView', ['.view.note-editor', note]);
    });

    $(document).on ('click', '#MenuNewNote', function () {
      var idNote = 'note-' + library[0].id.split('-')[1] + '-1';
      epigraphSelected.notes = [
        {
          id: idNote,
          bookmark: 1,
          title: 'Título del apunte',
          subtitle: 'Subtítulo del apunte',
          html: ''
        }
      ];
      var note = epigraphSelected.notes[0];
      $(document).trigger('OnShowView', ['.view.note-editor', note]);
    });

    /*$(document).on ('click', '#MenuNotes', function () {
      $(document).trigger('OnShowView', '.view.note');
      $(document).trigger('ShowNote');
      $('.btn-back.unit').show();
    });*/


    $(document).on ('click', '#MenuSummary', function () {
        $(document).trigger('OnShowView', '.view.summary');
        $(document).trigger('ShowSummary');
        $('.btn-back.unit').show();
    });

    $(document).on('click', '#buttonToggleSidebar', function () {
      sidebarOpened = !sidebarOpened;
      $('.view.unit').toggleClass('collapse');
      $(document).trigger('toggleSidebar', sidebarOpened);
    });

    $(document).on('click', '#buttonToggleMenu', function () {
      $('.unit-sidebar').removeClass('menu-collapse');
    });

    $(document).on('click', '#buttonCloseMenu', function () {
      $('.unit-sidebar').addClass('menu-collapse');
    });


    function getEpigraph(idBook, unit, epigraph) {
      var epigraphFound = null;
      for (var i = 0; i < library.length; i++) {
        if (library[i].id === idBook && typeof(library[i].elems[unit -1]) !== 'undefined'
          && typeof(library[i].elems[unit -1].elems[epigraph]) !== 'undefined') {
          epigraphFound = library[i].elems[unit -1].elems[epigraph];
          break;
        }
      }
      return epigraphFound;
    }

    function initTableContents(idBook, unit, epigraph, force) {
      if (idBook !== position.idBook || unit !== position.unit || (typeof(force)!=='undefined' && force)) {
        $('#contentList').empty();
        var unitFound = null;
        for (var i = 0; i < library.length; i++) {
          if (library[i].id === idBook && typeof(library[i].elems[unit -1]) !== 'undefined'
            && typeof(library[i].elems[unit -1].elems[epigraph]) !== 'undefined') {
            unitFound = library[i].elems[unit -1];
            break;
          }
        }
        if (unitFound !== null && (typeof(unitFound.visible) === 'undefined' || unitFound.visible)) {
          position.idBook = idBook;
          position.unit = unit;
          $('#sidebarUnitNumber').text('Unidad ' + unit);
          $('#sidebarUnitTitle').text(unitFound.title);
          for (var i = 0; i < unitFound.elems.length; i ++) {
            if (typeof(unitFound.elems[i].visible) === 'undefined' || unitFound.elems[i].visible) {
              var elem = unitFound.elems[i];
              var sectionsHtml = '';
              for (var j = 0; j < elem.elems.length; j++) {
                if (typeof(unitFound.elems[i].elems[j].visible) === 'undefined' || unitFound.elems[i].elems[j].visible) {
                  var subElem = unitFound.elems[i].elems[j];

                  sectionsHtml += '<div class="tit-section" id="sel-' + subElem.id + '">' +
                      '<span class="minidot"></span>' +
                      subElem.title + '</div>';
                }
              }


              var $epigraph = $((
                  '<li class="toc-list list-group-item">' +
                  '<div class="tit-epigraph" id="sel-' + elem.id + '">' +
                  '<span class="dot">' + elem.num + '</span>' +
                  '<span class="title">' + elem.title + '</span>' +
                  '</div>{0}' +
                  '</li>').replace('{0}', sectionsHtml));


              $('#contentList').append($epigraph);
              openEpigraph($epigraph.find('.tit-epigraph'), position.idBook, position.unit, i);
              $epigraph.find('.tit-section').each(function () {
                openSection($(this));
              });
            }
          }

          $('.unit-sidebar .logo').css('background-image', 'url(../images/' + unitFound.thumb + ')');

        }
      }
      position.epigraph = epigraph;
      $('.toc-list').removeClass('selected');
      $('.toc-list:eq(' + position.epigraph + ')').addClass('selected');


    }

    function initScroll() {
      var topMenu = $('#contentList'),
        // All list items
        menuItems = topMenu.find('div'),
        // Anchors corresponding to menu items
        scrollItems = [];

      // Bind to scroll
      $('.unit-content-container').scroll(function(){
        if (scrollItems.length === 0) {
          scrollItems = menuItems.map(function(){
            var id = $(this).attr('id');
            if (typeof(id) !== 'undefined') {
              id = id.substring(4);
            }
            var item = $('#' + id);
            if (item.length) { return item; }
          });
        }
        // Get container scroll position
        var fromTop = $(this).scrollTop()+100;

        // Get id of current scroll item
        var cur = scrollItems.map(function(){
          if ($(this).offset().top < fromTop)
            return this;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? 'sel-' + cur[0].id : '';

        if (id !== '' && lastId !== id) {
          lastId = id;
          repaintSelection(id)
        }
      });
    }

    function repaintSelection(id) {
      $('.tit-section').removeClass('before-current');
      $('.current').removeClass('current');

      var $area = $('#' + id);
      $area.addClass('current');
      while ($area.length) {
        if (!$area.hasClass('tit-section')) {
          break;
        }
        $area.addClass('before-current');
        $area = $area.prev();
      }
    }

    function openEpigraph($epigraph, idBook, unit, epigraph) {
      $epigraph.click(function() {
        $(document).trigger('showEpigraph', {idBook: idBook, unit: unit, epigraph: epigraph});
      });
    }

    function openSection($section) {
      $section.click(function(e) {
        $(document).trigger('showSection', $section.attr('id').substring(4));
      });
    }

  });
})(jQuery);