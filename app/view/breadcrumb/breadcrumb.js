(function ($) {
  var $breadCrumb;
  $(document).ready(function () {
    var actualEpigraph = null;

    $breadCrumb = $('.breadcrumb .title');

    var $breadCrumbItem = '<a class="breadcrumb-item" data-type="{$data-type}" href="#">{$title}</a>';

    $(document).on('ShowLibrary', function () {
      $breadCrumb.html('');
      appendLibrary();
    });

    $(document).on('showEpigraph', function (event, data) {
      actualEpigraph = data;
      $breadCrumb.html('');
      appendLibrary();
      appendUnit(actualEpigraph.unit - 1);
      appendEpigraph(actualEpigraph.unit - 1, actualEpigraph.epigraph);
    });

    $(document).on('ShowNote', function () {
      $breadCrumb.html('');
      appendLibrary();
      appendNote();
    });

    $(document).on('click', '.breadcrumb-item', function () {
      var type = $(this).attr('data-type');
      switch (type) {
        case 'units':
          $(document).trigger('OnShowView', '.view.units');
          $(document).trigger('ShowLibrary');
          break;
        case 'unit':
          actualEpigraph.epigraph = 0;
          $(document).trigger('showEpigraph', actualEpigraph);
          break;
        case 'epigraph':
          $('.unit-content').scrollTop(0);
          break;
      }
    });

    $(document).on('click', '.btn-back', function () {
      $(document).trigger('OnShowView', $(this).attr('data-back'));
    });


    var $buttonsBacks = $('.btn-back');
    var $btnBackUnits = $('.btn-back.units');
    var $btnBackUnit = $('.btn-back.unit');
    var $btnBackNote = $('.btn-back.note');

    $(document).on('OnShowView', function (event, selector) {
      $buttonsBacks.hide();
      switch (selector) {
        case '.view.unit':
          $btnBackUnits.show();
          break;
        case '.view.note':
          $btnBackUnit.show();
          break;
        case '.view.note-editor':
          $btnBackNote.show();
          break;
      }
    });

    function appendLibrary() {
      $breadCrumb.append($(
        $breadCrumbItem.replace('{$data-type}', 'units').replace('{$title}', library[0].title)
      ));
    }

    function appendUnit(indexUnit) {
      $breadCrumb.append($(
        $breadCrumbItem.replace('{$data-type}', 'unit').replace('{$title}', 'Unidad' + (indexUnit + 1) + '. ' + library[0].elems[indexUnit].title)
      ));
    }

    function appendEpigraph(indexUnit, indexEpigraph) {
      $breadCrumb.append($(
        $breadCrumbItem.replace('{$data-type}', 'epigraph').replace('{$title}', library[0].elems[indexUnit].elems[indexEpigraph].title)
      ));
    }

    function appendNote() {
      $breadCrumb.append($(
        $breadCrumbItem.replace('{$data-type}', 'note').replace('{$title}', 'Apuntes')
      ));
    }

    $(document).trigger('ShowLibrary');


  });
})(jQuery);