﻿var paginaGaleriaActual = 1;

$(document).ready(function() {
    $(".paginador > .paginador-boton").click(function() {
        var pag = $(this).attr("pag");
        if(isNaN(parseInt(pag, 10))) {
            pag = eval(paginaGaleriaActual + pag + "1");

            if(pag < 1) {
                pag = 1;
            }
        }
        else {
            pag = parseInt(pag, 10);
        }

        if (pag != paginaGaleriaActual) {
            $(".galeria-elemento[pag="+ paginaGaleriaActual +"]").hide();
            $(".galeria-elemento[pag="+ pag +"]").show();
            paginaGaleriaActual = pag;
        }
    });

    try {
        if(!lvb) {
            dis();
        }
    }
    catch(err) {
        // La variable no esta definida, puede ser un intento de burlar la fecha de expiración
        try {
            console.debug(err);
        }
        catch(ierr) {}
        dis();
    }

    if(typeof dsp === 'undefined' || ((typeof dsp !== 'undefined' && !dsp) && (location.protocol == "http:" || location.protocol == "https:"))) {
        $("body").hide();
        location.href = "http://www.grupo-sm.com";
    }

    // if(location.protocol !== "file:" || base === "" || !tieneSoporteMML()) {
    //     MathJax.Hub.Configured();
    // }

});

function cargarFlash(id, url, w, h) {
    var params = {
        base: "."
    };
    if (w <= 0) {
		w = $(window).width();
	}
	if (h <= 0) {
		h = $(window).height();
	}

    if (SWFObject) {
        // SWFObject 1.5
        var so = new SWFObject(url, id, w, h, "9.0.0", "#FFFFFF");
        so.addParam("base", ".");
        so.write("divFlash");
    }
    else if (swfobject) {
        swfobject.embedSWF(url, id, w, h, "9.0.0", base +"swf/expressInstall.swf", null, params);
    }

}

function mostrarErrorConexion(div) {
	$(document).ready(function() {
		$(div).hide();
		$("#divMsgErrorConexion").fadeIn('slow');
	});
}

var tieneConexion = false;

function verificaConexionInternet() {
//	var img = $("<img />").load(function() {
//		tieneConexion = true;
//	}).error(function() {
//		tieneConexion = false;
//	});
//	img.attr("src", "http://www.smlir.com/Plantillas/sm_lir/imgs/spinner.gif");
}

function tieneSoporteMML() {
  var hasMathML = false;
  if (document.createElementNS) {
    var _namespace = "http://www.w3.org/1998/Math/MathML",
        div = document.createElement("div");
        div.style.position = "absolute";
    var mfrac = div.appendChild(document.createElementNS(_namespace,"math"))
                   .appendChild(document.createElementNS(_namespace,"mfrac"));
    mfrac.appendChild(document.createElementNS(_namespace,"mi"))
         .appendChild(document.createTextNode("xx"));
    mfrac.appendChild(document.createElementNS(_namespace,"mi"))
         .appendChild(document.createTextNode("yy"));
    document.body.appendChild(div);
    hasMathML = div.offsetHeight > div.offsetWidth;
    document.body.removeChild(div);
  }
  return hasMathML;
}