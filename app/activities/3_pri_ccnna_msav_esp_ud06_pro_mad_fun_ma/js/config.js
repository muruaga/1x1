﻿$(document).ready(function () {
	var cfg = {
		sizeMode: 1,
		audios: [],
		autoEvaluate: true,
		platform: "SM",
		enunciado: cadenas.enunciado,
		SeriesEngine: { gameType: "arrastrar"},
		backgroundImage: { id:"fondo", x: 116, y: 113},
		audioOK: audios.acierto,
		audioKO: audios.error,
		audioClic: audios.sinAudio,
		audioFinal: audios.sinAudio,
		hotAreas: [
			{
				name: "gasolina",
				points: [ { x: 512, y: 119}, { x: 697, y: 117}, { x: 696, y: 287}, { x: 511, y: 288}, ],
				hotAreaVisible: false,
				multidrop: true,
			},
			{
				name: "electricidad",
				points: [ { x: 305, y: 118}, { x: 490, y: 116}, { x: 489, y: 286}, { x: 304, y: 287}, ],
				hotAreaVisible: false,
				multidrop: true,
			},
			{
				name: "personas",
				points: [ { x: 111, y: 115}, { x: 296, y: 113}, { x: 295, y: 283}, { x: 110, y: 284}, ],
				hotAreaVisible: false,
				multidrop: true,
			},
		],

		successes: 8,
		imageObjects: [{ x: 144, y: 442, id: "seca", hotAreas: [{hotArea: "electricidad", esAreaValida: true,dropZones: [{x: 391, y: 171},]}], clonable: false, initScale: 1, dragScale: 1, dropScale: 0.01  }, { x: 495, y: 458, id: "saca", hotAreas: [{hotArea: "personas", esAreaValida: true,dropZones: [{x: 160, y: 195},]}], clonable: false, initScale: 1, dragScale: 1, dropScale: 0.01  }, { x: 245, y: 371, id: "plancha", hotAreas: [{hotArea: "electricidad", esAreaValida: true,dropZones: [{x: 345, y: 160},]}], clonable: false, initScale: 0.5, dragScale: 0.5, dropScale: 0.01  }, { x: 156, y: 328, id: "pinza", hotAreas: [{hotArea: "personas", esAreaValida: true,dropZones: [{x: 155, y: 154},]}], clonable: false, initScale: 1, dragScale: 1, dropScale: 0.01  }, { x: 418, y: 361, id: "moto", hotAreas: [{hotArea: "gasolina", esAreaValida: true,dropZones: [{x: 601, y: 152},]}], clonable: false, initScale: 0.8, dragScale: 0.8, dropScale: 0.01  }, { x: 594, y: 336, id: "heli", hotAreas: [{hotArea: "gasolina", esAreaValida: true,dropZones: [{x: 518, y: 173},]}], clonable: false, initScale: 0.8, dragScale: 0.8, dropScale: 0.01  }, { x: 311, y: 438, id: "abre", hotAreas: [{hotArea: "personas", esAreaValida: true,dropZones: [{x: 139, y: 147},]}], clonable: false, initScale: 0.7, dragScale: 0.7, dropScale: 0.01  }, { x: 613, y: 432, id: "bici", hotAreas: [{hotArea: "personas", esAreaValida: true,dropZones: [{x: 198, y: 157},]}], clonable: false, initScale: 1, dragScale: 1, dropScale: 0.01  }, ],
		successesText: 0,
		textObjects: [],		canViewSolution: false, 

		buttonEnd: {x:400, y:363, width:100, textId:cadenas.corregir },
		buttonRepeat: {x:809, y:368, width:75, textId:cadenas.repetir },
		animation: {confettiColors:[["#FF0054","#E500FF"],["#00FF00","#0000FF"],["#00FFFF","#FF0000"],["#FFFF00","#B06C00"]],backgroundColor:"Black",backgroundOpacity:0.7,confettiRibbonCount:7,confettiPaperCount:100,pauseToEnd:1500,soundId:audios.fin_actividad},
	};
	var animation = new sm.ConfettiAnimation(cfg);
	var engine = new sm.SeriesEngine("html5Canvas", cfg, animation);
	engine.run();
});
