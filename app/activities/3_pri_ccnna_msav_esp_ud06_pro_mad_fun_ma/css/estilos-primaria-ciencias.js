﻿styles = {
//GENERALES
    backgroundColor: "#FFFFFF",
    baseColor1: "#F9FF80",
    baseColor2: "#97BF0D",
    baseColor3: "#218B29",
    complementaryColor: "#E85181",
    fontFamily: "SourceSansProRegular",
    fontFamilySymbol: "smsymbolregular",
    fontFamilyBold: "SourceSansProBold",
    fontSize: 16,
    fontColor: "#333333",
    fontLineHeight: 19, //separacion de lineas de texto
    roundBorder: 8, //redondeado de objetos por defecto
    roundSmallBorder: 5, //Redondeado para los objetos menores de 28px de ancho/alto    
    paddingTop: 20, //Distancia de los objetos hasta la cabecera
    distanceBetweenObjects: 7, //Distancia entre controles en pantalla
        
//CABECERA
    headerSM: {
        logoSM: "logo",
        objectDistanceBorder: 17,
        icon: "icon",
        iconDistanceLeft: 12,
        backgroundColor: "#F9FF80",
        fontFamily: "SourceSansProRegular",
        fontSize: 18,
        fontColor: "#333333",
        simple: false, //Indica si es con pastilla (se usara height) o simple tamaño de 7px
        height: 65, // Tamaño con pastilla            
        minHeight: 7, //Habrá veces que no se muestre la pastilla y se sustituirá por una banda de 7 px del mismo color.
        logoImage: {width: 112, height: 112, image: "data:image_png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABwCAYAAADG4PRLAAAABGdBTUEAALGOfPtRkwAAACBjSFJNAACHDwAAjA8AAP1SAACBQAAAfXkAAOmLAAA85QAAGcxzPIV3AAAKOWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAEjHnZZ3VFTXFofPvXd6oc0w0hl6ky4wgPQuIB0EURhmBhjKAMMMTWyIqEBEEREBRZCggAGjoUisiGIhKKhgD0gQUGIwiqioZEbWSnx5ee_l5ffHvd_aZ+9z99l7n7UuACRPHy4vBZYCIJkn4Ad6ONNXhUfQsf0ABniAAaYAMFnpqb5B7sFAJC83F3q6yAn8i94MAUj8vmXo6U+ng_9P0qxUvgAAyF_E5mxOOkvE+SJOyhSkiu0zIqbGJIoZRomZL0pQxHJijlvkpZ99FtlRzOxkHlvE4pxT2clsMfeIeHuGkCNixEfEBRlcTqaIb4tYM0mYzBXxW3FsMoeZDgCKJLYLOKx4EZuImMQPDnQR8XIAcKS4LzjmCxZwsgTiQ7mkpGbzuXHxArouS49uam3NoHtyMpM4AoGhP5OVyOSz6S4pyalMXjYAi2f+LBlxbemiIluaWltaGpoZmX5RqP+6+Dcl7u0ivQr43DOI1veH7a_8UuoAYMyKarPrD1vMfgA6tgIgd_8Pm+YhACRFfWu_8cV5aOJ5iRcIUm2MjTMzM424HJaRuKC_6386_A198T0j8Xa_l4fuyollCpMEdHHdWClJKUI+PT2VyeLQDf88xP848K_zWBrIieXwOTxRRKhoyri8OFG7eWyugJvCo3N5_6mJ_zDsT1qca5Eo9Z8ANcoISN2gAuTnPoCiEAESeVDc9d_75oMPBeKbF6Y6sTj3nwX9+65wifiRzo37HOcSGExnCfkZi2viawnQgAAkARXIAxWgAXSBITADVsAWOAI3sAL4gWAQDtYCFogHyYAPMkEu2AwKQBHYBfaCSlAD6kEjaAEnQAc4DS6Ay+A6uAnugAdgBIyD52AGvAHzEARhITJEgeQhVUgLMoDMIAZkD7lBPlAgFA5FQ3EQDxJCudAWqAgqhSqhWqgR+hY6BV2ArkID0D1oFJqCfoXewwhMgqmwMqwNG8MM2An2hoPhNXAcnAbnwPnwTrgCroOPwe3wBfg6fAcegZ_DswhAiAgNUUMMEQbigvghEUgswkc2IIVIOVKHtCBdSC9yCxlBppF3KAyKgqKjDFG2KE9UCIqFSkNtQBWjKlFHUe2oHtQt1ChqBvUJTUYroQ3QNmgv9Cp0HDoTXYAuRzeg29CX0HfQ4+g3GAyGhtHBWGE8MeGYBMw6TDHmAKYVcx4zgBnDzGKxWHmsAdYO64dlYgXYAux+7DHsOewgdhz7FkfEqeLMcO64CBwPl4crxzXhzuIGcRO4ebwUXgtvg_fDs_HZ+BJ8Pb4LfwM_jp8nSBN0CHaEYEICYTOhgtBCuER4SHhFJBLVidbEACKXuIlYQTxOvEIcJb4jyZD0SS6kSJKQtJN0hHSedI_0ikwma5MdyRFkAXknuZF8kfyY_FaCImEk4SXBltgoUSXRLjEo8UISL6kl6SS5VjJHslzypOQNyWkpvJS2lIsUU2qDVJXUKalhqVlpirSptJ90snSxdJP0VelJGayMtoybDFsmX+awzEWZMQpC0aC4UFiULZR6yiXKOBVD1aF6UROoRdRvqP3UGVkZ2WWyobJZslWyZ2RHaAhNm+ZFS6KV0E7QhmjvlygvcVrCWbJjScuSwSVzcopyjnIcuUK5Vrk7cu_l6fJu8onyu+U75B8poBT0FQIUMhUOKlxSmFakKtoqshQLFU8o3leClfSVApXWKR1W6lOaVVZR9lBOVd6vfFF5WoWm4qiSoFKmclZlSpWiaq_KVS1TPaf6jC5Ld6In0SvoPfQZNSU1TzWhWq1av9q8uo56iHqeeqv6Iw2CBkMjVqNMo1tjRlNV01czV7NZ874WXouhFa+1T6tXa05bRztMe5t2h_akjpyOl06OTrPOQ12yroNumm6d7m09jB5DL1HvgN5NfVjfQj9ev0r_hgFsYGnANThgMLAUvdR6KW9p3dJhQ5Khk2GGYbPhqBHNyMcoz6jD6IWxpnGE8W7jXuNPJhYmSSb1Jg9MZUxXmOaZdpn+aqZvxjKrMrttTjZ3N99o3mn+cpnBMs6yg8vuWlAsfC22WXRbfLS0suRbtlhOWWlaRVtVWw0zqAx_RjHjijXa2tl6o_Vp63c2ljYCmxM2v9ga2ibaNtlOLtdZzllev3zMTt2OaVdrN2JPt4+2P2Q_4qDmwHSoc3jiqOHIdmxwnHDSc0pwOub0wtnEme_c5jznYuOy3uW8K+Lq4Vro2u8m4xbiVun22F3dPc692X3Gw8Jjncd5T7Snt+duz2EvZS+WV6PXzAqrFetX9HiTvIO8K72f+Oj78H26fGHfFb57fB+u1FrJW9nhB_y8_Pb4PfLX8U_z_z4AE+AfUBXwNNA0MDewN4gSFBXUFPQm2Dm4JPhBiG6IMKQ7VDI0MrQxdC7MNaw0bGSV8ar1q66HK4RzwzsjsBGhEQ0Rs6vdVu9dPR5pEVkQObRGZ03WmqtrFdYmrT0TJRnFjDoZjY4Oi26K_sD0Y9YxZ2O8YqpjZlgurH2s52xHdhl7imPHKeVMxNrFlsZOxtnF7YmbineIL4+f5rpwK7kvEzwTahLmEv0SjyQuJIUltSbjkqOTT_FkeIm8nhSVlKyUgVSD1ILUkTSbtL1pM3xvfkM6lL4mvVNAFf1M9Ql1hVuFoxn2GVUZbzNDM09mSWfxsvqy9bN3ZE_kuOd8vQ61jrWuO1ctd3Pu6Hqn9bUboA0xG7o3amzM3zi+yWPT0c2EzYmbf8gzySvNe70lbEtXvnL+pvyxrR5bmwskCvgFw9tst9VsR23nbu_fYb5j_45PhezCa0UmReVFH4pZxde+Mv2q4quFnbE7+0ssSw7uwuzi7Rra7bD7aKl0aU7p2B7fPe1l9LLCstd7o_ZeLV9WXrOPsE+4b6TCp6Jzv+b+Xfs_VMZX3qlyrmqtVqreUT13gH1g8KDjwZYa5ZqimveHuIfu1nrUttdp15UfxhzOOPy0PrS+92vG140NCg1FDR+P8I6MHA082tNo1djYpNRU0gw3C5unjkUeu_mN6zedLYYtta201qLj4Ljw+LNvo78dOuF9ovsk42TLd1rfVbdR2grbofbs9pmO+I6RzvDOgVMrTnV32Xa1fW_0_ZHTaqerzsieKTlLOJt_duFczrnZ86nnpy_EXRjrjup+cHHVxds9AT39l7wvXbnsfvlir1PvuSt2V05ftbl66hrjWsd1y+vtfRZ9bT9Y_NDWb9nffsPqRudN65tdA8sHzg46DF645Xrr8m2v29fvrLwzMBQydHc4cnjkLvvu5L2key_vZ9yff7DpIfph4SOpR+WPlR7X_aj3Y+uI5ciZUdfRvidBTx6Mscae_5T+04fx_Kfkp+UTqhONk2aTp6fcp24+W_1s_Hnq8_npgp+lf65+ofviu18cf+mbWTUz_pL_cuHX4lfyr468Xva6e9Z_9vGb5Dfzc4Vv5d8efcd41_s+7P3EfOYH7IeKj3ofuz55f3q4kLyw8Bv3hPP74uYdwgAAAAlwSFlzAAAuIwAALiMBeKU_dgAAEj1JREFUeF7tnAmQHcV5xxshC2JpxRVgd6WV9pL20O6+mfeEKiC77CqqcpVtErCN7RyVuFxJJS47VUvsVAWXj9ixDeWjbBdOwu2gCxQdgGRJ+4TAHDIgQMdboRNJKyHQhQ4EBmzzJv9_7_RT7+z37hZod6erfrVS78y8nu83X0_3TL9VQRDEjGDEypiRg1gZM3IQK2NGDmJlzMhBrIwZOYiVLnhtSsIVreBfwVpwDAQ2J8BxsKnJD1Z0JIPFs5LBEvBgZzL4Jf7f1+4Hj4KnZvrBszP84IUWL8g0e8GORi_YO90LDjZ4wdGpQ49ZBe+C9eA6kDsHKT6uECtdYJ9AFXwMbABSsLS4E_WJ4HlIuTORCu7uSQULu95XgYbDYA7Q5yHFxxVipQtM46vgLwEDIQVIcxLyXoGARV3J7B0QSHn3v78ZaPM1oM9Fio8rxEoXmMZXyI3gCJACk+MkgIjsnT2p7C+6U8GCc0vgrUCfjxQfV4iVLjCNr4BPg6LyCAW+2Oi9e44KvAXoc5Li4wqx0gWm8WXySVCw27Q5xwV+H+jzkuLjCrHSBabxJXIeoLxDQAqGSCzw3BH4KVBSt2kTCzw3BF4Pyso8Qyzw_RfIzCv5nhclFvj+CmTmld1t2oQCOY0IYoGOMY3PAyfpFXWbNhCY3QoRd8QC3WMaL3ADqLjbtDlRnwgGGryn53el5vExWizQIabxET4Bqs48m9N1ieWbm7zJt_ekHpgfC3SHabzFTLATSCdbMafrE307p3sKAi+7rzu5OBboCNP4EE7U_xtIJ1oVx6ck0m_VJtSGFl_9DyRC4LJYoANM40P4Tm8bkE60WtKv1ydUf5Ov5nWl1KJZycsh8MFYYJWYxofw7cJJIJ1otaSBehtZuL7VV_f0JBUEXgqBS2OBVWAaH_JlwLfV0olWixZ4ClnIe+GyzqRaPCupIPAPIfDBWGCFmMaH3ASkk3SBFngMvIMsfLTNV_O7tUAFgVdC4EOxwAowjQ_pBdJJukALJCeQhfuneWplh6+WIQshUEFgLQQujQWWiWl8yHsikLxVl1B9yELcB41ABYG1ELg8FlgGpvEh75lATCvU4akJtao9iUzMCVQQWAeBy2KBJWIaH_KeCSQc0KxtGyZQQWAdBD4UCywB0_iQsyYQg5c0BCgJZmFEoILAKyDwoVhgEUzjQ86KQMgL0F2mD0GWxBMzRYEKAi+HwBWxwAKYxoc4F3gUQB7XhaYhQUngd2oVRqSCQAWBV0LgSmwXC5QwjQ9xKlDLa0gEryL4L0PgAUzgJfaDZ1p9tU4WqCDwSgh8KBYoYBof4kwgu81DEMcV2QfAvuleGiIoYxgD4CXQhyzMI5CZWguBK6TPqpJYYJRjyJQjobyXp3kBBAV7IPAlyMjHbvA8pPEJza8hcUOrpzbj_9tRT4HsZnHc+uP1iYXmizLSZ1dALNCG8g5P9XS3eeCMvGBXo5fe2eSpfOwAFHmwIaF5ReMpXABqACCDFY7DbT7OC+NEvTOJsUDD4D3vTOYh6AG6xmAnpgLbmrz0i5CUD2Ya5OvHbJzkk2NTPYVuWD92ozx0px_ub_b2bcH8EF1qll+akdpRJrFAYkabryL7TOZRHjKP8oKtzV4awVcSvNcxCykNx8pxFAKRyUbgXFwIA5AdbGrxgo0tXvZAQyLLFW+mDRUSCwzneYPdJjDdZph5lBcga9K8p+UDxxmGJfBDON4Aj0eBzMANrT7JDkxLZKu8J45tgZR3BPLYbRp5duYhw4LNAKPKNAcpEuxCcaxhhALnQuDL5oKgwPB4AQY7wKNEvfYU+1TC2BWY6zZDebzn2ZlnyWO2pJ_FXC_KrzFlwLFEIPAaHHsvBOaOawvE_sHjbX7wBCTuo8TK7oljU2Bkkm6PNoPtCDC7zU1gY6sXPIdAPzPDTz+N6YENpwsQrQcvOGaUuRC4h8fPK3DG4AvgR9v94LE2P7tnmpc9Vb7EsSdQTxUQWD3aBGa0uQtsQ3B15iHALwzep4KnwVMQ+CSEGThh55OXI5guRAcvYC7YA4H6AikmkFnY1+EH6bYkt8u+XpfIYn+x7QJjSyDlHUFg9T0vHG3mMs90m5TXEso7E+T0rzBJN6wD2F4vscBxbShvAATlCFyLLOTiqDUAF1P2NDOxtGwcOwLNPI9BpTxzzzPy2G3q4JrMswKMbi7NjDOs7vAxSU8o3Lfsz78G7ONnkXIFUh6XKfLPmFDi6xBYwuh0bAgcHLAMn6RHR5sbrcxDVxn8CkBYkIZAvjIiXAvDifvxofJymWeoROBK8H+zksFy_ESXnj1dV1Ti6Bc4ZJ6HYJqpAgNq5nmDmTc4YDGZh67SyAtWdyTTqzoG17_wDTz212_kcXxCefvN5xkqFcgsXNSl_5hQdgf2wT2xkMTRLdDM8xhIe7TJYJpukxPrjQiolgdMUHPy2nW3lgZ6LWgG9z7co8xn8g_t5LpNm2oEctn+fd2pYBEkcj_eE_NIvBnotkjxcYVY6QLT+JAhAqOTdPvZJgMZmefl5JnMQ3cZrBqUFyzvTKaXdibVw5CIbtdk39Vgj_2ZNtUIZDfKL9Dc25OiyCy67IBTjIjE34O_Bfr8pfi4Qqx0gWl8SE5gdJI+gCCae15utAmYedZUQco8HVDIS9_XldQvbn9bqxf4_hE+Z6_5PAkXAhcgC_nF0v_tSmU5zYlM9neADjC6BJpJ+rB5HuUheLnMM_e8fPIQyFAe_7zW2iXIQIhQb9Rpebv4WYVwIXA+BPJbwf8FiUs6klmcW9bKwtz9j0jxcYVY6QL7BEAvly3Yo0372aaRtwXBs+d5erSZk5fkgEXLQ7ep70UPYECBIK55BJP3tyAPAdyNzxoiS8KVwHvB3WEmclATZiH_WmEdyJ2_FB9XiJUugDCbXk7SGbRh8sJuk_L4eMzIW2_JYxDtzNPywDwED_ehJRi8eG_UF888w9kQuKnZDzA_5FfouoF98YrxcYVY6QK+MLXoteUxaEaefrZJecg8e6rwOIh2m8y8pWEAF3bp4GVXdPh7kXn9fPWDYJWES4F3heye7m1DBvo4_hB5RIqPK8RKF3BZn0WvecJiRptD3ipAnNRt5kaboTyTeZTH4C3Dvecg7qnlPmh2KfB2ZN_KtmT_a_WJrsjTn5EtEMJseqVukwHTo00ELDpJZ+DMVEF3m4D3vAWQdw_ldSazrzQkKnlL4FTgHd2p7Vsbve4TkDeqBCLbbHpNoEy3aYJVZJJ+Rh6CtmDW4F_lXd7pM_MqkkccCtx+Z3fKg0C95mZUCUS22fSKmRfKY7cpydPdJuDfwWbm8S8yLe2oTh5xJHArBPoQqEalQC6ctei15TFI9iT9zFQhT+ah66Q8dpsvQx7fBiAwFeNA4HYI9CFQjVqBLzZ7Nr2Up9+km9Em5YVBegRBWgtx9mjTDFh05iWYeYMDFkwXRCnlUKXAbRCYhEA1qgVCmE2vnqRb8zwGiJnHZQviJD2UdxczD_KQecEbdbKQcqlC4DYI9CBQjXqB9tI+cFOheZ6ZKuhuk_LC0SYnyew2D05NlPL+rWQqFLgd7fMhUI0Jgc+3+jY3SfM8c8972Mo8DhByU4VwwEJ5CIQzKhC4CwITEKjGjMDIMr_PQN5p3W2GmcfADBltWt0m5fFBtYsBi0SZArdCYAoC9Z8uKSQQI2N1aoo3DCk+rhArXcB1mRaTEJCHEZB3xdFmKG8e5PE9G4XynlfNVKEQJQjMor0n0d4HILARAvXb_nwCdzcl1TtTfMU_+bWltnMYUnxcIVa6ICKQy_3qMWD5AQLywpr2ZD_kZSAqg0zLICCZhV2pfgQos77V33J4aiIDef0IduZsAIEZCMxAYAYCMxCYgcB+CMxA4BMQ+FO091oIVGivkgTOg7iFiavUQm+OWjqlXT11RZu6_ZJpSipSfFwhVrpAEKhXjfW1+xNWdSQn4b5XgwFKDeZ6NfO7UjX3z0rVIJg1b9clak5NSdQg0JMAfzoHAmsgsAYC9WdCYA0EToLAiRCovwjK9koCuXRjefdstTp5tfpWa4fqbZyh1HmhqTxFio8rxEoXFBCouAgJAvXfNYNABYG4qlMKgdRrWk4KAwGXQKD97ST9uRCoIFB_gzefQPQYqi8xR_24tVN9saFJXTz+A6GiwkWKjyvESheMJoGYo6rVnSn1rH+NuqutRzVe+AehmtKKFB9XiJUuGE0C10HesnZPXXvxZWpamfJYpPi4Qqx0QaZt9hA2t89Wj7Un1Zrwil7XfZXqA6vBMnRLD_fMUa82p9RvzqJALrd_c6qv3pmWUm9OT6mTjSl1tGnwwskncB3azPa2VCDOFCk+rhArXdDwgQuGMuECdW9zp3oSV_PimT2qHv8ndaB2woX4eaFqxHabr+hQbyNDTmL+ZHMCwT9uEf19KQTTZqubJ9eqlvEXqGbQBBITJqq9Tb7aATLNvtrU4qvnZiTVegh8MszABrStmiLFxxVipQuk8sFx56vJ549XE_EzX6k5b5y6BL+_OMLXJ9fpSfH++m51sL5HTR8_Ydg2xbh03Hg1XhgyXoTfkcmG889XtzXMUFdPvEjV4N_VFik+rhArXRCXM0WKjyvEShfE5UyR4uMKsdIFcTlTpPi4Qqx0QRmFN6UizzLOSnHxmePCnwWLFB9XiJUuyFPqwXXgX8D3wd1gEVgI7gDfBV8CfwFmgEKFAi4CteBKC_7_chAdfUwHfw2+Cczn3gP+E_w96ADFCrfhtt8B94L7wV3gW+BvQDMYVqT4uEKsdEGktIEfgw3gNcANCsFttoAHwD+DK0C08DnWD8EzAk+BJsAyBfBieQG8CaTPexv0g58AXmTR0gVuA9yG20rH+A3YBL4BJoNckeLjCrHSBVbhFbsb2CdbDm+BbYDHscsEsAZI+xCK+BOw2aorBV4Ac4Ap7C32AGlbiSxgu2YCXaT4uEKsdEFY_hG8AewTrBR2e3a3SIHLgLTtYcCu8ZBVVw4bwUfAzwAvIGmbYqSBfr8kxccVYqULUGaDo2y_I+4E9sCjkMB3we8jdeXCC4_HkX5XKreAcVJ8XCFWugDl1vAkCsH7yQnAex5_Mmj5As9BTqkCzxWOgdlSfFwhVroAhTd8+2RseGU_B3rBn4OPgj8Dfwf+A6wEO4HdfVUr8LdgH8gA3pPzDUYKwYEK74c8Bn+WkuX_LsXHFWKlC1DyjfgI5UqjPbu0Ao5AKYmBWwBKvQdGeQLwYkmCy0A74HRlB5C2l+A9jff0bnAp6AFfBQeAtL3hMSk+rhArXRA5iSgcpZVamHUcgX4e2K8FShHITOdUQ5qGsFwFOOCR9jUcBzeDSUAq7DlOAmlfckiKjyvEShegFBp98p73V6DcUk4G_g5wTlascO4n7W8o5RgF2yHFxxVipQtQnrVOQoJX7S8An7p8EJRbigkcAPmyxi6fANL+Bj45Klb+DXD+J+2PcMgxcoFY6QKUr9gnUQBmI5+S8JHUZ8BUUEopJnAXKEXgtUDa3_BZUKz8E8g75YjGxiVipQtQ+OxxvTmJEmG3+wpYDG4EQx5JRUoxgS+Bi0Gx8sdA2t9QikAOtkadQBZO5jnkHnJCJcJhP_f9ApAyKRYIxEoXWIUPlfnknxP1ISdWBqtA9G1BLBCIlS4QCgcDywEHF3lPtgB8k8G5oSmxQCBWuqBA4dyLE+AVYDsYcrJF4Ps7s74vFgjESheUUMYDD_wDuA+U8sqGT3fmApZYIBArXVBm4ctZZub3QKFHcOQmwKUMvABigVKlC1D4gJpZUk7hY7Nvg3fAkCBY8B3dBYBPZWKBUqULUDg5fxzwSUd0fUqh8qegUBb+HPCZaCwQiJUuQDGvk06DrYBrYj4OuFhpIqAAdoWE_2ZW8W3BIyDvYynADOX2sUAgVroAhcsS7BOhFD5gplC+xlkLlgA+deGIlJN2vjay95G4AbDwvhkLlCpdgBIV6AJ2y+Z7zPEoFIiVLkApd45XDL6d_xwwJRYIxEoXoDxmn0SVvA74yoZTB1MokE92pO0J55UuBNoXTb7yRTDqBHJe9yPABbpDTqgMzBpLvpmIFgrkgEfaj_DZ6yWgWPkYkPY3cJ1OscK5qbSvJhobl4iVLrAKH0KzG+ISuz7ABUWngDTS5CBmP2D2cinE9SDfcgiOQvn+kMvcubTdhgujuCC3lG9mtgCObKPHIKznGphihRertD_b8W0pPq4QK2NGDmJlzMhBrIwZOYiVMSMHsTJm5CBWxowUAvX_SGLhX43S+eAAAAAASUVORK5CYII="},
    },
            
//FOOTER
    footerSM: {
        backgroundColor: "#F9FF80",
        height: 7,        
    },
    
//CHECKBOX
    checkBox: {
        fontSize: 16,        
        fontColor: "#333333",        
        backgroundColor: "#FFFFFF",
        borderColor: "#4ABDC4",
        roundBorder: 8,        
        height: 48,
        outterCircleSize: 30,
        outterCircleBorderSize: 5,
        innerCircleSize: 19,
        outterCircleDistanceBorder: 10,
        titleOffsetToCircle: 10,
        drawBackgroundShape: true,
    },
    
//BOTONES
    //Rectangulares simples
    button: {
        topLeftRoundBorder: 8, 
        topRightRoundBorder: 8,
        bottomLeftRoundBorder: 8,
        bottomRightRoundBorder: 8,
        buttonHeight: 32,
        backgroundColor: "#97BF0D", 
        backgroundColorHover: "#218B29", 
        backgroundColorDisabled: "#D7D7D7", 
        borderColor: "#DFDFDF",
        borderSize: 0,
        fontFamily: "SourceSansProRegular",
        fontSize: 16,        
        fontColor: "#FFFFFF", 
        fontColorDisabled: "#B7B7B7", 
    },
    
    //Redondos (pistas)
    imageRoundButton: {
        backgroundColor: "#97BF0D", 
        backgroundColorHover: "#218B29", 
        backgroundColorDisabled: "#D7D7D7",  
        borderColor: "#FFFFCC",
        borderSize: 3,
        fontFamily: "smsymbolregular",
        fontSize: 38,        
        fontColor: "#FFFFFF", 
        fontColorDisabled: "#FFFFFF", 
        radius: 28,
    },
    
//BARRA NAVEGACION
    navigationBar: {
        fontSize: 18,        
        size: 32,
        offset: 3,
        objectDistanceBorder: 12,
        titleFontColor: "#000000",
        buttonLeft: {
            topLeftRoundBorder: 8, 
            topRightRoundBorder: 8,
            bottomLeftRoundBorder: 8,
            bottomRightRoundBorder: 8, 
            backgroundColor: "#999999", 
            backgroundColorHover: "#218B29", 
            backgroundColorDisabled: "#D7D7D7", 
            fontFamily: "smsymbolregular",
            fontSize: 16,        
            fontColor: "#FFFFFF", 
            fontColorDisabled: "#B7B7B7", 
        },
        buttonCenter: {
            topLeftRoundBorder: 8, 
            topRightRoundBorder: 8,
            bottomLeftRoundBorder: 8,
            bottomRightRoundBorder: 8, 
            backgroundColor: "#999999", 
            backgroundColorHover: "#999999", 
            backgroundColorDisabled: "#999999", 
            fontFamily: "smsymbolregular",
            fontSize: 16,        
            fontColor: "#FFFFFF", 
            fontColorDisabled: "#FFFFFF",             
        },
        buttonRight: {
            topLeftRoundBorder: 8, 
            topRightRoundBorder: 8,
            bottomLeftRoundBorder: 8,
            bottomRightRoundBorder: 8, 
            backgroundColor: "#999999", 
            backgroundColorHover: "#218B29", 
            backgroundColorDisabled: "#D7D7D7", 
            fontFamily: "smsymbolregular",
            fontSize: 16,        
            fontColor: "#FFFFFF", 
            fontColorDisabled: "#B7B7B7",              
        },
    },
    
//BARRA SELECCIÓN
    selectionBar: {
        buttonSize: 32,
        offset: 5,
        button: {
            topLeftRoundBorder: 16,
            topRightRoundBorder: 16,
            bottomLeftRoundBorder: 16,
            bottomRightRoundBorder: 16,
            backgroundColor: "#999999",
            backgroundColorHover: "#218B29",
            backgroundColorDisabled: "#D7D7D7",
            fontFamily: "smsymbolregular",
            fontSize: 16,
            fontColor: "#FFFFFF",
            fontColorDisabled: "#B7B7B7",
        }, 
    },
    
//CARTELA INFO
    infoPopupBig: {
        fontSize: 18,        
        fontColor: "#000000",
        backgroundColor: "#FFF8C9",
        borderComplete: false,
        borderSize: 1,
        roundBorder: 0,
        hasTopBorder: true,
        hasBottomBorder: true,
        borderColor: "#97BF0D",
        offset: 10,
    },

//BOCADILLO
    speechBubble: {
        backgroundColor: "#FFFFFF",
        borderSize: 2,
        borderColor: "#757575",
        roundBorder: 5,
        triangleSize: 10,
    },
    
    //GRID
    grid: {
        borderShapeColor: "#000000",
        shapeColor: "#FFFFFF"
    },
    
    //TECLADO
    keyboard:
    {
        opacity: 0.8,
        buttonSize: 32,
        buttonRoundBorder: 8,

        buttonAlpha: {
            borderColor: "#97BF0D",
            backgroundColor: "#F9FF80",
            backgroundColorHover: "#218B29",
            backgroundColorDisabled: "#FF6600",
            shadowSize: 3,
            shadowColor: "#000000",
            fontFamily: "SourceSansProRegular",
            fontSize: 16,
            fontColor: "#333333",
            fontColorDisabled: "#B7B7B7",            
        },

        buttonNumeric: {
            borderColor: "#97BF0D",
            backgroundColor: "#EFD347",
            backgroundColorHover: "#218B29",
            backgroundColorDisabled: "#33CC00",
            shadowSize: 3,
            shadowColor: "#000000",
            fontFamily: "SourceSansProRegular",
            fontSize: 16,
            fontColor: "#333333",
            fontColorDisabled: "#B7B7B7",
        },

        buttonSymbol: {
            borderColor: "#97BF0D",
            backgroundColor: "#DEBD19",
            backgroundColorHover: "#218B29",
            backgroundColorDisabled: "#33CC00",            
            shadowSize: 3,
            shadowColor: "#000000",
            fontFamily: "SourceSansProRegular",
            fontSize: 16,
            fontColor: "#333333",
            fontColorDisabled: "#B7B7B7",
        },
        
        buttonSpecial: {
            borderColor: "#97BF0D",
            backgroundColor: "#DEBD19",
            backgroundColorHover: "#218B29",
            backgroundColorDisabled: "#33CC00",            
            shadowSize: 3,
            shadowColor: "#000000",
            fontFamily: "smsymbolregular",
            fontSize: 25,
            fontColor: "#333333",
            fontColorDisabled: "#B7B7B7",
        }         
    },
    
    // KARAOKES
    karaoke: {
        mainMenu: {
            button: {
                topLeftRoundBorder: 8, 
                topRightRoundBorder: 8,
                bottomLeftRoundBorder: 8,
                bottomRightRoundBorder: 8,
                buttonHeight: 32,
                backgroundColor: "#97BF0D", 
                backgroundColorHover: "#218B29", 
                backgroundColorDisabled: "#D7D7D7", 
                fontFamily: "SourceSansProBold",
                fontSize: 24,        
                fontColor: "#FFFFFF", 
                fontColorDisabled: "#B7B7B7", 
            },            
        },
        lyrics: {
            gradientColor: "#218B29",
            fontFamily: "SourceSansProBold",
            fontSize: 28,
            fontColor: "#FFFFFF",
            box: {
                backgroundColor: "#97BF0D",
                borderColor: "#218B29",
            },
        },
        backButton: {
                topLeftRoundBorder: 8, 
                topRightRoundBorder: 8,
                bottomLeftRoundBorder: 8,
                bottomRightRoundBorder: 8,
                buttonHeight: 32,
                backgroundColor: "#97BF0D", 
                backgroundColorHover: "#218B29", 
                backgroundColorDisabled: "#D7D7D7", 
                fontFamily: "SourceSansProBold",
                fontSize: 24,        
                fontColor: "#FFFFFF", 
                fontColorDisabled: "#B7B7B7", 
            }, 
        buttonGroup1: {
            backgroundColor: "#007AFF",
            borderColor: "#9B599B",
        },
        buttonGroup2: {
            backgroundColor: "#FF0000",
            borderColor: "#9B599B",
        },  
        noteColor: "#218B29",
    },
    
    // MEDIACONTROLBAR
    mediaControlBar: {
        height: 40,
        roundCorners: 7,
        color1: "#97BF0D",
        color2: "#218B29",
        color3: "#FFFFFF",
        fontSize: 26,
    },
};
