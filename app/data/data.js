var library = [
  {
    id:'naturaleza-4df5-54ge-df55-3ft',
    title:'Ciencias de la naturaleza.',
    level: '1º ESO',
    type:'book',
    theme:'green',
    num: 1,
    content:'<h1>Introducción</h1><p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis gravida eget neque vel vulputate.</p>',
    thumb:'natu-bg.jpg',
    elems: [
      {
        id: 'lasmaquina-4df5-54ge-df55-3ft',
        title:'Las máquinas',
        type:'unit',
        thumb:'f.jpg',
        percent: 100,
        elems: [
          {
            id: 'introduccion-4df5-54ge-df55-3ft',
            title:'¡Empezamos!',
            type:'intro',
            num:'0',
            embed: [
            ],
            content:
                '<p>' +
                  'Integer at faucibus urna. Nullam condimentum leo id elit sagittis auctor. Curabitur elementum nunc a leo imperdiet, nec elementum diam elementum. Etiam elementum euismod commodo.' +
                '</p>',
            summary:
              '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
              'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
              'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
              'Duis gravida eget neque vel vulputate.</p>',
            notes: [
              {
                id: 'note-4df5-54ge-df55-3ft-1',
                bookmark: 1,
                title: 'Título del apunte',
                subtitle: 'Subtítulo del apunte',
                html: ''
              }
            ],
            elems: [
              {
                id: 'los-disparates-4df5-54ge-df55-3ft',
                title: 'Juego: Los disparates',
                type: 'section',
                embed: [
                  {
                    id: 'sm_050101_51_es',
                    type: 'html-activity',
                    num:'1',
                    title: 'Juego: Los disparates.',
                    elems: [
                      {id: 'sm_050101_imagen1', type: 'image', url: 'resources/sm_050101_imagen1.png'},
                      {id: 'sm_050101_imagen2', type: 'image', url: 'resources/sm_050101_imagen2.png'},
                      {id: 'sm_050101_imagen3', type: 'image', url: 'resources/sm_050101_imagen3.png'},
                      {id: 'sm_050101_solucion1', type: 'image', url: 'resources/sm_050101_solucion1.png'},
                      {id: 'sm_050101_solucion1', type: 'image', url: 'resources/sm_050101_solucion2.png'},
                      {id: 'sm_050101_solucion1', type: 'image', url: 'resources/sm_050101_solucion3.png'},
                      {id: 'help-circle', type: 'image', url: 'icons/help-circle.svg'},
                    ],
                    content:
                        '<p class="activity-title">' +
                        'En las siguientes imágenes hay cosas que no cuadran… Busca y encuentra qué es lo que falla ' +
                        'y averigua qué ocurrirá en la última viñeta.' +
                        '</p>' +
                        '<div class="activity-content">' +
                        '<div class="comic-sequence">' +
                        '<div class="comic-card"><img alt="" src="{0}"></div>' +
                        '<div class="comic-card"><img alt="" src="{1}"></div>' +
                        '<div class="comic-card"><img alt="" src="{2}"></div>' +
                        '<div class="comic-card mini container"><div class="comic-card-text"><img alt="" src="{6}"><p>¿Qué ocurrirá en esta viñeta?</p></div></div>' +
                        '<div class="comic-card mini"><img alt="" src="{3}"></div>' +
                        '<div class="comic-card mini"><img alt="" src="{4}"></div>' +
                        '<div class="comic-card mini"><img alt="" src="{5}"></div>' +
                        '</div>' +
                        '<span class="clear"></span>' +
                        '<p>Es mejor que las carretillas tengan una rueda redonda / triangular / cuadrada porque es ' +
                        'la mejor forma para que nos ayude a mover los elementos pesados.</p>' +
                        '</div>'
                  }
                ],
                content:
                    '<div class="sm-section-text">' +
                    '<p>{0}</p>' +
                    '</div>'
              },
              {
                id: 'interactive-4df5-54ge-df55-3ft',
                title: 'Lo que ya sé',
                type: 'section',
                embed: [
                  {
                    id: 'civilRomana', thumb:'resources/interactivo.png', type: 'activity', minHeight: '80%',
                    buttonText: '¡Adelante!', header:'¿Qué sabes sobre las máquinas?',
                    subheader:'Demuestra todo lo que sabes sobre las máquinas con este pequeño reto',
                    url: 'activities/Que_sabes_sobre_las_maquinas/index.html'
                  }
                ],
                content: '<div class="sm-section-text">' +
                    '{0}' +
                    '</div>',
              },
            ]
          },
          {
              id: 'maquinas-por-todas-partes-4df5-54ge-df55-3ft',
              title:'Máquinas por todas partes',
              type:'epigraph',
              num:'1',
              embed: [],
              summary:
                  '<div class="sm-section-text">' +
                  '<p>Las máquinas nos ayudan a realizar las tareas con menos esfuerzo.</p>' +
                  '<p class="sm-section-notebook-content">Definición de máquina euismod pretium, nisi tellus eleifend ' +
                  'odio, luctus viverra sem dolor id sem. Maecenas a venenatis.</p>' +
                  '<p>Las máquinas sencillas, tienen pocos elementos, las llamamos máquinas simples. Las ' +
                  'máquinas más complicadas, tienen bastantes elementos y las llamamos máquinas compuestas.</p>' +
                  '<p>Para que una máquina funcione necesita energía. Según el tipo de energía que utilizan para ' +
                  'funcionar las máquinas pueden ser:</p>' +
                  '<div class="sm-accordion" id="sm_accordion050101_07_es-1">' +
                  '<div class="card">' +
                  '<div class="card-header" id="headingOne">' +
                  '<a data-toggle="collapse" class="collapsed" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">' +
                  '<h5 class="mb-0">Máquinas manuales</h5>' +
                  '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                  '</a>' +
                  '</div>' +
                  '<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#sm_accordion050101_07_es-1">' +
                  '<div class="card-body">' +
                  '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                  '</div>' +
                  '</div>' +
                  '</div>' +
                  '<div class="card">' +
                  '<div class="card-header" id="headingTwo">' +
                  '<a data-toggle="collapse" class="collapsed" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">' +
                  '<h5 class="mb-0">Máquinas eléctricas</h5>' +
                  '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                  '</a>' +
                  '</div>' +
                  '<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#sm_accordion050101_07_es-1">' +
                  '<div class="card-body">' +
                  '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                  '</div>' +
                  '</div>' +
                  '</div>' +
                  '<div class="card">' +
                  '<div class="card-header" id="headingThree">' +
                  '<a data-toggle="collapse" class="collapsed" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">' +
                  '<h5 class="mb-0">Máquinas térmicas</h5>' +
                  '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                  '</a>' +
                  '</div>' +
                  '<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#sm_accordion050101_07_es-1">' +
                  '<div class="card-body">' +
                  '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                  '</div>' +
                  '</div>' +
                  '</div>' +
                  '</div>' +
                  '</div>',
              elems: [
                {
                  id: 'que-es-una-maquina-4df5-54ge-df55-3ft',
                  title: 'Qué es una máquina',
                  type: 'section',
                  minutes: 0,
                  embed: [
                    {id: 'sm_050101_02_es', type: 'image', url: 'resources/sm_050101_02_es.jpg'},
                    {id: 'sm_050101_03_es', type: 'image', url: 'resources/sm_050101_03_es.jpg'},
                    {id: 'sm_050101_04_es', type: 'image', url: 'resources/sm_050101_04_es.jpg'},
                    {id: 'sm_050101_05_es', type: 'image', url: 'resources/sm_050101_05_es.jpg'},
                    {id: 'sm_050101_06_es', type: 'image', url: 'resources/sm_050101_06_es.jpg'},
                    {
                      id: 'sm_050101_50_es',
                      type: 'html-activity',
                      num:'2',
                      title:'Adjunta una imagen a 3 nuevas máquinas.',
                      content:
                          '<p class="activity-title">¿Conoces alguna máquina distinta? ¿Cuáles?. ' +
                          'Escribe su nombre y adjunta una imagen que identifique 3 nuevas máquinas.</p>' +
                          '<ul class="sm-activity-img-input list-group list-group-horizontal">' +
                          '<li class="list-group-item border-0">' +
                          '<div class="card">' +
                          '<div class="card-body">' +
                          '<div class="sm-activity-drop-img text-center">' +
                          '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                          '<input type="file" class="input-file">' +
                          '</div>' +
                          '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                          '</div>' +
                          '</li>' +
                          '<li class="list-group-item border-0">' +
                          '<div class="card">' +
                          '<div class="card-body">' +
                          '<div class="sm-activity-drop-img text-center">' +
                          '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                          '<input type="file" class="input-file">' +
                          '</div>' +
                          '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                          '</div>' +
                          '</li>' +
                          '<li class="list-group-item border-0">' +
                          '<div class="card">' +
                          '<div class="card-body">' +
                          '<div class="sm-activity-drop-img text-center">' +
                          '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                          '<input type="file" class="input-file">' +
                          '</div>' +
                          '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                          '</div>' +
                          '</li>' +
                          '</ul>'
                    }
                  ],
                  content:
                      '<div class="sm-section-text">' +
                      '<p>' +
                      'Estamos rodeados de máquinas, como las que aparecen ' +
                      'en estas imágenes. ¿Cuáles más conoces?' +
                      '</p>' +
                      '<div class="sm-carousel">' +
                      '<ul>' +
                      '<li>{0}</li>' +
                      '<li>{1}</li>' +
                      '<li>{2}</li>' +
                      '<li>{3}</li>' +
                      '<li>{4}</li>' +
                      '</ul>' +
                      '</div>' +
                      '<p>' +
                      'Las máquinas nos ayudan a realizar las tareas con menos esfuerzo. ' +
                      'Algunas máquinas son muy sencillas, como las tijeras o las pinzas. ' +
                      'Y otras son más complicadas, como la lavadora, el coche o el ordenador.' +
                      '</p>' +
                      '{5}' +
                      '</div>'
                },
                {
                  id: 'clasificamos-maquinas-4df5-54ge-df55-3ft',
                  title: 'Clasificamos las máquinas',
                  type: 'section',
                  minutes: 0,
                  embed: [
                    {
                      id: 'sm_050101_52_es',
                      type: 'html-activity',
                      num:'3',
                      title: 'Clasifica máquinas según sean simples o compuestas.',
                      elems: [
                        {id: 'sm_050101_09_es', type: 'image', url: 'resources/sm_050101_09_es.jpg'},
                        {id: 'sm_050101_10_es', type: 'image', url: 'resources/sm_050101_10_es.jpg'},
                        {id: 'sm_050101_11_es', type: 'image', url: 'resources/sm_050101_11_es.jpg'},
                        {id: 'sm_050101_12_es', type: 'image', url: 'resources/sm_050101_12_es.jpg'},
                        {id: 'sm_050101_13_es', type: 'image', url: 'resources/sm_050101_13_es.jpg'}
                      ],
                      content:
                          '<p class="activity-title">' +
                          'En grupo, clasificamos las máquinas según sean simples o compuestas. ' +
                          'Haz clic o arrastra cada imagen hasta su categoría.' +
                          '</p>' +
                          '<div class="sm-activity-drag-drop">' +
                          '<div class="sm-activity-drag">' +
                          '<div class="block-drag" data-value="simple"><img alt="" src="{0}"></div>' +
                          '<div class="block-drag" data-value="complex"><img alt="" src="{1}"></div>' +
                          '<div class="block-drag" data-value="complex"><img alt="" src="{2}"></div>' +
                          '<div class="block-drag" data-value="complex"><img alt="" src="{3}"></div>' +
                          '<div class="block-drag" data-value="simple"><img alt="" src="{4}"></div>' +
                          '</div>' +
                          '<div class="sm-activity-drop">' +
                          '<div class="block-drop" data-value="simple"></div>' +
                          '<div class="block-drop" data-value="complex"></div>' +
                          '</div>' +
                          '</div>'
                    }
                  ],
                  content:
                    '<div class="sm-section-text">' +
                      '<p>{0}</p>' +
                      '<p>' +
                        'Las máquinas más sencillas tienen pocas piezas y reciben el nombre de máquinas simples. ' +
                        'Las máquinas más complicadas tienen bastantes piezas y las llamamos máquinas compuestas.' +
                      '</p>' +
                    '</div>'
                },
                {
                  id: 'como-funcionan-maquinas-3df5-54ge-df55-3ft',
                  title: '¿Cómo funcionan las máquinas?',
                  type: 'section',
                  minutes: 0,
                  embed: [
                      {id: 'sm_050101_07_es', type: 'video', thumb:'resources/video1.png', url: 'resources/sm_050101_07_es.mp4'}
                  ],
                  content:
                      '<div class="sm-section-text">' +
                      '<div class="activity-content">' +
                      '<p>{0}</p>' +
                      '<p>Las máquinas se pueden clasificar según el tipo de energía que necesiten en máquinas <b>manuales, eléctricas y térmicas.</b></p>' +
                      '</div>' +
                      '</div>'
                },
                {
                  id: 'actividades_1-4df5-54ge-df55-3ft',
                  title: 'Actividades',
                  type: 'section',
                  embed: [
                    {
                      id: 'selImages', thumb:'resources/interactivo2.png', type: 'activity', minHeight: '60%',
                      buttonText: '¡Adelante!', header:'Actividades',
                      subheader:'Demuestra todo lo que has aprendido sobre las máquinas resolviendo estas actividades',
                      icon:'icons/star.svg', linkText: 'Descubre lo que aprenderás con estas actividades',
                      url: 'activities/3_pri_ccnna_msav_esp_ud06_pro_mad_fun_ma/index.html'
                    }
                  ],
                  content:
                    '<div class="sm-section-text">' +
                    '<p>{0}</p>' +
                    '<div class="sm-section-notebook-content">' +
                    '<div class="section-icon svg-button loader"><img src="icons/notebook.svg" height="50" width="50"></div>' +
                    '<div class="sm-section-subtitle"><h2>Actividades para el cuaderno</h2></div>' +
                    '<div class="sm-section-subtitle"><p>Responde a estas actividades en tu cuaderno.</p></div>' +
                    '<ol  class="activity-list-number">' +
                    '<li>' +
                    '<span class="dot-circle">1</span>' +
                    'Elige una máquina simple y explica en qué tarea nos ayuda.' +
                    'Haz lo mismo con una máquina compuesta.' +
                    '</li>' +
                    '<li>' +
                    '<span class="dot-circle">2</span>' +
                    'Escribe todas las máquinas que has utilizado hoy antes de llegar a tu centro. ' +
                    'Elige la que más te gusta y escribe si es una máquina simple o compuesta.' +
                    '</li>' +
                    '<li>' +
                    '<span class="dot-circle">3</span>' +
                    'Aenean sed nibh a magna posuere tempor. Nunc faucibus pellentesque nunc in aliquet. ' +
                    'Donec congue, nunc vel tempor congue, enim sapien lobortis ipsum, ' +
                    'in volutpat sem ex in ligula. Nunc purus est, consequat condimentum faucibus sed, ' +
                    'iaculis sit amet massa. Fusce ac condimentum turpis. ' +
                    '</li>' +
                    '</ol></div>' +
                    '</div>'
                },
                {
                  id: 'para-estudiar-4df5-54ge-df55-3ft',
                  title: 'Para estudiar',
                  type: 'section',
                  embed: [],
                  content:
                      '<div class="sm-section-text">' +
                      '<p>Las máquinas nos ayudan a realizar las tareas con menos esfuerzo.</p>' +
                      '<p class="sm-section-highlight-content">Definición de máquina euismod pretium, nisi tellus eleifend ' +
                      'odio, luctus viverra sem dolor id sem. Maecenas a venenatis.</p>' +
                      '<p>Las máquinas sencillas, tienen pocos elementos, las llamamos máquinas simples. Las ' +
                      'máquinas más complicadas, tienen bastantes elementos y las llamamos máquinas compuestas.</p>' +
                      '<p>Para que una máquina funcione necesita energía. Según el tipo de energía que utilizan para ' +
                      'funcionar las máquinas pueden ser:</p>' +
                      '<div class="sm-accordion" id="sm_accordion050101_07_es-1">' +
                      '<div class="card">' +
                      '<div class="card-header" id="headingOne">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">' +
                      '<h5 class="mb-0">Máquinas manuales</h5>' +
                      '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                      '</div>' +
                      '<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                      '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '<div class="card">' +
                      '<div class="card-header" id="headingTwo">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">' +
                      '<h5 class="mb-0">Máquinas eléctricas</h5>' +
                      '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                      '</div>' +
                      '<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                      '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '<div class="card">' +
                      '<div class="card-header" id="headingThree">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">' +
                      '<h5 class="mb-0">Máquinas térmicas</h5>' +
                      '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                      '</div>' +
                      '<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                      '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '</div>',
                }
              ],
              notes: [
                {
                  id: 'note-4df5-54ge-df55-3ft-2',
                  bookmark: 2,
                  epigraph: 'maquinas-por-todas-partes-4df5-54ge-df55-3ft',
                  title: 'Visita a la fábrica de Ford',
                  subtitle: 'Seminario en inglés',
                  html:
                    '<h2>TECNOLOGÍA FORD</h2><h3>Conectividad</h3><p class="">FordPass Connect integrado en el vehículo que te permiten permanecer conectado con tu coche cuando estás en casa. Esta tecnología te abrirá un mundo de posibilidades de conectividad que son aún más potentes si las combinas con la aplicación FordPass.<br></p><h3>El motor</h3><p class="">La inyección directa ayuda a los motores Ford EcoBoost a alcanzar estas cifras tan notables. Una cantidad precisa y altamente presurizada de combustible se inyecta directamente en la cámara de combustión. Esta forma innovadora y eficaz de utilizar combustible significa que los motores Ford EcoBoost son capaces de utilizar muy poco combustible en casos como el control de crucero adaptativo.</p>' +
                    '<div class="medium-insert-embeds" contenteditable="false"><figure>' +
                    '<div class="medium-insert-embed">' +
                    '<div style="left: 0; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;"><iframe src="https://www.youtube.com/embed/klnoes5kC94?rel=0" style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;" allowfullscreen="" scrolling="no" allow="encrypted-media; accelerometer; gyroscope; picture-in-picture"></iframe></div>' +
                    '</div>' +
                    '</figure></div>'
                }
              ]
          },
          {
            id: 'maquinas-simples-4df8-54ge-df55-3ft',
            title:'Las máquinas simples',
            type:'epigraph',
            num:'2',
            embed: [
            ],
            summary:
              '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
              'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
              'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
              'Duis gravida eget neque vel vulputate.</p><img alt="" src="resources/sm_050101_04_es.jpg">',
            elems: [
              {
                id: 'nos-preguntamos-4df5-54ge-df55-3ft',
                title: 'Nos preguntamos',
                type: 'section',
                embed: [
                  {id: 'sm_video2', type: 'video', thumb:'resources/video2.png', url: 'resources/video2.mp4'}
                ],
                content:
                    '<div class="activity-content">' +
                    '{0}' +
                    '<p>María, Sergio y Antonio no podían mover la piedra por eso casi sin darse cuenta han fabricado una maquina simple. ¡Han fabricado una palanca!</b></p>' +
                    '</div>',
              },
              {
                id: 'maq-simples-4df5-54ge-df55-3ft',
                title: 'Las máquinas simples',
                type: 'section',
                embed: [
                  {id: 'sm_video3', type: 'video', thumb:'resources/video3.png', url: 'resources/video3.mp4'}
                ],
                content:
                    '<div class="sm-section-text">' +
                    '<div class="activity-content">' +
                    '{0}' +
                    '<p>Las máquinas simples más comunes son la palanca, la rueda, la polea y el plano inclinado o rampa.</b></p>' +
                    '</div>' +
                    '</div>',
              },
              {
                id: 'maquina_uso-4df5-54ge-df55-3ft',
                title: 'Cada máquina con su uso',
                type: 'section',
                /* learnosity_data: [
                  {
                    response_id: "pepe",
                    type: "mcq",
                    options: [
                      {
                        value: "0",
                        label: "Tipo 1"
                      },
                      {
                        value: "1",
                        label: "Tipo 2"
                      },
                      {
                        value: "2",
                        label: "Tipo 3"
                      }
                    ],
                    valid_responses: [
                      {
                        value: "1",
                        score: 1
                      }
                    ],
                    instant_feedback: true,
                    multiple_responses: false,
                    penalty_score: -1
                  }
                ], */
                embed: [
                  {
                    id: 'sm_050101_55_es',
                    type: 'html-activity',
                    num:'4',
                    title: 'Cada máquina con su uso.',
                    elems: [
                      {id: 'sm_050101_imagen1', type: 'image', url: 'resources/sm_050101_captura_0.jpg'},
                    ],
                    content:
                        '<p class="activity-title">' +
                        'Selecciona el tipo de uso que tiene cada una de las máquinas que aparecen en las imágenes' +
                        '</p>' +
                        '<div class="activity-content">' +
                        '<div class="comic-sequence">' +
                        '<div class="comic-card"><img alt="" src="{0}"></div>' +
                        '<div class="comic-card more-padding">' +
                        /* '<span class="learnosity-response question-pepe"></span>' + */
                        '<input type="radio" name="gender" value="male"> Tipo 1<br>\n' +
                        '<input type="radio" name="gender" value="female"> Tipo 2<br>\n' +
                        '<input type="radio" name="gender" value="other"> Tipo 3</div>' +
                        '</div>' +
                        '<span class="clear"></span>' +
                        '</div>'
                  }
                ],
                content:
                    '<div class="sm-section-text">' +
                    '<p>{0}</p>' +
                    '</div>'
              },
              {
                id: 'actividades_2-4df5-54ge-df55-3ft',
                title: 'Actividades',
                type: 'section',
                embed: [
                  {
                    id: 'inventHist', thumb:'resources/interactivo3.png', type: 'activity', minHeight: '80%',
                    buttonText: '¡Adelante!', header:'Actividades',
                    subheader:'Demuestra todo lo que has aprendido sobre las máquinas resolviendo estas actividades.',
                    icon:'icons/star.svg', linkText: 'Descubre lo que aprenderás con estas actividades',
                    url: 'activities/parate_a_pensar/index.html'
                  }
                ],
                content:
                  '<div class="sm-section-text">' +
                  '<p>{0}</p>' +
                  '<div class="sm-section-notebook-content">' +
                  '<div class="section-icon svg-button loader"><img src="icons/notebook.svg" height="50" width="50"></div>' +
                  '<div class="sm-section-subtitle"><h2>Actividades para el cuaderno</h2></div>' +
                  '<div class="sm-section-subtitle"><p>Responde a estas actividades en tu cuaderno.</p></div>' +
                  '<ol  class="activity-list-number">' +
                  '<li>' +
                  '<span class="dot-circle">1</span>' +
                  'Elige una máquina simple y explica en qué tarea nos ayuda.' +
                  'Haz lo mismo con una máquina compuesta.' +
                  '</li>' +
                  '<li>' +
                  '<span class="dot-circle">2</span>' +
                  'Escribe todas las máquinas que has utilizado hoy antes de llegar a tu centro. ' +
                  'Elige la que más te gusta y escribe si es una máquina simple o compuesta.' +
                  '</li>' +
                  '<li>' +
                  '<span class="dot-circle">3</span>' +
                  'Aenean sed nibh a magna posuere tempor. Nunc faucibus pellentesque nunc in aliquet. ' +
                  'Donec congue, nunc vel tempor congue, enim sapien lobortis ipsum, ' +
                  'in volutpat sem ex in ligula. Nunc purus est, consequat condimentum faucibus sed, ' +
                  'iaculis sit amet massa. Fusce ac condimentum turpis. ' +
                  '</li>' +
                  '</ol></div>' +
                  '</div>'
              }
            ],
            notes: [
              {
                id: 'note-4df5-54ge-df55-3ft-3',
                bookmark: 2,
                epigraph: 'maquinas-simples-4df8-54ge-df55-3ft',
                title: 'Actividad en el circuito ....',
                subtitle: 'Proyecto de clase',
                html: ''
              }
            ]
          },
          {
              id: 'compuestas-4df5-54ge-df55-3ft',
              title:'Máquinas compuestas y sus usos',
              type:'epigraph',
              num:'3',
              embed: [],
              content:'<div class="sm-section-text"></div>',
              summary:
                '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
                'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
                'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                'Duis gravida eget neque vel vulputate.</p>',
              elems: []

          },
          {
              id: 'inventos-4df5-54ge-df55-3ft',
              title:'Los inventos cambian nuestras vidas',
              type:'epigraph',
              num:'4',
              embed: [],
              content:'<div class="sm-section-text"></div>',
              summary:
                '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
                'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
                'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                'Duis gravida eget neque vel vulputate.</p>',
              elems:[]
          }
        ]
      }
    ]
  }
];