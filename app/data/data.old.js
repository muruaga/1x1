var library = [
  {
    id:'naturaleza-4df5-54ge-df55-3ft',
    title:'Ciencias de la naturaleza.',
    level: '1º ESO',
    type:'book',
    theme:'green',
    num: 1,
    content:'<h1>Introducción</h1><p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis gravida eget neque vel vulputate.</p>',
    thumb:'natu-bg.jpg',
    elems: [
      {
        id: 'lasmaquina-4df5-54ge-df55-3ft',
        title:'Las máquinas',
        type:'unit',
        thumb:'f.jpg',
        percent: 100,
        elems: [
          {
            id: 'introduccion-4df5-54ge-df55-3ft',
            title:'¡Nos activamos!',
            type:'intro',
            num:'',
            embed: [
              {id: 'sm_050101_00_es', type: 'image', url: 'resources/sm_050101_00_es.jpg'}
            ],
            content:
                '<p>' +
                  'Juego de los disparates, de las 7 diferencias, de busca y encuentra, ' +
                  'adivina que pasará en la viñeta que falta, adivinar los elementos que ' +
                  'faltan en una escena, qué imagen no cuadra…' +
                '</p>' +
                '{0}',
            summary:
              '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
              'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
              'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
              'Duis gravida eget neque vel vulputate.</p>',
            elems: [],
            notes: [
              {
                id: 'note-4df5-54ge-df55-3ft-1',
                bookmark: 1,
                title: 'Título del apunte',
                subtitle: 'Subtítulo del apunte',
                html: ''
              }
            ]
          },
          {
              id: 'maquinas-por-todas-partes-4df5-54ge-df55-3ft',
              title:'Máquinas por todas partes',
              type:'epigraph',
              num:'1',
              embed: [
                {id: 'sm_050101_02_es', type: 'image', url: 'resources/sm_050101_02_es.jpg'},
                {id: 'sm_050101_03_es', type: 'image', url: 'resources/sm_050101_03_es.jpg'},
                {id: 'sm_050101_04_es', type: 'image', url: 'resources/sm_050101_04_es.jpg'},
                {id: 'sm_050101_05_es', type: 'image', url: 'resources/sm_050101_05_es.jpg'},
                {id: 'sm_050101_06_es', type: 'image', url: 'resources/sm_050101_06_es.jpg'},
                {
                  id: 'sm_050101_50_es',
                  type: 'html-activity',
                  num:'1',
                  title:'Adjunta una imagen a 3 nuevas máquinas.',
                  content:
                    '<p class="activity-title">¿Conoces alguna máquina distinta? ¿Cuáles?. ' +
                    'Escribe su nombre y adjunta una imagen que identifique 3 nuevas máquinas.</p>' +
                    '<ul class="sm-activity-img-input list-group list-group-horizontal">' +
                      '<li class="list-group-item border-0">' +
                        '<div class="card">' +
                          '<div class="card-body">' +
                            '<div class="sm-activity-drop-img text-center">' +
                              '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                              '<input type="file" class="input-file">' +
                            '</div>' +
                            '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                        '</div>' +
                      '</li>' +
                      '<li class="list-group-item border-0">' +
                        '<div class="card">' +
                          '<div class="card-body">' +
                            '<div class="sm-activity-drop-img text-center">' +
                              '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                              '<input type="file" class="input-file">' +
                            '</div>' +
                            '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                        '</div>' +
                      '</li>' +
                      '<li class="list-group-item border-0">' +
                        '<div class="card">' +
                          '<div class="card-body">' +
                            '<div class="sm-activity-drop-img text-center">' +
                              '<span class="icon svg-button"><i data-feather="image"></i></span>' +
                              '<input type="file" class="input-file">' +
                            '</div>' +
                            '<input class="p-1 text-center" placeholder="Nombre máquina">' +
                          '</div>' +
                        '</div>' +
                      '</li>' +
                    '</ul>'
                }
              ],

              content:
                '<div class="sm-section-text">' +
                  '<p>' +
                    'Estamos rodeados de máquinas, como las que aparecen ' +
                    'en estas imágenes. ¿Cuáles más conoces?' +
                  '</p>' +
                  '<div class="sm-carousel">' +
                    '<ul>' +
                      '<li>{0}</li>' +
                      '<li>{1}</li>' +
                      '<li>{2}</li>' +
                      '<li>{3}</li>' +
                      '<li>{4}</li>' +
                    '</ul>' +
                  '</div>' +
                  '<p>' +
                    'Las máquinas nos ayudan a realizar las tareas con menos esfuerzo. ' +
                    'Algunas máquinas son muy sencillas, como las tijeras o las pinzas. ' +
                    'Y otras son más complicadas, como la lavadora, el coche o el ordenador.' +
                  '</p>' +
                  '{5}' +
                '</div>',
              summary:
                '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
                'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
                'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                'Duis gravida eget neque vel vulputate.</p><img alt="" src="resources/sm_050101_12_es.jpg">',
              elems: [
                {
                  id: 'todo-juntos-4df5-54ge-df55-3ft',
                  title: 'Todos juntos',
                  type: 'section',
                  minutes: 0,
                  embed: [
                    {
                      id: 'sm_050101_51_es',
                      type: 'html-activity',
                        num:'2',
                      title: 'Clasifica máquinas según sean simples o compuestas.',
                      elems: [
                        {id: 'sm_050101_09_es', type: 'image', url: 'resources/sm_050101_09_es.jpg'},
                        {id: 'sm_050101_10_es', type: 'image', url: 'resources/sm_050101_10_es.jpg'},
                        {id: 'sm_050101_11_es', type: 'image', url: 'resources/sm_050101_11_es.jpg'},
                        {id: 'sm_050101_12_es', type: 'image', url: 'resources/sm_050101_12_es.jpg'},
                        {id: 'sm_050101_13_es', type: 'image', url: 'resources/sm_050101_13_es.jpg'}
                      ],
                      content:
                         '<p class="activity-title">' +
                          'En grupo, clasificamos las máquinas según sean simples o compuestas. ' +
                          'Haz clic o arrastra cada imagen hasta su categoría.' +
                        '</p>' +
                        '<div class="sm-activity-drag-drop">' +
                          '<div class="sm-activity-drag">' +
                            '<div class="block-drag" data-value="simple"><img alt="" src="{0}"></div>' +
                            '<div class="block-drag" data-value="complex"><img alt="" src="{1}"></div>' +
                            '<div class="block-drag" data-value="complex"><img alt="" src="{2}"></div>' +
                            '<div class="block-drag" data-value="complex"><img alt="" src="{3}"></div>' +
                            '<div class="block-drag" data-value="simple"><img alt="" src="{4}"></div>' +
                          '</div>' +
                          '<div class="sm-activity-drop">' +
                            '<div class="block-drop" data-value="simple"></div>' +
                            '<div class="block-drop" data-value="complex"></div>' +
                          '</div>' +
                        '</div>'
                    }
                  ],
                  content:
                    '<div class="sm-section-text">' +
                      '<p>{0}</p>' +
                      '<p>' +
                        'Las máquinas más sencillas tienen pocas piezas y reciben el nombre de máquinas simples. ' +
                        'Las máquinas más complicadas tienen bastantes piezas y las llamamos máquinas compuestas.' +
                      '</p>' +
                    '</div>'
                },

                {
                  id: 'actividades-cuaderno-4df5-54ge-df55-3ft',
                  title: 'Actividades para el cuaderno',
                  subtitle:'Responde a estas actividades en tu cuaderno.',
                  type: 'sectionNotebook',
                  embed: [],
                  content:
                    '<ol class="activity-list-number">' +
                      '<li>' +
                        '<span class="dot-circle">1</span>' +
                        'Elige una máquina simple y explica en qué tarea nos ayuda. ' +
                        'Haz lo mismo con una máquina compuesta.' +
                      '</li>' +
                      '<li>' +
                        '<span class="dot-circle">2</span>' +
                        'Escribe todas las máquinas que has utilizado hoy antes de llegar a tu centro. ' +
                        'Elige la que más te gusta y escribe si es una máquina simple o compuesta.' +
                      '</li>' +
                      '<li>' +
                        '<span class="dot-circle">3</span>' +
                        'Aenean sed nibh a magna posuere tempor. Nunc faucibus pellentesque nunc in aliquet. ' +
                        'Donec congue, nunc vel tempor congue, enim sapien lobortis ipsum, ' +
                        'in volutpat sem ex in ligula. Nunc purus est, consequat condimentum faucibus sed, ' +
                        'iaculis sit amet massa. Fusce ac condimentum turpis. ' +
                      '</li>' +
                    '</ol>'
                }
              ],
              notes: [
                {
                  id: 'note-4df5-54ge-df55-3ft-2',
                  bookmark: 2,
                  epigraph: 'maquinas-por-todas-partes-4df5-54ge-df55-3ft',
                  title: 'Visita a la fábrica de Ford',
                  subtitle: 'Seminario en inglés',
                  html:
                    '<h2>TECNOLOGÍA FORD</h2><h3>Conectividad</h3><p class="">FordPass Connect integrado en el vehículo que te permiten permanecer conectado con tu coche cuando estás en casa. Esta tecnología te abrirá un mundo de posibilidades de conectividad que son aún más potentes si las combinas con la aplicación FordPass.<br></p><h3>El motor</h3><p class="">La inyección directa ayuda a los motores Ford EcoBoost a alcanzar estas cifras tan notables. Una cantidad precisa y altamente presurizada de combustible se inyecta directamente en la cámara de combustión. Esta forma innovadora y eficaz de utilizar combustible significa que los motores Ford EcoBoost son capaces de utilizar muy poco combustible en casos como el control de crucero adaptativo.</p>' +
                    '<div class="medium-insert-embeds" contenteditable="false"><figure>' +
                    '<div class="medium-insert-embed">' +
                    '<div style="left: 0; width: 100%; height: 0; position: relative; padding-bottom: 56.25%;"><iframe src="https://www.youtube.com/embed/klnoes5kC94?rel=0" style="border: 0; top: 0; left: 0; width: 100%; height: 100%; position: absolute;" allowfullscreen="" scrolling="no" allow="encrypted-media; accelerometer; gyroscope; picture-in-picture"></iframe></div>' +
                    '</div>' +
                    '</figure></div>'
                }
              ]
          },
          {
            id: 'como-funciona-maquinas-4df5-54ge-df55-3ft',
            title:'¿Cómo funcionan las máquinas?',
            type:'epigraph',
            num:'2',
            embed: [
              {id: 'sm_050101_07_es', type: 'video', thumb:'resources/video1.png', url: 'resources/sm_050101_07_es.mp4'},
              {
                id: 'sm_050101_40_es',
                type: 'html-activity',
                  num:'3',
                title: 'Actividad vacía.',
                content: ''
              }
            ],
            content:
              '<div class="sm-section-text">' +
                '{0}' +
                '<p>' +
                  'Las máquinas necesitan energía. ' +
                  'Según la energía que utilicen pueden ser manuales, eléctricas o térmicas.' +
                '</p>' +
                '<div class="sm-accordion" id="sm_accordion050101_07_es-1">' +
                  '<div class="card">' +
                    '<div class="card-header" id="headingOne">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">' +
                        '<h5 class="mb-0">Máquinas manuales</h5>' +
                        '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                    '</div>' +
                    '<div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                        '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                  '<div class="card">' +
                    '<div class="card-header" id="headingTwo">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">' +
                        '<h5 class="mb-0">Máquinas eléctricas</h5>' +
                        '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                    '</div>' +
                    '<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                        '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                  '<div class="card">' +
                    '<div class="card-header" id="headingThree">' +
                      '<a data-toggle="collapse" class="collapsed" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">' +
                        '<h5 class="mb-0">Máquinas térmicas</h5>' +
                        '<span class="icon svg-button"><i data-feather="chevron-down"></i></span>' +
                      '</a>' +
                    '</div>' +
                    '<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#sm_accordion050101_07_es-1">' +
                      '<div class="card-body">' +
                        '<p>Las máquinas manuales son aquellas cuyo funcionamiento requiere de la fuerza humana.</p>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                '</div>' +
              '</div>',
            summary:
              '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
              'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
              'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
              'Duis gravida eget neque vel vulputate.</p><img alt="" src="resources/sm_050101_04_es.jpg">',
            elems: [
              {
                id: 'todo-juntos2-4df5-54ge-df55-3ft',
                title: 'Todos juntos',
                type: 'section',
                minutes: 0,

                embed: [
                  {id: 'sm_050101_04_es', type: 'image', url: 'resources/sm_050101_04_es.jpg'},
                  {id: 'sm_050101_12_es', type: 'image', url: 'resources/sm_050101_12_es.jpg'},
                  {id: 'sm_050101_02_es', type: 'image', url: 'resources/sm_050101_02_es.jpg'},
                  {id: 'sm_050101_11_es', type: 'image', url: 'resources/sm_050101_11_es.jpg'},
                  {id: 'sm_050101_03_es', type: 'image', url: 'resources/sm_050101_03_es.jpg'}
                ],
                content:
                '<div class="sm-section-text">' +
                  '<p>' +
                    'En grupo, clasificamos las máquinas según sean simples o compuestas. ' +
                    'Haz clic o arrastra cada imagen hasta su categoría.' +
                  '</p>' +
                  '<div class="sm-activity-drag-drop">' +
                    '<div class="sm-activity-drag">' +
                      '<div class="block-drag" data-value="simple">{0}</div>' +
                      '<div class="block-drag" data-value="complex">{1}</div>' +
                      '<div class="block-drag" data-value="complex">{2}</div>' +
                      '<div class="block-drag" data-value="complex">{3}</div>' +
                      '<div class="block-drag" data-value="simple">{4}</div>' +
                    '</div>' +
                    '<div class="sm-activity-drop bloks-3">' +
                      '<div class="block-drop">' +
                        '<h5 class="label">Funciona con la fuerza de las personas</h5>' +
                      '</div>' +
                      '<div class="block-drop">' +
                        '<h5 class="label">Funciona con la electricidad</h5>' +
                      '</div>' +
                      '<div class="block-drop">' +
                        '<h5 class="label">Funciona con gasolina o combustible fósil</h5>' +
                      '</div>' +
                    '</div>' +
                  '</div>' +
                  '<p>' +
                    'Las máquinas más sencillas tienen pocas piezas y reciben el nombre de máquinas simples. ' +
                    'Las máquinas más complicadas tienen bastantes piezas y las llamamos máquinas compuestas.' +
                  '</p>' +
                '</div>'
              },

              {
                id: 'actividades2-cuaderno-4df5-54ge-df55-3ft',
                title: 'Actividades para el cuaderno',
                subtitle:'Responde a estas actividades en tu cuaderno.',
                type: 'sectionNotebook',
                embed: [],
                content:
                '<ol  class="activity-list-number">' +
                '<li>' +
                '<span class="dot-circle">1</span>' +
                'Elige una máquina simple y explica en qué tarea nos ayuda. ' +
                'Haz lo mismo con una máquina compuesta.' +
                '</li>' +
                '<li>' +
                '<span class="dot-circle">2</span>' +
                'Escribe todas las máquinas que has utilizado hoy antes de llegar a tu centro. ' +
                'Elige la que más te gusta y escribe si es una máquina simple o compuesta.' +
                '</li>' +
                '<li>' +
                '<span class="dot-circle">3</span>' +
                'Aenean sed nibh a magna posuere tempor. Nunc faucibus pellentesque nunc in aliquet. ' +
                'Donec congue, nunc vel tempor congue, enim sapien lobortis ipsum, ' +
                'in volutpat sem ex in ligula. Nunc purus est, consequat condimentum faucibus sed, ' +
                'iaculis sit amet massa. Fusce ac condimentum turpis. ' +
                '</li>' +
                '</ol>'
              }
            ],
            notes: [
              {
                id: 'note-4df5-54ge-df55-3ft-3',
                bookmark: 2,
                epigraph: 'como-funciona-maquinas-4df5-54ge-df55-3ft',
                title: 'Actividad en el circuito ....',
                subtitle: 'Proyecto de clase',
                html: ''
              }
            ]
          },
          {
              id: 'las-maquinas-4df5-54ge-df55-3ft',
              title:'Las máquinas simples',
              type:'epigraph',
              num:'3',
              embed: [],
              content:'<div class="sm-section-text"></div>',
              summary:
                '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
                'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
                'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                'Duis gravida eget neque vel vulputate.</p>',
              elems: []

          },
          {
              id: 'maquina-compuestas-y-susu-usos-4df5-54ge-df55-3ft',
              title:'Máquinas compuestas y sus usos',
              type:'epigraph',
              num:'5',
              embed: [],
              content:'<div class="sm-section-text"></div>',
              summary:
                '<p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. ' +
                'Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. ' +
                'Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. ' +
                'Duis gravida eget neque vel vulputate.</p>',
              elems:[]
          }
        ]
      }
    ]
  }
];