var library = [
  {
    id:'naturaleza-4df5-54ge-df55-3ft',
    title:'5.º EP CIENCIAS DE LA NATURALEZA.',
    level: '1 ESO',
    type:'book',
    content:'<h1>Introducción</h1><p>Fusce at nisi eget dolor rhoncus facilisis. Mauris ante nisl, consectetur et luctus et, porta ut dolor. Curabitur ultricies ultrices nulla. Morbi blandit nec est vitae dictum. Etiam vel consectetur diam. Maecenas vitae egestas dolor. Fusce tempor magna at tortor aliquet finibus. Sed eu nunc sit amet elit euismod faucibus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis gravida eget neque vel vulputate.</p>',
    thumb:'letter-bg.jpg',
    elems: [
      {
        id: 'lasplantas-4df5-54ge-df55-3ft',
        title:'Las plantas',
        type:'unit',
        thumb:'f.jpg',
        percent: 100,
        embed: [
          {id: 'sm_050101_01_es', type: 'image', url: 'resources/sm_050101_01_es.jpg'}
        ],
        content:
            ' <div class="sm-cover">' +
            '  <div class="sm-media-actions sm-cover-actions sm-media-actions-dark">' +
            '  <a data-smreference="f9372e37-0731-4746-904e-238fce65bbfe" class="sm-media-button sm-media-actions-teacherdark" data-teacher-action="true" href="javascript:;" title="¿Qué recordamos de las plantas? (Actividad de diagnóstico)">' +
            '  <span class="sm-ui sm-icon-interactive"></span>' +
            '  </a>' +
            '  <a data-smreference="326c8f0e-85d7-4a05-85ef-147c962dbe62" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿Qué sabes sobre las plantas?">' +
            '  <span class="sm-ui sm-icon-interactive"></span>' +
            '  </a>' +
            '  <a data-smreference="dea0611f-872c-4129-9102-63070c3cb3ab" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Tipos de plantas">' +
            '  <span class="sm-ui sm-icon-interactive"></span>' +
            '  </a>' +
            '  <a data-smreference="3ce1d157-15e1-4a2e-82b1-381ae9730c43" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Funciones de las plantas">' +
            '  <span class="sm-ui sm-icon-interactive"></span>' +
            '  </a>' +
            '   </div>' +
            '   {0}' +
            '</div>' +
            '<div class="sm-subject-summary">' +
                '<div class="sm-subject-summary-header">' +
                    '<span class="sm-ui sm-summary-ribbon"></span>' +
                    '<h3>¡Qué importante es...<br></h3>' +
                    '<h3><span>la curiosidad científica!<br><span></span></span></h3>' +
                '</div>' +
                '<div class="sm-subject-summary-content">' +
                    '<div class="sm-line">' +
                        '<div class="sm-block sm-block-1-5">' +
                            '<div class="  ">' +
                                '<p>En la Antigüedad había personas que, movidas por la&nbsp;curiosidad, descubrieron las propiedades curativas de&nbsp;algunas plantas. Las plantas también son importantes porque nos aportan alimentos y el oxígeno del aire,&nbsp;fundamental para que podamos respirar.</p>' +
                            '</div>' +
                        '</div>' +
                        '<div class="sm-block sm-block-1-5">' +
                            '<div class="  ">' +
                                '<h3>Tarea final</h3>' +
                                '<p><span style="line-height: 21.99995994567871px;">Sé un investigador. Pon algodón húmedo en dos frascos. Coloca cinco judías en cada uno y cúbrelas con más algodón húmedo. Introduce un frasco en la nevera, el otro déjalo en la habitación... ¡y al final de la&nbsp;unidad saca tus propias conclusiones!</span><br></p>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>',
        elems: [
            {
                id: 'introduccion-4df5-54ge-df55-3ft',
                title:'Introducción',
                type:'epigraph',
                elems: [
                    {
                        id: 'propiedadessecretas-4df5-54ge-df55-3ft',
                        title:'Las propiedades secretas de las plantas',
                        type:'section',
                        embed: [
                            {id: 'youtube', thumb:'resources/video.png', type: 'video', minHeight: '60%', url: 'https://www.youtube.com/embed/G17jQg_pUJg'},
                            {id: 'sm_050101_02_es', type: 'image', url: 'resources/sm_050101_02_es.jpg'}
                        ],
                        content:
                            '<div class="sm-section-content">' +
                                '<div class="sm-textblock floated-right">' +
                                    '<div class="sm-media">' +
                                        '<div class="sm-media-content">' +
                                            '{1}' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="sm-reading-text">' +
                                    '<div class="  ">' +
                                        '<p>–Hoy nos vamos a dedicar a replantar mandrágoras. Veamos, ¿quién me puede decir qué propiedades tiene la mandrágora?</p>' +
                                        '<p>Sin que nadie se sorprendiera, Hermione fue la primera en alzar la mano.</p>' +
                                        '<p>–La mandrágora, o mandrágula, es un reconstituyente muy eficaz –dijo Hermione […]–. Se utiliza para volver a su estado original a la gente<br>que ha sido transformada o encantada.</p>' +
                                        '<p>–Excelente, diez puntos para Gryffindor –dijo la profesora Sprout. […]</p>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="sm-reading-text-author">' +
                                    '<p>J. K. Rowling: <em>Harry Potter y la cámara secreta.</em><br>Editorial Salamandra<br></p>' +
                                '</div>' +
                                '{0}' +
                            '</div>'
                    },
                    {
                        id: 'hablamos-4df5-54ge-df55-3ft',
                        title:'Hablamos',
                        type:'activities',
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-activity">' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">1</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>En su novela, J. K. Rowling inventa un uso de&nbsp;una planta, la mandrágora. Indica de qué uso&nbsp;se trata.</p>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <p>Dice que la mandrágora es un reconstituyente muy eficaz, porque devuelve a su estado original a la gente que ha sido transformada o encantada.<br></p>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">2</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>En la naturaleza crecen gran variedad de&nbsp;plantas. Algunas son tóxicas, mientras que&nbsp;otras nos aportan muchos beneficios. Fíjate&nbsp;en esta lista de plantas e investiga. ¿Para qué&nbsp;se utilizan?</p>' +
                        '</div>' +
                        '<div class="sm-text-seroregular">' +
                        '   <p style="text-align: center;">manzanilla<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp;&nbsp;&nbsp;</span><span style="color: rgb(189, 213, 54); line-height: 21.99995994567871px; text-align: center;">● &nbsp;&nbsp;</span>aloe vera<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp;&nbsp;&nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp;&nbsp;</span>regaliz<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp;&nbsp;</span>laurel<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp;&nbsp;</span>perejil<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp;&nbsp;</span>​tila</p>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <div class="sm-text-seroregular myHtmlEditor" data-html-id="f126acb5c2a4cae36c3332e515c2e590" style="color:#EC038A;" contenteditable="true">' +
                        ' <p><strong>Manzanilla:</strong> es digestiva y sedante. Regula el funcionamiento intestinal. Se utiliza también para lavar heridas ya que es cicatrizante y antiséptica. Es también un colirio eficaz.</p>' +
                        ' <p><strong>Aloe vera:</strong> suaviza la piel, cicatriza las heridas y tonifica. Se utiliza también para quemaduras y afecciones de la piel.</p>' +
                        ' <p><strong>Regaliz: </strong>Favorece la expectoración, calma la tos y desinflama las vías respiratorias. Calma la acidez de estómago y hace desaparecer la sensación de empacho o de pesadez.<br></p>' +
                        ' <p><strong>Laurel:</strong> facilita la digestión y elimina gases. El aceite de laurel aplicado externamente alivia el dolor reumático.</p>' +
                        ' <p><strong>Perejil:</strong> es diurético y aperitivo (facilita la digestión). Vasodilatador y tonificante.<br></p>' +
                        ' <p><strong>Tila:</strong> tiene efectos sedantes y relajantes.</p>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">3</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>Observa al druida de la imagen. ¿Qué parte de&nbsp;la planta piensas que va a echar en el caldero?</p>' +
                        '</div>' +
                        '<div class="sm-text-seroregular">' +
                        '   <ol style="list-style-type: upper-alpha;" start="1">' +
                        '  <li>Tallo<br></li>' +
                        '  <li>Hojas<br></li>' +
                        '  <li>Raíz<br></li>' +
                        '   </ol>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        ' <p>La raíz, que es la parte a la que se le atribuían propiedades mágicas.<br></p>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div clasepigraphs="sm-activity-list">' +
                        '<span style="">4</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="sm-media-actions-textblock">' +
                        '   <a class="sm-media-button " data-teacher-action="false" data-open-popup="2722e53dd48f4bfced47eeb583c7e617" href="javascript:;" title="Ampliar">' +
                        '   <span class="sm-ui sm-icon-picture"></span>' +
                        '   </a>' +
                        '</div>' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>Fíjate en el roble de la imagen. ¿En qué estación&nbsp;del año crees que está? ¿Por qué?</p>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        ' <p>En otoño, porque las hojas del árbol tienen colores anaranjados.<br></p>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">5</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="sm-media-actions-textblock">' +
                        '   <a data-smreference="58310f78-ba14-481a-a573-8ab9786f257d" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="De semilla a planta (Vídeo)">' +
                        '   <span class="sm-ui sm-icon-video"></span>' +
                        '   </a>' +
                        '</div>' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>Este roble nació de una pequeña bellota&nbsp;y puede llegar a pesar más de 25.000 kg.&nbsp;¿De dónde habrá obtenido la materia para&nbsp;crecer y pesar tanto? Ten en cuenta que puede&nbsp;haber más de una respuesta correcta.</p>' +
                        '</div>' +
                        '<div class="sm-text-seroregular">' +
                        '   <ol style="list-style-type: upper-alpha;" start="1">' +
                        '  <li>Del suelo<br></li>' +
                        '  <li>Del agua<br></li>' +
                        '  <li>Del aire<br></li>' +
                        '   </ol>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <p>Las tres respuestas son correctas. Las plantas obtienen del suelo (por la raíz), agua y sales minerales. Del aire (por las hojas) toman dióxido de carbono y con todo esto y la luz del sol fabrican los nutrientes que necesitan para crecer mediante la fotosíntesis. Las plantas se construyen básicamente a partir de dióxido de carbono y agua.<br></p>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'maquinas-por-todas-partes-4df5-54ge-df55-3ft',
                title:'Características de las plantas',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_02_es', type: 'image', url: 'resources/sm_050101_02_es.jpg'}
                ],
                content:
                  '<div class="sm-section-content">' +
                    '<div class="sm-textblock floated-right">' +
                      '<div class="sm-media">' +
                        '<div class="sm-media-content">' +
                          '{0}' +
                        '</div>' +
                      '</div>' +
                    '</div>' +
                    '<div class="  ">' +
                      '<p>El druida tiene una duda. Le extraña que los cactus piedra sean plantas como los robles o los rosales, porque no lo parecen. ¿Por qué crees que son tan diferentes unas plantas de otras?</p>' +
                      '<p>Existe una gran variedad de <strong>plantas</strong>. Todas tienen en común que <strong>fabrican su propio alimento</strong>, pero su aspecto puede ser muy distinto porque depende del entorno en el que viven.<br></p>' +
                    '</div>' +
                  '</div>',
                elems: [
                  {
                    id: 'partes-4df5-54ge-df55-3ft',
                    title: 'Las partes de una planta',
                    type: 'section',
                    minutes: 0,
                    embed: [
                        {id: 'sm_050101_04_es', type: 'image', url: 'resources/sm_050101_04_es.jpg'},
                        {id: 'sm_050101_05_es', type: 'image', url: 'resources/sm_050101_05_es.jpg'}
                    ],
                    content:
                      '<div class="sm-section-content">' +
                        '<div class="  ">' +
                          '<p>Como ya sabes, para crecer, las plantas utilizan <strong>agua</strong> y <strong>sustancias minerales</strong><br>del suelo, <strong>luz</strong> y un gas del aire, el <strong>dióxido de carbono</strong>. Pero&nbsp;conseguir todo esto no es tarea fácil.<br></p>' +
                          '<p>El agua se encuentra en el suelo, por lo que las plantas necesitan órganos<br>para absorberla. También tienen que captar la luz del sol y,&nbsp;para ello, necesitan crecer y crear una estructura que las sostenga.</p>' +
                          '<p>Todas estas necesidades hacen que la mayoría de las plantas posean <strong>raíz</strong>, <strong>tallo</strong> y <strong>hojas</strong>. ¿Qué función tiene cada una de estas partes?</p>' +
                        '</div>' +
                        '<div class="sm-media">' +
                          '<div class="sm-media-content">' +
                            '{0}' +
                          '</div>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<p><strong>Raíz.</strong> Es el órgano que fija la&nbsp;planta al suelo. Absorbe el&nbsp;agua con las sales minerales&nbsp;disueltas y, en ocasiones,&nbsp;también sirve como almacén&nbsp;de sustancias.</p>' +
                        '</div>' +
                        '<div class="sm-textblock floated-right">' +
                          '<div class="sm-media">' +
                            '<div class="sm-media-content">' +
                              '{1}' +
                            '</div>' +
                            '<div class="sm-media-caption">' +
                              '<p>El tronco es un tipo de tallo.<br></p>' +
                            '</div>' +
                          '</div>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<p><strong>Hojas.</strong> Son los órganos que captan la luz del sol y el dióxido de carbono. Junto con el agua y sales minerales del suelo, los utilizan para fabricar el alimento.</p>' +
                          '<p><strong>Tallo.</strong> Es el órgano que sostiene<br>la planta y comunica sus distintas<br>partes a través de dos tipos de<br>conductos:</p>' +
                          '<ul>' +
                            '<li>Los<strong> vasos leñosos</strong>&nbsp;conectan las&nbsp;raíces con las hojas.</li>' +
                            '<li><strong></strong>Los <strong>vasos liberianos</strong> conectan<br>las hojas con toda la planta.</li>' +
                          '</ul>' +
                        '</div>' +
                      '</div>'
                  },
                    {
                        id: 'interactive-4df5-54ge-df55-3ft',
                        title: 'Un interactivo',
                        type: 'section',
                        embed: [
                            {id: 'civilRomana', thumb:'resources/interactivo.png', type: 'interactive', minHeight: '60%', url: 'activities/Civilizacion_Romana/index.html'}
                        ],
                        content: '<div class="sm-section-content">' +
                        '<div class="  ">' +
                        '{0}' +
                        '</div>' +
                        '</div>',
                    },
                  {
                    id: 'clasificacion-4df5-54ge-df55-3ft',
                    title: 'La clasificación de las plantas',
                    type: 'section',
                    minutes: 0,
                    embed: [
                        {id: 'sm_050101_06_es', type: 'image', url: 'resources/sm_050101_06_es.jpg'},
                        {id: 'sm_050101_07_es', type: 'image', url: 'resources/sm_050101_07_es.jpg'},
                        {id: 'sm_050101_08_es', type: 'image', url: 'resources/sm_050101_08_es.jpg'},
                        {id: 'sm_050101_09_es', type: 'image', url: 'resources/sm_050101_09_es.jpg'}
                    ],
                    content:
                      '<div class="sm-section-content">' +
                        '<div class="sm-media-actions-textblock">' +
                          '<a data-smreference="f96fd1ea-4dd7-4024-9f94-4d45483d8f28" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="Clasificación de las plantas (Presentación)">' +
                            '<span class="sm-ui sm-icon-interactive"></span>' +
                          '</a>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<p>Además de raíz, tallos y hojas, muchas plantas poseen también <strong>flores</strong>, <strong>frutos</strong> y <strong>semillas</strong>. Estos órganos les sirven para reproducirse, y conocerlos nos permite clasificarlas en dos grupos:</p>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<h3><br>Plantas con flores<br><br></h3>' +
                        '</div>' +
                        '<div class="sm-line">' +
                          '<div class="sm-block sm-block-1-5">' +
                            '<div class="sm-media">' +
                              '<div class="sm-media-content">' +
                                '{0}' +
                              '</div>' +
                              '<div class="sm-media-caption">' +
                                '<p><strong>Angiospermas</strong><br>Tienen frutos, y en su interior hay semillas.</p>' +
                              '</div>' +
                            '</div>' +
                          '</div>' +
                          '<div class="sm-block sm-block-1-5">' +
                            '<div class="sm-media">' +
                              '<div class="sm-media-content">' +
                                '<div class="sm-media-actions sm-media-actions-dark">' +
                                  '<a class="sm-media-button " data-teacher-action="false" data-open-popup="34cb243c8eef645745497039ef5640de" href="javascript:;" title="Ampliar">' +
                                    '<span class="sm-ui sm-icon-picture"></span>' +
                                  '</a>' +
                                '</div>' +
                                '{1}' +
                              '</div>' +
                              '<div class="sm-media-caption">' +
                                '<p><strong>Gimnospermas</strong><br>No tienen frutos, pero sí semillas.<br><br></p>' +
                              '</div>' +
                            '</div>' +
                          '</div>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<p>Las plantas que tienen flores se reproducen mediante&nbsp;<strong>semillas</strong>.<br></p>' +
                          '<ul>' +
                            '<li>Si las semillas se encuentran en el interior de los frutos, las plantas se llaman <strong><a class="fancyboxText" id="popuplink_442081914071" title="Angiospermas" data-cke-saved-href="#4057b43aed782053ce62116748d9062b" href="#4057b43aed782053ce62116748d9062b">angiospermas</a></strong>.<br><br>El manzano y la encina son plantas angiospermas.</li>' +
                            '<li>Si las plantas no tienen frutos pero sí tienen semillas, reciben el nombre de plantas <strong><a class="fancyboxText" id="popuplink_243869007012" title="Gimnospermas" data-cke-saved-href="#ad61304576eb3443d6bc168433615a29" href="#ad61304576eb3443d6bc168433615a29">gimnospermas</a></strong>.<br><br>El pino es una planta gimnosperma. Aunque puede&nbsp;parecer que las piñas son sus frutos, en realidad&nbsp;no lo son. En el interior de las piñas están los piñones,&nbsp;que son las semillas.</li>' +
                          '</ul>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<h3><br>Plantas sin flores<br><br></h3>' +
                        '</div>' +
                        '<div class="sm-line">' +
                          '<div class="sm-block sm-block-1-5">' +
                            '<div class="sm-media">' +
                              '<div class="sm-media-content">' +
                                '{2}' +
                              '</div>' +
                              '<div class="sm-media-caption">' +
                                '<p><strong>Helechos</strong><br>Tienen raíces, hojas y conductos.<br></p>' +
                              '</div>' +
                            '</div>' +
                          '</div>' +
                          '<div class="sm-block sm-block-1-5">' +
                            '<div class="sm-media">' +
                              '<div class="sm-media-content">' +
                                '{3}' +
                              '</div>' +
                              '<div class="sm-media-caption">' +
                                '<p><strong>Musgos</strong><br>No tienen raíces, hojas ni conductos.<br></p>' +
                              '</div>' +
                            '</div>' +
                          '</div>' +
                        '</div>' +
                        '<div class="sm-media-actions-textblock">' +
                          '<a data-smreference="3593632d-3ed6-4bb5-baf6-ae4cabcbe342" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="Musgos y helechos">' +
                            '<span class="sm-ui sm-icon-video"></span>' +
                          '</a>' +
                        '</div>' +
                        '<div class="  ">' +
                          '<p>Las plantas sin flores no producen semillas. Se reproducen mediante <strong>esporas</strong>.</p>' +
                          '<p>A este grupo pertenecen los <strong>helechos</strong> y los <strong>musgos</strong>.</p>' +
                          '<p>Los helechos poseen raíces, hojas y conductos por los que circula la savia, y los musgos, en cambio, no.</p>' +
                        '</div>' +
                        '<div class="sm-highlight">' +
                          '<div class="sm-highlight-content">' +
                            '<div class="  ">' +
                              '<ul>' +
                                '<li>Todas las plantas, salvo los musgos, tienen <strong>raíz</strong>, <strong>tallo </strong>y <strong>hojas</strong>.</li>' +
                                '<li>Las plantas con <strong>flores</strong> se reproducen por <strong>semillas</strong>. Se clasifican en <strong>angiospermas</strong> y <strong>gimnospermas</strong>. Las plantas <strong>sin&nbsp; flores</strong> se reproducen por <strong>esporas</strong>: son los <strong>helechos</strong> y los <strong>musgos</strong>.</li>' +
                              '</ul>' +
                            '</div>' +
                          '</div>' +
                          '<span class="sm-ui sm-highlight-flap"></span>' +
                        '</div>' +
                      '</div>'
                  }
                ]
            },
            {
                id: 'nutricion-4df5-54ge-df55-3ft',
                title:'La nutrición de las plantas',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_10_es', type: 'image', url: 'resources/sm_050101_10_es.jpg'}
                ],
                content:
                    '<div class="sm-section-content">' +
                    '      <div class="sm-textblock floated-right">' +
                    '         <div class="sm-media">' +
                    '            <div class="sm-media-content">' +
                    '               {0}' +
                    '            </div>' +
                    '         </div>' +
                    '      </div>' +
                    '      <div class="  ">' +
                    '         <p>En 1624, un científico llamado Van Helmont quería averiguar qué utilizaban las pantas para crecer. Observa el experimento que ideó para descubrirlo:<br></p>' +
                    '      </div>' +
                    '      <div class="sm-media-actions-textblock">' +
                    '         <a class="sm-media-button " data-teacher-action="false" data-open-popup="2f70f3cc01bc696a761b1804b20938f5" href="javascript:;" title="Ampliar">' +
                    '         <span class="sm-ui sm-icon-picture"></span>' +
                    '         </a>' +
                    '      </div>' +
                    '      <div class="  ">' +
                    '         <p>Al cabo de cinco años, el sauce pesaba 75 kg más que al principio&nbsp;y la tierra había perdido solo medio kilogramo. Por eso&nbsp;pensó que el sauce usaba solamente el agua para crecer.</p>' +
                    '         <p>Hoy sabemos que estaba equivocado. Pero entonces ¿qué otra sustancia había utilizado el sauce para crecer?</p>' +
                    '      </div>' +
                    '   </div>',
                elems: [
                    {
                        id: 'comocrecen-4df5-54ge-df55-3ft',
                        title: '¿Cómo crecen las plantas?',
                        type: 'section',
                        embed: [
                            {id: 'sm_050101_11_es', type: 'image', url: 'resources/sm_050101_11_es.jpg'}
                        ],
                        minutes: 0,
                        content:
                            '<div class="sm-section-content">' +
                            '      <div class="sm-media-actions-textblock">' +
                            '         <a data-smreference="2f5084f6-e7f1-4317-acac-c6725338163f" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="La fotosíntesis y la respiración">' +
                            '         <span class="sm-ui sm-icon-video"></span>' +
                            '         </a>' +
                            '         <a data-smreference="dc89a9d6-33d4-40c9-b610-c6df38836885" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="¿Qué recordamos de la fotosíntesis y la respiración? (Actividad de diagnóstico)">' +
                            '         <span class="sm-ui sm-icon-interactive"></span>' +
                            '         </a>' +
                            '      </div>' +
                            '      <div class="  ">' +
                            '         <p>Las plantas, como los demás seres vivos, necesitan alimento y energía para crecer, y los obtienen mediante la <strong>nutrición</strong>. En&nbsp;las plantas, la nutrición tiene dos fases:</p>' +
                            '         <ul>' +
                            '            <li><strong></strong><strong>Fabricación de su propio alimento</strong>. Las plantas fabrican su&nbsp;alimento en las hojas mediante un proceso que se llama&nbsp;<strong>fotosíntesis</strong>. En este proceso se desprende un gas, el oxígeno.<br><br>En la fotosíntesis las plantas utilizan agua, sales minerales, dióxido de carbono y la energía de la luz del sol captada mediante la <strong><a class="fancyboxText" id="popuplink_295770404077" title="Clorofila" data-cke-saved-href="#4d1da9aa0993738a2a745805ba145b66" href="#4d1da9aa0993738a2a745805ba145b66">clorofila</a></strong>, una sustancia de color verde que está en las hojas.</li>' +
                            '            <li><strong>Obtención de energía.</strong> Al igual que los animales, las plantas <strong></strong><strong>respiran</strong>. Al respirar toman oxígeno del aire a través de&nbsp;las hojas. El oxígeno del aire y los alimentos que se han fabricado&nbsp;en la fotosíntesis se unen y se produce energía. En&nbsp;este proceso se desprende un gas, el dióxido de carbono.</li>' +
                            '         </ul>' +
                            '      </div>' +
                            '      <div class="  ">' +
                            '         <p><br></p>' +
                            '      </div>' +
                            '      <div class="sm-media-actions-textblock" style="margin-left: 15px; position: relative; z-index: 1;">' +
                            '         <a data-smreference="4798baad-2729-402e-a8c7-a7b0d9631e8a" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿Qué necesita una planta?">' +
                            '         <span class="sm-ui sm-icon-interactive"></span>' +
                            '         </a>' +
                            '      </div>' +
                            '      <div id="id11ec45702eee71b816ecf9ccf283d76b" class="sm-panel  sm-text-seroregular">' +
                            '         <style></style>' +
                            '         <h2><strong>¿Qué necesita una planta?</strong><br></h2>' +
                            '         <p>La fotosíntesis<br></p>' +
                            '      </div>' +
                            '      <h3>' +
                            '         <div>' +
                            '            <p>Fabricación del alimento<br></p>' +
                            '         </div>' +
                            '      </h3>' +
                            '      <div class="sm-media">' +
                            '         <div class="sm-media-content">' +
                            '            <div class="sm-media-actions sm-media-actions-dark">' +
                            '               <a class="sm-media-button " data-teacher-action="false" data-open-popup="fa8f298597465264f7b11c4730d6251e" href="javascript:;" title="Ampliar">' +
                            '               <span class="sm-ui sm-icon-picture"></span>' +
                            '               </a>' +
                            '            </div>' +
                            '            {0}' +
                            '         </div>' +
                            '      </div>' +
                            '      <div class="  ">' +
                            '         <ul>' +
                            '            <li>Las plantas absorben <strong>dióxido de carbono</strong> a través de unos poros de las hojas llamados <strong><a class="fancyboxText" id="popuplink_594561460427" title="Estomas" data-cke-saved-href="#c99fc0c2bae19375d9b190d30cdf45bd" href="#c99fc0c2bae19375d9b190d30cdf45bd">estomas</a></strong>.</li>' +
                            '            <li>Las <strong>raíces</strong> absorben del suelo <strong>agua con sales minerales</strong> disueltas. Esta mezcla constituye la <strong>savia bruta</strong>. La savia bruta asciende hacia las hojas a través de los <strong>vasos leñosos</strong>.</li>' +
                            '            <li>En las hojas se produce la <strong>fotosíntesis</strong>. En ellas la <strong>savia bruta</strong> y el <strong>dióxido de carbono</strong> se transforman en <strong>alimento</strong> gracias a la <strong>luz solar</strong>. En este proceso se desprende <strong>oxígeno</strong>.</li>' +
                            '            <li>El alimento forma la <strong>savia elaborada</strong>. La savia elaborada se distribuye por toda la planta a través&nbsp;de&nbsp;los <strong>vasos liberianos</strong>. Así, la planta crece y su peso aumenta.</li>' +
                            '         </ul>' +
                            '      </div>' +
                            '      <h3>' +
                            '         <div>' +
                            '            <p>Obtención de energía<br></p>' +
                            '         </div>' +
                            '      </h3>' +
                            '      <div class="sm-media-actions-textblock">' +
                            '         <a data-smreference="68d4adaf-36c7-435a-963f-bb83731fbef1" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿Cómo se alimentan las plantas?">' +
                            '         <span class="sm-ui sm-icon-interactive"></span>' +
                            '         </a>' +
                            '      </div>' +
                            '      <div class="  ">' +
                            '         <p>Tanto de día como de noche las plantas necesitan <strong>energía</strong>. Para&nbsp;ello utilizan los <strong>alimentos </strong>que han&nbsp;producido y el <strong>oxígeno</strong> del aire.&nbsp;<br></p>' +
                            '         <p>En este proceso, llamado<strong> respiración</strong>,&nbsp;se desprende <strong>dióxido de carbono</strong>.</p>' +
                            '      </div>' +
                            '   </div>'
                    },
                    {
                        id: 'actividades-4df5-54ge-df55-3ft',
                        title: 'Actividades',
                        type: 'activities',
                        minutes: 0,
                        content:
                        '<div class="sm-section-content">' +
                        '      <div class="  ">' +
                        '         <h3><span style="color:#8bc543;">Trabaja con la imagen</span><br><br></h3>' +
                        '      </div>' +
                        '      <div class="sm-activity">' +
                        '         <div class="sm-activity-item">' +
                        '            <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '            <div class="sm-activity-list">' +
                        '               <span style="">4</span>' +
                        '            </div>' +
                        '            <div class="sm-activity-content">' +
                        '               <div class="  sm-text-seromedium ">' +
                        '                  <p>¿Qué función realizan estas partes de las plantas? Raíz, vasos leñosos y vasos liberianos.<br></p>' +
                        '               </div>' +
                        '               <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '                  <div class="  ">' +
                        '                     <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                        <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                           <p><strong>Raíz:</strong> absorber del suelo agua con sales minerales disueltas.</p>' +
                        '                           <p><strong>Vasos leñosos:</strong> hacer llegar desde la raíz a las hojas la savia bruta.</p>' +
                        '                           <p><strong>Vasos liberianos:</strong> distribuir la savia elaborada por toda la planta.<br></p>' +
                        '                        </div>' +
                        '                     </div>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '         <div class="sm-activity-item">' +
                        '            <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '            <div class="sm-activity-list">' +
                        '               <span style="">5</span>' +
                        '            </div>' +
                        '            <div class="sm-activity-content">' +
                        '               <div class="  sm-text-seromedium ">' +
                        '                  <p>Copia y completa la tabla en tu cuaderno.</p>' +
                        '               </div>' +
                        '               <div class="sm-text-seroregular">' +
                        '                  <table border="1" cellpadding="1" cellspacing="1" height="66" width="582">' +
                        '                     <tbody>' +
                        '                        <tr>' +
                        '                           <td style="width: 34%; border-color: rgb(255, 255, 255); background-color: rgb(255, 255, 255);"><br></td>' +
                        '                           <td style="border-color: rgb(139, 197, 67); width: 33%; text-align: center; vertical-align: middle; background-color: rgb(215, 228, 135);">Fotosíntesis</td>' +
                        '                           <td style="border-color: rgb(139, 197, 67); width: 33%; height: 50px; text-align: center; vertical-align: middle; background-color: rgb(215, 228, 135);">Respiración</td>' +
                        '                        </tr>' +
                        '                        <tr>' +
                        '                           <td style="text-align: center; vertical-align: middle; width: 34%; border-color: rgb(139, 197, 67); background-color: rgb(215, 228, 135);">Gas que entra</td>' +
                        '                           <td style="text-align: center;">Dióxido de carbono</td>' +
                        '                           <td style="width: 33%; height: 50px; text-align: center;"><span style="color:#bdd536;">●●●</span></td>' +
                        '                        </tr>' +
                        '                        <tr>' +
                        '                           <td style="text-align: center; vertical-align: middle; width: 34%; border-color: rgb(139, 197, 67); background-color: rgb(215, 228, 135);">Gas producido</td>' +
                        '                           <td style="text-align: center;"><span style="color:#bdd536;">●●●</span></td>' +
                        '                           <td style="width: 33%; height: 50px; text-align: center;"><span style="color:#bdd536;">●●●</span></td>' +
                        '                        </tr>' +
                        '                     </tbody>' +
                        '                  </table>' +
                        '                  <p><br></p>' +
                        '               </div>' +
                        '               <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '                  <div class="  ">' +
                        '                     <table border="1" cellpadding="1" cellspacing="1" height="66" width="582">' +
                        '                        <thead>' +
                        '                           <tr>' +
                        '                              <th scope="col" style="width: 34%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><br></th>' +
                        '                              <th scope="col" style="background-color: rgb(250, 192, 226); border-color: rgb(236, 3, 138); width: 33%; text-align: center; vertical-align: middle;">Fotosíntesis</th>' +
                        '                              <th scope="col" style="background-color: rgb(250, 192, 226); border-color: rgb(236, 3, 138); width: 33%; height: 50px; text-align: center; vertical-align: middle;">Respiración</th>' +
                        '                           </tr>' +
                        '                        </thead>' +
                        '                        <tbody>' +
                        '                           <tr>' +
                        '                              <th style="text-align: center; vertical-align: middle; width: 34%; background-color: rgb(250, 192, 226); border-color: rgb(236, 3, 138);">Gas que entra</th>' +
                        '                              <td style="text-align: center; border-color: rgb(236, 3, 138);">Dióxido de carbono</td>' +
                        '                              <td style="width: 33%; height: 50px; text-align: center; border-color: rgb(236, 3, 138);">Oxígeno</td>' +
                        '                           </tr>' +
                        '                           <tr>' +
                        '                              <th style="text-align: center; vertical-align: middle; width: 34%; background-color: rgb(250, 192, 226); border-color: rgb(236, 3, 138);">Gas producido</th>' +
                        '                              <td style="text-align: center; border-color: rgb(236, 3, 138);">Oxígeno</td>' +
                        '                              <td style="width: 33%; height: 50px; text-align: center; border-color: rgb(236, 3, 138);">Dióxido de Carbono</td>' +
                        '                           </tr>' +
                        '                        </tbody>' +
                        '                     </table>' +
                        '                     <p><br>Hay que aclarar que la planta realiza más fotosíntesis que respiración, este balance favorable a la primera es el que permite que crezca, ya que la respiración es un proceso en donde se consume lo sintetizado.</p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-highlight">' +
                        '         <div class="sm-highlight-content">' +
                        '            <div class="  ">' +
                        '               <ul>' +
                        '                  <li>Las plantas obtienen el <strong>alimento</strong> para crecer y vivir mediante la <strong>fotosíntesis</strong>. En este proceso se libera <strong>oxígeno</strong>.<br></li>' +
                        '                  <li>Las plantas obtienen energía mediante la <strong>respiración</strong>. En este proceso se desprende <strong>dióxido de carbono</strong>.<br></li>' +
                        '               </ul>' +
                        '            </div>' +
                        '         </div>' +
                        '         <span class="sm-ui sm-highlight-flap"></span>' +
                        '      </div>' +
                        '   </div>'
                    }
                ]
            },
            {
                id: 'repsexual-4df5-54ge-df55-3ft',
                title:'La reproducción sexual de las plantas',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_12_es', type: 'image', url: 'resources/sm_050101_12_es.jpg'}
                ],
                content:
                '<div class="sm-section-content">' +
                '   <div class="sm-textblock floated-right">' +
                '  <div class="sm-media">' +
                ' <div class="sm-media-content">' +
                '{0}' +
                ' </div>' +
                ' <div class="sm-media-caption">' +
                '<p><em></em>¡Las secuoyas&nbsp;pueden llegar a vivir&nbsp;<br>más de 2.000 años!<em></em></p>' +
                ' </div>' +
                '  </div>' +
                '   </div>' +
                '   <div class="sm-media-actions-textblock">' +
                '  <a data-smreference="42a280ea-9c34-4ca3-8c88-3a6a4f15eb96" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="De la flor a la cereza">' +
                '  <span class="sm-ui sm-icon-video"></span>' +
                '  </a>' +
                '   </div>' +
                '   <div class="  ">' +
                '  <p>Algunos árboles viven siglos, y otras plantas, como las hierbas, solo&nbsp;unos meses; no obstante, todas las plantas terminan muriendo. Por&nbsp;eso, para que no se extingan y desaparezcan, deben reproducirse.&nbsp;<br></p>' +
                '  <p>Las plantas pueden reproducirse de forma <strong>sexual</strong> mediante la unión&nbsp;de un grano de <strong>polen</strong> y un <strong>óvulo</strong>. Este tipo de reproducción es diferente&nbsp;según la planta tenga o no tenga flor y, en ella, la nueva planta<br>es similar a sus progenitores.<br></p>' +
                '  <p>Algunas plantas también pueden reproducirse de forma <strong>asexual</strong> a<br>partir de un<strong> fragmento</strong> de ellas mismas.</p>' +
                '   </div>' +
                '</div>',
                elems: [
                    {
                        id: 'plantasflores-4df5-54ge-df55-3ft',
                        title: 'Las plantas con flores',
                        type: 'section',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_13_es', type: 'image', url: 'resources/sm_050101_13_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="  ">' +
                        '  <p>La <strong>flor </strong>es el órgano reproductor de la mayoría de las plantas. En&nbsp;muchas plantas, las flores tienen <strong>pistilo</strong>, que es el <strong>órgano reproductor&nbsp;femenino</strong>, y <strong>estambres,</strong> que son los <strong>órganos reproductores&nbsp;masculinos.</strong> Este tipo de flores, como las del almendro, se llaman&nbsp;flores hermafroditas.<br></p>' +
                        '  <p>En estas flores el polen de los estambres y los óvulos del pistilo no se&nbsp;desarrollan a la vez. Por eso, para que tenga lugar la reproducción, el&nbsp;polen de una flor tiene que llegar al pistilo de la flor de otra planta.&nbsp;Este proceso, llamado <strong>polinización</strong>, se produce gracias a que el viento&nbsp;y algunos animales, como los insectos, transportan el polen.&nbsp;<br></p>' +
                        '  <p>Una vez en el pistilo de la flor, el polen llega al ovario y se une a un&nbsp;óvulo. Tras la unión, llamada <strong>fecundación</strong>, el ovario se transforma en&nbsp;un fruto, y el óvulo fecundado, en una semilla.</p>' +
                        '  <p>Cuando la semilla cae al suelo y <strong>germina</strong> da lugar a una nueva planta.</p>' +
                        '   </div>' +
                        '   <div class="sm-media">' +
                        '  <div class="sm-media-content">' +
                        ' <div class="sm-media-actions sm-media-actions-dark">' +
                        '<a data-smreference="219b93be-526e-409e-a6fb-50f28b81f0e6" class="sm-media-button sm-media-actions-teacherdark" data-teacher-action="true" href="javascript:;" title="El desarrollo de una nueva planta (Animación)">' +
                        '<span class="sm-ui sm-icon-interactive"></span>' +
                        '</a>' +
                        '<a data-smreference="7983a0e6-150a-4d40-9076-985f4c12d5c1" class="sm-media-button sm-media-actions-teacherdark" data-teacher-action="true" href="javascript:;" title="Las partes de una flor (Actividad grupal)">' +
                        '<span class="sm-ui sm-icon-interactive"></span>' +
                        '</a>' +
                        ' </div>' +
                        ' {0}' +
                        '  </div>' +
                        '   </div>' +
                        '   <div class="sm-media-actions-textblock" style="margin-left: 15px; position: relative; z-index: 1;">' +
                        '  <a data-smreference="45c90ff4-8985-4896-891a-b6eb78f0a316" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Juega con el almendro">' +
                        '  <span class="sm-ui sm-icon-interactive"></span>' +
                        '  </a>' +
                        '   </div>' +
                        '   <div id="id1275102757ae83ec5b771c04782084d4" class="sm-panel  sm-text-seroregular">' +
                        '  <style></style>' +
                        '  <p><strong>Juega y aprende</strong><br>¿Cómo se reproduce el almendro?<br></p>' +
                        '  <p></p>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'floresmascfem-4df5-54ge-df55-3ft',
                        title: '¿Y si tienen flores masculinas y flores femeninas?',
                        type: 'section',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_14_es', type: 'image', url: 'resources/sm_050101_14_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-textblock floated-right">' +
                        '  <div class="sm-media">' +
                        ' <div class="sm-media-content">' +
                        '{0}' +
                        ' </div>' +
                        '  </div>' +
                        '   </div>' +
                        '   <div class="  ">' +
                        '  <p>En algunas plantas, como la encina, los órganos reproductores masculinos y femeninos están en flores distintas, es decir, existen <strong>flores masculinas</strong> y <strong>flores femeninas</strong>.</p>' +
                        '  <p>En estos casos, el polen de la flor masculina de una planta fecunda lel pistilo de la flor femenina de otra planta distinta y se forma así una semilla. Cuando esta semilla germina origina una nueva planta.</p>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'sinflores-4df5-54ge-df55-3ft',
                        title: 'Plantas sin flores',
                        type: 'section',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_15_es', type: 'image', url: 'resources/sm_050101_15_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="  ">' +
                        '  <p>Los musgos y los helechos carecen de flores. ¿Cómo logran, entonces, reproducirse? Como los helechos y los musgos no tienen flores, la fecundación tiene lugar en el exterior, es decir, fuera de la planta.</p>' +
                        '  <p>Observa lo que ocurre con este helecho. Bajo las hojas posee unos pequeños abultamientos, llamados <strong>soros</strong>, que producen miles de <strong>esporas</strong>. Cuando las esporas caen al suelo húmedo, germinan y forman un pequeño órgano. En ese órgano tendrá lugar la fecundación y se originará un nuevo helecho.</p>' +
                        '   </div>' +
                        '   <div class="sm-media">' +
                        '  <div class="sm-media-content">' +
                        ' {0}' +
                        '  </div>' +
                        '   </div>' +
                        '   <div class="sm-highlight">' +
                        '  <div class="sm-highlight-content">' +
                        ' <div class="  ">' +
                        '<p>La <strong>reproducción sexual</strong> de la mayoría de las plantas tiene lugar en la <strong>flor</strong>, a partir de los <strong>óvulos</strong> y del <strong>polen</strong>. Los <strong>helechos</strong> y los&nbsp;<strong>musgos</strong> se reproducen por medio de <strong>esporas</strong>.</p>' +
                        ' </div>' +
                        '  </div>' +
                        '  <span class="sm-ui sm-highlight-flap"></span>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'activitiessexual-4df5-54ge-df55-3ft',
                        title: 'Actividades',
                        type: 'activities',
                        minutes: 0,
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-activity">' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">6</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>¿En qué se diferencia la reproducción de un helecho de la de una encina?<br></p>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        ' <p>En la encina, la fecundación se produce en la flor femenina y se produce una semilla, mientras que el helecho produce esporas y la fecundación se realiza fuera de la planta madre.<br></p>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">7</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="sm-media-actions-textblock">' +
                        '   <a data-smreference="563f8b6d-455c-42f7-88ac-f638f85945e8" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿El pino tiene flores? Fases de la reproducción">' +
                        '   <span class="sm-ui sm-icon-interactive"></span>' +
                        '   </a>' +
                        '</div>' +
                        '<div class="sm-media-actions-textblock">' +
                        '</div>' +
                        '<div class="  sm-text-seroregular ">' +
                        '   <p><span style="font-family: seropro-bold; line-height: 21.99995994567871px;">Practica</span><br style="line-height: 21.99995994567871px;"><span style="line-height: 21.99995994567871px;">Ordena&nbsp;las fotografías de la reproducción del pino.</span><br></p>' +
                        '</div>' +
                        '<div class="sm-media-actions-textblock">' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '  <div class="sm-activity-item">' +
                        ' <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        ' <div class="sm-activity-list">' +
                        '<span style="">8</span>' +
                        ' </div>' +
                        ' <div class="sm-activity-content">' +
                        '<div class="  sm-text-seromedium ">' +
                        '   <p>Revisa las judías del experimento que estás&nbsp;haciendo para la tarea final. Obsérvalas y&nbsp;anota si han sufrido algún cambio.</p>' +
                        '   <p> ¡Recuerda hacer fotografías!</p>' +
                        '</div>' +
                        '<div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '   <div class="  ">' +
                        '  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        ' <p>Además de hacer fotografías, recuerde a sus alumnos que deben tomar nota de la temperatura de la nevera y de la habitación, así como ir describiendo todo el proceso observado.<br></p>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>' +
                        ' </div>' +
                        '  </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'repasexual-4df5-54ge-df55-3ft',
                title:'La reproducción asexual en las plantas',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_16_es', type: 'image', url: 'resources/sm_050101_16_es.jpg'},
                    {id: 'sm_050101_17_es', type: 'image', url: 'resources/sm_050101_17_es.jpg'}
                ],
                content:
                '<div class="sm-section-content">' +
                '   <div class="sm-textblock floated-right">' +
                '      <div class="sm-media">' +
                '         <div class="sm-media-content">' +
                '            {0}' +
                '         </div>' +
                '      </div>' +
                '   </div>' +
                '   <div class="sm-media-actions-textblock">' +
                '      <a data-smreference="5d44ef3c-9529-48d7-9b65-b4dd73b8e718" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="La reproducción asexual de las plantas (Presentación)">' +
                '      <span class="sm-ui sm-icon-interactive"></span>' +
                '      </a>' +
                '   </div>' +
                '   <div class="  ">' +
                '      <p>Muchas plantas, como el geranio, aunque florezcan y produzcan semillas, también pueden reproducirse a partir de un <strong>fragmento</strong> de ellas mismas. A partir de este fragmento se genera una nueva planta que es una <strong>copia idéntica</strong> a su progenitora. Este tipo de reproducción se llama <strong>reproducción asexual</strong>.</p>' +
                '      <p>Existen diferentes tipos de fragmentos a partir de los cuales la planta&nbsp;se puede reproducir asexualmente:</p>' +
                '      <ul>' +
                '         <li><strong>Tubérculos.</strong> Son <strong>tallos subterráneos</strong> que acumulan gran cantidad de alimento. La planta de la patata se reproduce mediante tubérculos.</li>' +
                '         <li><strong>Bulbos.</strong> Son el <strong>extremo inferior del tallo</strong> de algunas plantas y contienen<br>gran cantidad de alimento. Están formados por capas que,&nbsp;en la temporada siguiente, se convertirán en hojas. Al sembrarlos,&nbsp;cada bulbo origina una nueva planta. El ajo se reproduce por bulbos.</li>' +
                '         <li><strong>Rizomas.</strong> Son <strong>tallos subterráneos</strong> que crecen <strong>horizontalmente</strong>. Sobre estos tallos se desarrollan nuevas plantas. El lirio se reproduce&nbsp;por rizomas.</li>' +
                '         <li><strong>Estolones.</strong> Son <strong>tallos</strong> que crecen <strong>horizontalmente sobre el suelo</strong>. A medida que crecen desarrollan raíces que dan lugar a una nueva planta. Las plantas de las fresas se reproducen por estolones.</li>' +
                '         <li><strong>Esquejes.</strong> Son <strong>tallos</strong> que tienen la capacidad de producir raíces&nbsp;cuando están separados de la planta. Las vides y los geranios se&nbsp;reproducen mediante esquejes.</li>' +
                '      </ul>' +
                '   </div>' +
                '   <div class="sm-media">' +
                '      <div class="sm-media-content">' +
                '         {1}' +
                '      </div>' +
                '   </div>' +
                '   <div class="sm-highlight">' +
                '      <div class="sm-highlight-content">' +
                '         <div class="  ">' +
                '            <ul>' +
                '               <li>La reproducción asexual de las plantas se puede producir mediante los <strong>tubérculos</strong>, los <strong>bulbos</strong>, los <strong>estolones</strong>, los <strong>rizomas</strong> o los <strong>esquejes</strong>.</li>' +
                '               <li>La reproducción asexual genera <strong>plantas</strong> que son <strong>idénticas</strong> a&nbsp;sus progenitores.</li>' +
                '            </ul>' +
                '         </div>' +
                '      </div>' +
                '      <span class="sm-ui sm-highlight-flap"></span>' +
                '   </div>' +
                '</div>',
                elems: [
                    {
                        id: 'activitiesasex-4df5-54ge-df55-3ft',
                        title: 'Actividades',
                        type: 'activities',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_38_es', type: 'image', url: 'resources/sm_050101_38_es.jpg'},
                            {id: 'sm_050101_37_es', type: 'image', url: 'resources/sm_050101_37_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">9</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>¿En qué se parece y&nbsp;en qué se diferencia la&nbsp;reproducción de los lirios&nbsp;y de las fresas?</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>El lirio se reproduce por rizomas y la fresa por estolones.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '   <div class="  ">' +
                        '      <p>&nbsp;</p>' +
                        '   </div>' +
                        '   <div class="sm-arrowbox ">' +
                        '      <div class="sm-arrowbox-header">' +
                        '         <div class="sm-arrowbox-arrow">' +
                        '            <div class="sm-ui sm-arrowbox-part sm-arrowbox-end"></div>' +
                        '            <!--' +
                        '               -->' +
                        '            <div class="sm-arrowbox-part sm-arrowbox-middle">' +
                        '               <h3>Taller de ciencias<br></h3>' +
                        '            </div>' +
                        '            <div class="sm-ui sm-arrowbox-part sm-arrowbox-start"></div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-arrowbox-content">' +
                        '         <div class="  ">' +
                        '            <h3>Construye un huerto</h3>' +
                        '         </div>' +
                        '         <div class="sm-textblock floated-right">' +
                        '            <div class="sm-media">' +
                        '               <div class="sm-media-content">' +
                        '                  {0}' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <p>Desde la Antigüedad, los seres humanos sembraban semillas en los alrededores de sus cuevas y creaban sus propios “huertos”. Esto les permitía disponer de alimentos en épocas de escasez.</p>' +
                        '            <p>Actualmente, muchas personas de las ciudades también cultivan pequeños huertos en sus casas. A&nbsp;este tipo de huertos se los conoce como <em>huertos urbanos</em>.</p>' +
                        '            <p>Sigue las instrucciones y crea tu huerto urbano colgante.</p>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <h3><br>Huerto colgante<br></h3>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <p><strong>Material:</strong></p>' +
                        '            <ul>' +
                        '               <li>Tres botellas de plástico, de 1,5 ℓ, mejor verdes o azules.</li>' +
                        '               <li>Sustrato vegetal, que encontrarás en tiendas de jardinería.<br></li>' +
                        '               <li>Estolones de fresas.<br></li>' +
                        '               <li>Piedras.<br></li>' +
                        '            </ul>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <p><strong>Instrucciones:</strong><br></p>' +
                        '         </div>' +
                        '         <div class="sm-media">' +
                        '            <div class="sm-media-content">' +
                        '               {1}' +
                        '            </div>' +
                        '         </div>' +
                        '         <div class="sm-media-actions-textblock" style="margin-left: 15px; position: relative; z-index: 1;">' +
                        '            <a data-smreference="5f74a4af-a0a5-43ff-be6b-8e2f641cd0f5" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="El huerto">' +
                        '            <span class="sm-ui sm-icon-interactive"></span>' +
                        '            </a>' +
                        '         </div>' +
                        '         <div id="id595aeade2ef3d41bb5e288e817549eb8" class="sm-panel  sm-text-seroregular">' +
                        '            <style></style>' +
                        '            <h2><strong>Investiga en la web</strong><br>El huerto<br></h2>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <p>&nbsp;<br></p>' +
                        '         </div>' +
                        '         <div class="sm-activity">' +
                        '            <div class="sm-activity-item">' +
                        '               <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '               <div class="sm-activity-list">' +
                        '                  <span style="">10</span>' +
                        '               </div>' +
                        '               <div class="sm-activity-content">' +
                        '                  <div class="  sm-text-seromedium ">' +
                        '                     <p>Indica qué tipo de reproducción tiene lugar en el huerto colgante.</p>' +
                        '                  </div>' +
                        '                  <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '                     <div class="  ">' +
                        '                        <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                           <p>Al realizarse con estolones de fresa, se trata de una reproducción asexual.<br></p>' +
                        '                        </div>' +
                        '                     </div>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="sm-activity-item">' +
                        '               <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '               <div class="sm-activity-list">' +
                        '                  <span style="">11</span>' +
                        '               </div>' +
                        '               <div class="sm-activity-content">' +
                        '                  <div class="  sm-text-seromedium ">' +
                        '                     <p>¿En qué se diferencia este tipo de reproducción de la de tu experimento con judías?</p>' +
                        '                  </div>' +
                        '                  <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '                     <div class="  ">' +
                        '                        <div class="sm-text-seroregular myHtmlEditor" data-html-id="af680d78e753468898dcdf30d5001c99" style="color:#EC038A;" contenteditable="true">' +
                        '                           <p>En el semillero se germinan semillas, las cuales son consecuencia de la reproducción sexual. La reproducción del huerto colgante es asexual.<br></p>' +
                        '                        </div>' +
                        '                     </div>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'relaentorno-4df5-54ge-df55-3ft',
                title:'La relación de las plantas con el entorno',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_18_es', type: 'image', url: 'resources/sm_050101_18_es.jpg'}
                ],
                content:
                '<div class="sm-section-content">' +
                '   <div class="sm-textblock floated-right">' +
                '      <div class="sm-media">' +
                '         <div class="sm-media-content">' +
                '            <div class="sm-media-actions sm-media-actions-dark">' +
                '               <a data-smreference="3557ce25-3c34-4f12-8cc0-a6324ad58bb2" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Una planta carnívora (Youtube)">' +
                '               <span class="sm-ui sm-icon-link"></span>' +
                '               </a>' +
                '            </div>' +
                '            {0}' +
                '         </div>' +
                '      </div>' +
                '   </div>' +
                '   <div class="sm-media-actions-textblock">' +
                '      <a data-smreference="f19c0cca-b88a-4782-b6e8-d404267fa391" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="Tropismos y nastias (Presentación)">' +
                '      <span class="sm-ui sm-icon-interactive"></span>' +
                '      </a>' +
                '   </div>' +
                '   <div class="  ">' +
                '      <p>Cuando una mosca se posa sobre una planta carnívora, esta lo detecta&nbsp;y se cierra automáticamente. ¿Cómo es posible?<br></p>' +
                '      <p>Aunque las plantas no tienen órganos de los sentidos, se relacionan&nbsp;con el medio que las rodea y reaccionan ante ciertos estímulos. Un&nbsp;ejemplo de estímulo es el contacto de una mosca, que hace que la&nbsp;planta cierre sus hojas.</p>' +
                '   </div>' +
                '</div>',
                elems: [
                    {
                        id: 'semueven-4df5-54ge-df55-3ft',
                        title: 'Las plantas se mueven',
                        type: 'section',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_19_es', type: 'image', url: 'resources/sm_050101_19_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="  ">' +
                        '      <p>Las plantas son capaces de reaccionar con un movimiento ante el contacto, y también ante otros estímulos como la luz, la gravedad o la humedad. Estas formas de relación con el medio, que implican un movimiento, se llaman <strong>nastias</strong> y <strong>tropismos</strong>.</p>' +
                        '      <ul>' +
                        '         <li>Las <strong>nastias no van acompañadas de crecimiento de la planta</strong>. Es el <span style="line-height: 21.99995994567871px;"></span>caso de la planta carnívora, que se cierra para capturar insectos, o&nbsp;del movimiento de los girasoles.<span style="line-height: 21.99995994567871px;"></span></li>' +
                        '         <li>Los <strong>tropismos</strong> sí van acompañados del <strong>crecimiento de la planta. </strong>Un&nbsp;ejemplo de tropismo es el crecimiento de las plantas hacia la luz.</li>' +
                        '      </ul>' +
                        '   </div>' +
                        '   <div class="sm-media">' +
                        '      <div class="sm-media-content">' +
                        '         {0}' +
                        '      </div>' +
                        '   </div>' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">12</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="sm-media-actions-textblock">' +
                        '               <a data-smreference="f499b5c2-e38e-404d-b98c-e898d26f9d66" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Tropismos en los tomates (Youtube)">' +
                        '               <span class="sm-ui sm-icon-link"></span>' +
                        '               </a>' +
                        '            </div>' +
                        '            <div class="  sm-text-seroregular ">' +
                        '               <p><span style="font-family: seropro-bold; line-height: 21.99995994567871px;">Observa y relaciona</span><br style="line-height: 21.99995994567871px;"><span style="line-height: 21.99995994567871px;">¿Qué tipo de tropismo&nbsp;aparece en el vídeo?</span><br></p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>Se trata de un fototropismo.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'estaciones-4df5-54ge-df55-3ft',
                        title: 'Las plantas y las estaciones',
                        type: 'section',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_20_es', type: 'image', url: 'resources/sm_050101_20_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-media-actions-textblock">' +
                        '      <a data-smreference="5293d70f-69f3-49f6-9f3a-8947c39312e1" class="sm-media-button sm-media-actions-textblockteacher" data-teacher-action="true" href="javascript:;" title="Las cuatro estaciones (Actividad grupal)">' +
                        '      <span class="sm-ui sm-icon-interactive"></span>' +
                        '      </a>' +
                        '   </div>' +
                        '   <div class="  ">' +
                        '      <p>Las plantas también reaccionan ante estímulos como los cambios en&nbsp;el entorno. Así, las variaciones ambientales de luz, de temperatura y&nbsp;de humedad influyen en sus ciclos vitales.</p>' +
                        '      <p>Observa cómo reacciona el haya, un árbol de hoja caduca típico de los bosques del norte de España, ante los cambios ambientales que se producen a lo largo del año debido a las estaciones.</p>' +
                        '   </div>' +
                        '   <div class="sm-media">' +
                        '      <div class="sm-media-content">' +
                        '         {0}' +
                        '      </div>' +
                        '   </div>' +
                        '   <div class="sm-highlight">' +
                        '      <div class="sm-highlight-content">' +
                        '         <div class="  ">' +
                        '            <p>Las plantas reaccionan ante la <strong>luz</strong>, el <strong>contacto</strong>, la <strong>gravedad</strong>, la<br><strong>humedad</strong>, el número de<strong> horas de luz</strong> y la <strong>temperatura</strong>.</p>' +
                        '         </div>' +
                        '      </div>' +
                        '      <span class="sm-ui sm-highlight-flap"></span>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'activitiesentorn-4df5-54ge-df55-3ft',
                        title: 'Actividades',
                        type: 'activities',
                        minutes: 0,
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="  ">' +
                        '      <h3><span style="color:#8bc543;">Trabaja con la gráfica</span><br><br></h3>' +
                        '   </div>' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">13</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="sm-media-actions-textblock">' +
                        '               <a class="sm-media-button " data-teacher-action="false" data-open-popup="94572205d60cdf496fbee2dc2a9805fe" href="javascript:;" title="Ampliar">' +
                        '               <span class="sm-ui sm-icon-picture"></span>' +
                        '               </a>' +
                        '            </div>' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Fíjate en la línea roja de la gráfica del haya. ¿Cómo influye la temperatura en la aparición de las hojas y en el desarrollo de los frutos? ¿Y en la caída de las hojas? ¿Cómo influye la cantidad de horas de luz del día?<br></p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>Como el haya vegeta en climas sin sequía estival, el agua solo escaseará durante el invierno, ya que las temperaturas invernales de las regiones en donde habita suelen ser bajas.</p>' +
                        '                     <p><strong>Primavera </strong>(Temperatura e insolación crecientes): se produce el brote y desarrollo de las hojas. Floración. Fotosíntesis muy intensa. Crecimiento del árbol.</p>' +
                        '                     <p><strong>Verano </strong>(Temperatura alta e insolación intensa): desarrollo de fruto y semilla. Formación de las yemas. Fotosíntesis muy intensa.</p>' +
                        '                     <p><strong>Otoño </strong>(Temperatura e insolación decreciente): maduración de fruto y absorción de la clorofila de las hojas, (por eso se hacen visibles otros pigmentos de las mismas): Caída de la hoja, la fotosíntesis se reduce.</p>' +
                        '                     <p><strong>Invierno </strong>(Temperatura baja con heladas): Árbol sin hojas. No hay fotosíntesis. El árbol solo realiza respiración celular.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">14</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="sm-media-actions-textblock">' +
                        '               <a class="sm-media-button " data-teacher-action="false" data-open-popup="94572205d60cdf496fbee2dc2a9805fe" href="javascript:;" title="Ampliar">' +
                        '               <span class="sm-ui sm-icon-picture"></span>' +
                        '               </a>' +
                        '            </div>' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>¿Tiene hojas el haya en invierno? ¿Crees que realizará la fotosíntesis en esta estación del año? Razona tu respuesta.</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>El haya en invierno no tiene hojas y no puede realizar la fotosíntesis ya que la luz es muy escasa y las bajas temperaturas hielan el agua.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '   <div class="  ">' +
                        '      <h3><span style="color:#8bc543;"><br>Relaciona<br></span><br></h3>' +
                        '   </div>' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">15</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Hay árboles de climas fríos que no pierden sus hojas en invierno. Las conservan porque la primavera y el verano son muy cortos, y no les compensa perderlas y hacerlas brotar de nuevo para tan poco tiempo. Investiga y busca ejemplos de árboles a los que les suceda. ¿Qué nombre reciben estas plantas?</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <p>Son las coníferas (pinos, abetos, cedros). La mayor parte de ellos son plantas de latitudes altas o de montaña, en donde el periodo de actividad vegetativa (crecimiento) es muy corto. Mantienen la hoja, salvo algunas excepciones, para poder realizar la fotosíntesis durante el mayor tiempo posible.<br></p>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'organice-4df5-54ge-df55-3ft',
                title:'Organiza tus ideas',
                type:'epigraph',
                embed: [
                    {id: 'sm_050101_21_es', type: 'image', url: 'resources/sm_050101_21_es.jpg'}
                ],
                content:
                '<div class="sm-section-content">' +
                '   <div class="sm-media">' +
                '      <div class="sm-media-content">' +
                '         <div class="sm-media-actions sm-media-actions-dark">' +
                '            <a data-smreference="ab9d84dd-228a-4935-a465-3abc691a8c8e" class="sm-media-button sm-media-actions-teacherdark" data-teacher-action="true" href="javascript:;" title="Las plantas (Mapa conceptual)">' +
                '            <span class="sm-ui sm-icon-document"></span>' +
                '            </a>' +
                '         </div>' +
                '         {0}' +
                '      </div>' +
                '   </div>' +
                '   <div class="sm-highlight">' +
                '      <div class="sm-highlight-content">' +
                '         <div class="  ">' +
                '            <h3><span style="color:#ef5987;"><strong>Diccionario ciencitífico</strong></span><br></h3>' +
                '            <p><strong>Angiospermas:</strong> Grupo de plantas con flor que producen fruto.</p>' +
                '            <p><strong>Clorofila:</strong> Sustancia de color verde de las hojas de las plantas que interviene en la fotosíntesis.</p>' +
                '            <p><strong>Estomas:</strong> Pequeños poros del envés de las hojas a través de los cuales se produce el intercambio de gases con el medio.</p>' +
                '            <p><strong>Gimnospermas:</strong> Grupo de plantas con flor que carecen de fruto.</p>' +
                '         </div>' +
                '      </div>' +
                '      <span class="sm-ui sm-highlight-flap"></span>' +
                '   </div>' +
                '</div>',
                elems: [
                    {
                        id: 'esquema-4df5-54ge-df55-3ft',
                        title: 'Trabaja con el esquema',
                        type: 'activities',
                        minutes: 0,
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">16</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Copia en tu cuaderno el dibujo de la flor y&nbsp;añade el nombre de sus partes.</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>Respuesta libre.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">17</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Añade en el esquema de tu cuaderno el&nbsp;nombre de los diferentes tipos de reproducción&nbsp;asexual y dibújalos.</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>Respuesta libre.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">18</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Observa el esquema y completa la tabla&nbsp;con los siguientes términos. <br></p>' +
                        '            </div>' +
                        '            <div class="sm-text-seroregular">' +
                        '               <p style="text-align: center;">dióxido de carbono &nbsp;&nbsp;<span style="color: rgb(189, 213, 54); text-align: center;">● &nbsp;&nbsp;</span>oxígeno<span style="color: rgb(189, 213, 54); line-height: 21.99995994567871px; text-align: center;">&nbsp;&nbsp;&nbsp;</span><br>agua con sales minerales &nbsp;&nbsp;<span style="color: rgb(189, 213, 54); text-align: center;">● &nbsp;&nbsp;</span>savia elaborada &nbsp;&nbsp;<span style="color: rgb(189, 213, 54); text-align: center;">● &nbsp;&nbsp;</span>luz<br></p>' +
                        '               <p style="text-align: center;"><br></p>' +
                        '            </div>' +
                        '            <div class="sm-text-seroregular">' +
                        '               <table border="1" cellpadding="1" cellspacing="1" height="66" width="582">' +
                        '                  <tbody>' +
                        '                     <tr>' +
                        '                        <td style="border-color: rgb(139, 197, 67); width: 50%; text-align: center; vertical-align: middle; height: 50px; background-color: rgb(215, 228, 135);">¿Qué necesitan las plantas para fabricar su alimento?</td>' +
                        '                        <td style="border-color: rgb(139, 197, 67); width: 50%; height: 50px; text-align: center; vertical-align: middle; background-color: rgb(215, 228, 135);">¿Qué productos se obtienen en la fabricación de alimento?</td>' +
                        '                     </tr>' +
                        '                     <tr>' +
                        '                        <td style="text-align: center;"><span style="color:#bdd536;">●●●</span></td>' +
                        '                        <td style="width: 33%; height: 50px; text-align: center;"><span style="color: rgb(189, 213, 54); text-align: center;">●●●</span></td>' +
                        '                     </tr>' +
                        '                     <tr>' +
                        '                        <td style="text-align: center;"><span style="color: rgb(189, 213, 54); text-align: center;">●●●</span></td>' +
                        '                        <td style="width: 33%; height: 50px; text-align: center;"><span style="color: rgb(189, 213, 54); text-align: center;">●●●</span></td>' +
                        '                     </tr>' +
                        '                     <tr>' +
                        '                        <td style="text-align: center;"><span style="color: rgb(189, 213, 54); text-align: center;">●●●</span></td>' +
                        '                        <td style="width: 33%; height: 50px; text-align: center;"><span style="color: rgb(189, 213, 54); text-align: center;">●●●</span></td>' +
                        '                     </tr>' +
                        '                  </tbody>' +
                        '               </table>' +
                        '               <p><br></p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" data-html-id="a2303027e22fd873542f885a03e05ca0" style="color:#EC038A;" contenteditable="true">' +
                        '                     <p>Para fabricar su alimento, las plantas necesitan dióxido de carbono, agua con sales minerales y luz. Al fabricar alimento se producen oxígeno y savia elaborada.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">19</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Indica si son verdaderas o falsas estas&nbsp;afirmaciones. Después corrige las que&nbsp;sean falsas.</p>' +
                        '            </div>' +
                        '            <div class="sm-text-seroregular">' +
                        '               <ol style="list-style-type: lower-alpha;" start="1">' +
                        '                  <li>Las plantas se reproducen sexualmente&nbsp;por medio de las esporas.&nbsp;<br></li>' +
                        '                  <li>Los musgos y los helechos son plantas&nbsp;que no poseen flores.<br></li>' +
                        '                  <li>Todas las plantas se componen de raíces,&nbsp;tallo y hojas.</li>' +
                        '               </ol>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                        <ol style="list-style-type: lower-alpha;" start="1">' +
                        '                           <li>Verdadero.</li>' +
                        '                           <li>Verdadero. Su reproducción es por esporas.</li>' +
                        '                           <li>Falso. Los musgos carecen de ellos.<br></li>' +
                        '                        </ol>' +
                        '                     </div>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'repasa-4df5-54ge-df55-3ft',
                title: 'Repasa la unidad',
                type: 'epigraph',
                embed: [
                    {id: 'sm_050101_26_es', type: 'image', url: 'resources/sm_050101_26_es.jpg'},
                    {id: 'sm_050101_27_es', type: 'image', url: 'resources/sm_050101_27_es.jpg'},
                    {id: 'sm_050101_28_es', type: 'image', url: 'resources/sm_050101_28_es.jpg'},
                    {id: 'sm_050101_29_es', type: 'image', url: 'resources/sm_050101_29_es.jpg'},
                    {id: 'sm_050101_30_es', type: 'image', url: 'resources/sm_050101_30_es.jpg'}
                ],
                content:
                '<div class="sm-section-content">' +
                '   <div class="sm-activity">' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">20</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Relaciona con flechas en tu cuaderno cada uno de los siguientes órganos con la función que desempeña.<br></p>' +
                '            </div>' +
                '            <div class="sm-text-seroregular">' +
                '               <table style="width: 100%;" border="1" cellpadding="1" cellspacing="1">' +
                '                  <tbody>' +
                '                     <tr>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Raíz</td>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 15%; text-align: right; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 55%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Fabricación de alimento</td>' +
                '                     </tr>' +
                '                     <tr>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Tallo</td>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 20%; text-align: right; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 45%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Reproducción sexual<br></td>' +
                '                     </tr>' +
                '                     <tr>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Hoja</td>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 20%; text-align: right; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 45%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Absorción de agua y sales minerales</td>' +
                '                     </tr>' +
                '                     <tr>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Flor</td>' +
                '                        <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 20%; text-align: right; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);"><span style="color:#8bc543;">●</span></td>' +
                '                        <td style="width: 45%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Distribución de sustancias<br></td>' +
                '                     </tr>' +
                '                  </tbody>' +
                '               </table>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <table style="width: 100%;" border="1" cellpadding="1" cellspacing="1">' +
                '                     <tbody>' +
                '                        <tr>' +
                '                           <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Raíz<br></td>' +
                '                           <td style="width: 5%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255); text-align: center;"><span style="FONT-SIZE: large">→</span></td>' +
                '                           <td colspan="2" style="width: 85%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Absorción de agua y sales minerales</td>' +
                '                        </tr>' +
                '                        <tr>' +
                '                           <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Tallo</td>' +
                '                           <td style="width: 5%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255); text-align: center;"><span style="FONT-SIZE: large">→</span></td>' +
                '                           <td colspan="2" style="width: 85%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Distribución de sustancias</td>' +
                '                        </tr>' +
                '                        <tr>' +
                '                           <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Hoja</td>' +
                '                           <td style="width: 5%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255); text-align: center;"><span style="FONT-SIZE: large">→</span></td>' +
                '                           <td colspan="2" style="width: 85%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Fabricación de alimento</td>' +
                '                        </tr>' +
                '                        <tr>' +
                '                           <td style="width: 10%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Flor</td>' +
                '                           <td style="width: 5%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255); text-align: center;"><span style="FONT-SIZE: large">→</span></td>' +
                '                           <td colspan="2" style="width: 85%; background-color: rgb(255, 255, 255); border-color: rgb(255, 255, 255);">Reproducción sexual</td>' +
                '                        </tr>' +
                '                     </tbody>' +
                '                  </table>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">21</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Indica en tu cuaderno cuál es la diferencia fundamental entre los helechos y los musgos. ¿Qué tienen en común?</p>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <p>Se diferencian en que los helechos poseen raíces, tallo y hojas y los musgos no. Se parecen en que se reproducen mediante esporas.<br></p>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">22</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>¿Por qué las plantas no pueden efectuar la fotosíntesis&nbsp;por la noche?</p>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <p>Las plantas no pueden realizar la fotosíntesis por la noche porque necesita una fuente de energía, en este caso la energía luminosa y por la noche no hay luz.<br></p>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">23</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Responde y dibuja en tu cuaderno:<span style="line-height: 1.22222em; font-size: 1.125em;"></span></p>' +
                '            </div>' +
                '            <div class="sm-textblock floated-right">' +
                '               <div class="sm-media">' +
                '                  <div class="sm-media-content">' +
                '                     {0}' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '            <div class="sm-text-seroregular">' +
                '               <ol style="list-style-type: lower-alpha;" start="1">' +
                '                  <li>¿En qué se diferencia la savia bruta de la savia elaborada?</li>' +
                '                  <li>¿Por qué tipo de conductos de las plantas circula la savia bruta? ¿Y la savia elaborada?</li>' +
                '                  <li>Copia el dibujo e indica el recorrido de los dos tipos de savia en una planta.</li>' +
                '               </ol>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                '                     <ol style="list-style-type: lower-alpha;" start="1">' +
                '                        <li>En que la savia bruta está compuesta de agua con sales minerales disueltas, mientras que la savia elaborada contiene los azúcares que la planta ha sintetizado disueltos en agua.</li>' +
                '                        <li>La savia bruta circula por los vasos leñosos, mientras que la elaborada lo hace por los vasos liberianos.</li>' +
                '                        <li>Respuesta libre.<br></li>' +
                '                     </ol>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">24</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Clasifica en el apartado correspondiente las&nbsp;palabras que aparecen a continuación.</p>' +
                '            </div>' +
                '            <div class="sm-text-seroregular">' +
                '               <p style="text-align: center;">dióxido de carbono<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;&nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp; &nbsp;</span>oxígeno<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;&nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp; &nbsp;</span>agua<br>sales minerales<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;&nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp; &nbsp;</span>alimento<span style="line-height: 21.99995994567871px; text-align: center;">&nbsp; &nbsp;&nbsp;</span><span style="line-height: 21.99995994567871px; text-align: center; color: rgb(189, 213, 54);">● &nbsp; &nbsp;</span>luz</p>' +
                '               <ol style="list-style-type: lower-alpha;" start="1">' +
                '                  <li>Sustancias que se necesitan para realizar la&nbsp;fotosíntesis.<br></li>' +
                '                  <li>Sustancias que se obtienen mediante la fotosíntesis.</li>' +
                '                  <li>Sustancias de desecho de la fotosíntesis.</li>' +
                '                  <li>Energía necesaria para la fotosíntesis.</li>' +
                '               </ol>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                '                     <ol style="list-style-type: lower-alpha;" start="1">' +
                '                        <li>Dióxido de carbono, agua y sales minerales.</li>' +
                '                        <li>Alimento.</li>' +
                '                        <li>Oxígeno por el día procedente de la fotosíntesis y dióxido de carbono y vapor de agua por el día y la noche procedente de la respiración.</li>' +
                '                        <li>Las plantas la obtienen de la luz solar.<br></li>' +
                '                     </ol>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">25</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Observa las siguientes fotografías.</p>' +
                '               <p>¿Cuál de estas plantas crees que será polinizada por el viento? ¿Y por los insectos? Razona tu respuesta.</p>' +
                '            </div>' +
                '            <div class="sm-line">' +
                '               <div class="sm-block sm-block-1-5">' +
                '                  <div class="sm-media">' +
                '                     <div class="sm-media-content">' +
                '                        {1}' +
                '                     </div>' +
                '                  </div>' +
                '               </div>' +
                '               <div class="sm-block sm-block-1-5">' +
                '                  <div class="sm-media">' +
                '                     <div class="sm-media-content">' +
                '                        {2}' +
                '                     </div>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                '                     <p>La primera fotografía, que corresponde a una gimnosperma (pino), se poliniza por el viento. Es una especie anemófila, mientras que la segunda, la flor del almendro, lo es por los insectos, dada la presencia de pétalos, colores atractivos y, aunque no pueda verse, el olor que exhala.<br></p>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">26</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="sm-textblock floated-right">' +
                '               <div class="sm-media">' +
                '                  <div class="sm-media-content">' +
                '                     {3}' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Un agricultor tiene en su huerto un ciruelo que produce ciruelas de gran calidad. ¿Cuál de las siguientes opciones de reproducción le recomendarías para obtener nuevos ejemplares que den ciruelas de la misma calidad? Explica por qué.</p>' +
                '            </div>' +
                '            <div class="sm-text-seroregular">' +
                '               <ol style="list-style-type: upper-alpha;" start="1">' +
                '                  <li>Por esqueje<br></li>' +
                '                  <li>Por semilla<br></li>' +
                '               </ol>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                '                     <p>Por esqueje (reproducción asexual) ya que así se mantienen las mismas características. La semilla implica mezcla con otro ejemplar y los frutos resultantes no presentarán la misma calidad. La forma más rápida y eficaz, aunque no tratada en la unidad sería el injerto, que consiste en unir a un pie de un ejemplar de semilla una yema o esqueje de la planta que queramos reproducir.<br></p>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '      <div class="sm-activity-item">' +
                '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                '         <div class="sm-activity-list">' +
                '            <span style="">27</span>' +
                '         </div>' +
                '         <div class="sm-activity-content">' +
                '            <div class="sm-textblock floated-right">' +
                '               <div class="sm-media">' +
                '                  <div class="sm-media-content">' +
                '                     {4}' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '            <div class="  sm-text-seromedium ">' +
                '               <p>Existe una planta, llamada flor del lagarto, que tiene una flor con olor a carne podrida. ¿Quién crees que la polinizará?&nbsp;Razona tu respuesta.</p>' +
                '            </div>' +
                '            <div class="sm-text-seroregular">' +
                '               <ol style="list-style-type: upper-alpha;" start="1">' +
                '                  <li>El viento<br></li>' +
                '                  <li>Las abejas<br></li>' +
                '                  <li>Las moscas<br></li>' +
                '               </ol>' +
                '            </div>' +
                '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                '               <div class="  ">' +
                '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                '                     <p>Las moscas, ya que son insectos que suelen nutrirse y dejar sus huevos en restos en descomposición.<br></p>' +
                '                  </div>' +
                '               </div>' +
                '            </div>' +
                '         </div>' +
                '      </div>' +
                '   </div>' +
                '</div>',
                elems: [
                    {
                        id: 'relate-4df5-54ge-df55-3ft',
                        title: 'Relaciona con lo que sabes',
                        type: 'activities',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_31_es', type: 'image', url: 'resources/sm_050101_31_es.jpg'},
                            {id: 'sm_050101_32_es', type: 'image', url: 'resources/sm_050101_32_es.jpg'},
                            {id: 'sm_050101_33_es', type: 'image', url: 'resources/sm_050101_33_es.jpg'},
                            {id: 'sm_050101_34_es', type: 'image', url: 'resources/sm_050101_34_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">28</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>En un laboratorio se han realizado dos montajes iguales con una planta acuática llamada <em>Elodea</em>. Uno de los montajes se ha colocado en un lugar luminoso y el otro, en la oscuridad.</p>' +
                        '               <p>En ambos casos vemos que se liberan burbujitas de un gas. Responde a las preguntas y justifica tus respuestas.</p>' +
                        '            </div>' +
                        '            <div class="sm-media">' +
                        '               <div class="sm-media-content">' +
                        '                  {0}' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="sm-text-seroregular">' +
                        '               <ol style="list-style-type: lower-alpha;" start="1">' +
                        '                  <li>¿Qué gas se desprende en el montaje a ?<br></li>' +
                        '                  <li>¿Qué gas se desprende en el montaje b ?<br></li>' +
                        '                  <li>¿En cuál de los dos montajes se ha desprendido más gas? ¿Por qué?<br></li>' +
                        '                  <li>Si un animal vive al lado de una <em>Elodea</em>, ¿qué le aportará esta planta?<br></li>' +
                        '               </ol>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" data-html-id="6b37de838af92d9f108e8e8ac7a23a41" style="color:#EC038A;" contenteditable="true">' +
                        '                     <ol start="1" style="list-style-type: lower-alpha;">' +
                        '                        <li>Se desprende principalmente oxígeno, ya que en presencia de luz, la planta realiza la fotosíntesis. Aunque también se desprende dióxido de carbono, porque la planta respira tanto de día como de noche.<br></li>' +
                        '                        <li>Se desprende dióxido de carbono.</li>' +
                        '                        <li>Se ha desprendido más gas en el primer montaje, porque la actividad es mayor. La planta crece, mientras que en el segundo la planta básicamente se mantiene.</li>' +
                        '                        <li>Le aportará oxígeno y alimento.</li>' +
                        '                     </ol>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">29</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>En las fotografías de la Tierra tomadas por los&nbsp;satélites se puede observar que las regiones&nbsp;donde abundan los bosques se sitúan en las&nbsp;zonas más húmedas del planeta. ¿A qué crees&nbsp;que se debe este hecho?</p>' +
                        '            </div>' +
                        '            <div class="sm-media">' +
                        '               <div class="sm-media-content">' +
                        '                  {1}' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>A que para la fotosíntesis es necesaria el agua.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">30</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Los botánicos son las personas que se encargan del estudio de las plantas. Para clasificarlas utilizan claves dicotómicas.</p>' +
                        '               <p>Como ya eres casi un botánico, copia y completa&nbsp;la siguiente clave con los grupos de plantas&nbsp;que faltan.</p>' +
                        '            </div>' +
                        '            <div class="sm-media">' +
                        '               <div class="sm-media-content">' +
                        '                  {2}' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p>Reproducir el esquema con las respuestas: Musgos, Helechos, Angiospermas.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">31</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="sm-media-actions-textblock">' +
                        '               <a data-smreference="55130146-968e-4f91-826b-b4ed00126088" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Las plantas transpiran agua">' +
                        '               <span class="sm-ui sm-icon-video"></span>' +
                        '               </a>' +
                        '            </div>' +
                        '            <div class="  sm-text-seroregular ">' +
                        '               <p><strong>Observa y relaciona</strong><br>La fotosíntesis libera&nbsp;oxígeno al aire. Las plantas también&nbsp;liberan agua. Observa el vídeo. ¿De dónde&nbsp;proviene el agua?<br></p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <p>Las plantas transpiran y transfieren agua en forma de vapor a la atmósfera a través de los estomas. Este hecho se comprueba al ver cómo el vapor de agua se condensa en el plástico que recubre a la planta.<br></p>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">32</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>Investiga. ¿Qué es más peligroso, dormir con&nbsp;un gato o con una planta? ¿Por qué?</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <p>Es más peligroso dormir con un gato, ya que aunque las plantas respiren y consuman oxígeno (no solo por la noche), este consumo es bajísimo y mucho menor que la cantidad de oxígeno producido durante el día.<br></p>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">33</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="sm-textblock floated-right">' +
                        '               <div class="sm-media">' +
                        '                  <div class="sm-media-content">' +
                        '                     {3}' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>El aumento de dióxido de carbono en el aire&nbsp;está provocando un aumento de la temperatura&nbsp;del planeta. Escribe en tu cuaderno alguna&nbsp;medida relacionada con las plantas para ayudar&nbsp;a disminuir el dióxido de carbono.<br></p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <p>Con independencia de otras muchas medidas, aumentar la superficie arbolada incrementaría la fotosíntesis y como en este proceso las plantas consumen dióxido de carbono, disminuirá su concentración en la atmósfera.<br></p>' +
                        '               </div>' +
                        '            </div>' +
                        '            <div class="sm-media-actions-textblock" style="margin-left: 15px; position: relative; z-index: 1;">' +
                        '               <a data-smreference="f3d60dc6-6e9a-486e-b804-4481ccede216" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿Qué recordamos de la fotosíntesis y la respiración? (Actividad de diagnóstico)">' +
                        '               <span class="sm-ui sm-icon-interactive"></span>' +
                        '               </a>' +
                        '            </div>' +
                        '            <div id="id9ccd5ec909468be718457418ceda55a0" class="sm-panel  sm-text-seroregular">' +
                        '               <style>#id9ccd5ec909468be718457418ceda55a0{border-color: #8bc543;}</style>' +
                        '               <p><strong>Valora lo aprendido</strong><br>Comprueba lo que sabes en la autoevaluación.<br></p>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    }
                ]
            },
            {
                id: 'proof-4df5-54ge-df55-3ft',
                title: 'Ponte a prueba',
                type: 'epigraph',
                elems: [
                    {
                        id: 'look-4df5-54ge-df55-3ft',
                        title: 'Observa y compara',
                        type: 'activities',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_22_es', type: 'image', url: 'resources/sm_050101_22_es.jpg'},
                            {id: 'sm_050101_23_es', type: 'image', url: 'resources/sm_050101_23_es.jpg'}
                        ],
                        content:
                        '<div class="sm-section-content">' +
                        '   <div class="  ">' +
                        '      <h3><span style="color:#8bc543;">Las plantas y las algas<br></span><br></h3>' +
                        '   </div>' +
                        '   <div class="  ">' +
                        '      <p>Además de la plantas, existen otros seres vivos que también realizan la fotosíntesis. Son las algas, que parecen plantas, y no lo son. Averigua por qué.</p>' +
                        '   </div>' +
                        '   <div class="sm-line">' +
                        '      <div class="sm-block sm-block-1-5">' +
                        '         <div class="sm-media">' +
                        '            <div class="sm-media-content">' +
                        '               {0}' +
                        '            </div>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <ul>' +
                        '               <li>Tengo clorofila en mis hojas y en ellas realizo la fotosíntesis.</li>' +
                        '               <li>Poseo raíces para absorber el agua del suelo.</li>' +
                        '               <li>Necesito un tallo para sostenerme y que tenga conductos que conecten las raíces y las hojas.</li>' +
                        '               <li>Muchas nos reproducimos por flores.<br></li>' +
                        '            </ul>' +
                        '         </div>' +
                        '      </div>' +
                        '      <div class="sm-block sm-block-1-5">' +
                        '         <div class="sm-media">' +
                        '            <div class="sm-media-content">' +
                        '               {1}' +
                        '            </div>' +
                        '         </div>' +
                        '         <div class="  ">' +
                        '            <ul>' +
                        '               <li>Poseo clorofila y realizo la fotosíntesis sin tener&nbsp;verdaderas hojas.</li>' +
                        '               <li>No necesito raíces, ya que vivo rodeada de agua.</li>' +
                        '               <li>Carezco de tallo. Me sostiene el agua y, como no tengo raíces, tampoco necesito conductos.</li>' +
                        '               <li>No poseo flores.</li>' +
                        '            </ul>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '   <div class="sm-activity">' +
                        '      <div class="sm-activity-item">' +
                        '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                        '         <div class="sm-activity-list">' +
                        '            <span style="">1</span>' +
                        '         </div>' +
                        '         <div class="sm-activity-content">' +
                        '            <div class="  sm-text-seromedium ">' +
                        '               <p>¿En qué se parecen y en qué se diferencian las plantas y las algas?</p>' +
                        '            </div>' +
                        '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                        '               <div class="  ">' +
                        '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                        '                     <p><strong>Semejanzas</strong><br>Las dos tiene clorofila y realizan la fotosíntesis.</p>' +
                        '                     <p><strong>Diferencias</strong><br>Las plantas tienen hojas, raíces y tallo. Las algas, no.<br>Las plantas tienen conductos para transportar sustancias y conectar las raíces con las hojas. Las algas no los necesitan.<br>Muchas plantas se reproducen mediante flores. Las algas no tienen flores.<br></p>' +
                        '                  </div>' +
                        '               </div>' +
                        '            </div>' +
                        '         </div>' +
                        '      </div>' +
                        '   </div>' +
                        '</div>'
                    },
                    {
                        id: 'read-4df5-54ge-df55-3ft',
                        title: 'Lee y deduce',
                        type: 'activities',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_24_es', type: 'image', url: 'resources/sm_050101_24_es.jpg'}
                        ],
                        content:
                            '<div class="sm-section-content">' +
                            '   <div class="  ">' +
                            '      <h3><span style="color:#8bc543;">El misterio de la vainilla</span><br><br></h3>' +
                            '   </div>' +
                            '   <div class="sm-textblock floated-right">' +
                            '      <div class="sm-media">' +
                            '         <div class="sm-media-content">' +
                            '            {0}' +
                            '         </div>' +
                            '      </div>' +
                            '   </div>' +
                            '   <div class="  ">' +
                            '      <p>La vainilla es el fruto de una orquídea. Con él se elaboran dulces y perfumes. La orquídea de la vainilla es una planta originaria de Centroamérica. En el siglo XVIII se intentó cultivar en otros lugares de clima parecido y, aunque crecía con fuerza y florecía, misteriosamente no producía frutos.</p>' +
                            '      <p>Un esclavo, Edmond Albius, manipulando la flor con una fina caña, logró que produjera su fruto, la vainilla.</p>' +
                            '   </div>' +
                            '   <div class="sm-activity">' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">1</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>¿Por qué la orquídea no producía vainilla en los lugares&nbsp;donde se intentó cultivar?</p>' +
                            '            </div>' +
                            '            <div class="sm-text-seroregular">' +
                            '               <ol style="list-style-type: upper-alpha;" start="1">' +
                            '                  <li>Porque el clima no era igual al de su zona de origen.<br></li>' +
                            '                  <li>Porque es una planta que necesita ser polinizada por insectos que no habitan en esa zona.<br></li>' +
                            '               </ol>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <p>La respuesta correcta es la B.<br></p>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">2</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>¿Qué tipos de polinización conoces? Explica en qué consisten.</p>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <p>A través de los insectos y por el viento. Existe también la polinización manual como por ejemplo la de los cultivos de vainilla.<br></p>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">3</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>Dibuja el ciclo de reproducción de la vainilla.</p>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <div class="sm-text-seroregular myHtmlEditor" style="color:#EC038A;">' +
                            '                     <p>Respuesta libre.<br></p>' +
                            '                  </div>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '   </div>' +
                            '</div>'
                    },
                    {
                        id: 'investigate-4df5-54ge-df55-3ft',
                        title: 'Investiga',
                        type: 'activities',
                        minutes: 0,
                        embed: [
                            {id: 'sm_050101_25_es', type: 'image', url: 'resources/sm_050101_25_es.jpg'}
                        ],
                        content:
                            '<div class="sm-section-content">' +
                            '   <div class="  ">' +
                            '      <h3><span style="color:#8bc543;">Cultivando plantas</span><br><br></h3>' +
                            '   </div>' +
                            '   <div class="sm-media-actions-textblock">' +
                            '      <a data-smreference="e7a14494-0966-4f07-954e-094de7920805" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="Cultivando plantas">' +
                            '      <span class="sm-ui sm-icon-interactive"></span>' +
                            '      </a>' +
                            '   </div>' +
                            '   <div class="  ">' +
                            '      <p>A María le regalaron semillas de una hermosa planta y decidió cultivarla. Pero&nbsp;antes realizó esta experiencia para saber en qué condiciones crecería mejor.<br></p>' +
                            '      <p>Cogió cuatro tiestos iguales y sembró diez semillas en cada uno. Después puso&nbsp;cada tiesto en un lugar con distinta temperatura, y los regó todos los días.&nbsp;<br></p>' +
                            '      <p>Observa los resultados que obtuvo y responde a las siguientes preguntas.</p>' +
                            '   </div>' +
                            '   <div class="sm-media">' +
                            '      <div class="sm-media-content">' +
                            '         {0}' +
                            '      </div>' +
                            '   </div>' +
                            '   <div class="sm-activity">' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">1</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>¿Qué condición ambiental investigó María en esta experiencia?</p>' +
                            '            </div>' +
                            '            <div class="sm-text-seroregular">' +
                            '               <ol style="list-style-type: upper-alpha;" start="1">' +
                            '                  <li>La luz <br></li>' +
                            '                  <li>La temperatura <br></li>' +
                            '                  <li>La humedad</li>' +
                            '               </ol>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <p>María solo consideró la temperatura, ya qué la luz y la humedad eran iguales para todas las siembras.<br></p>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">2</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>Observa las macetas. ¿En qué macetas no germinó ninguna semilla?<br>¿Cómo eran las temperaturas en cada caso?</p>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <p>No germinó ninguna semilla en las macetas 1 y 4, por baja y alta temperatura respectivamente.<br></p>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '      <div class="sm-activity-item">' +
                            '         <a class="sm-ui sm-activity-teachermark" href="javascript:;"></a>' +
                            '         <div class="sm-activity-list">' +
                            '            <span style="">3</span>' +
                            '         </div>' +
                            '         <div class="sm-activity-content">' +
                            '            <div class="  sm-text-seromedium ">' +
                            '               <p>A la vista de los resultados del experimento, ¿qué temperatura debería haber en el invernadero de María para que las plantas crecieran más altas? ¿Y para que germinaran más semillas? Razona tus respuestas.</p>' +
                            '            </div>' +
                            '            <div class="teacher-answer" style="color:#EC038A; display: none;">' +
                            '               <div class="  ">' +
                            '                  <p>Para que crezcan más altas, 25 ºC , ya que aunque germinan menos plantas se observa un crecimiento mayor. Para que germinen más semillas, 20 ºC.<br></p>' +
                            '               </div>' +
                            '            </div>' +
                            '         </div>' +
                            '      </div>' +
                            '   </div>' +
                            '</div>'
                    },
                    {
                        id: 'finaltask-4df5-54ge-df55-3ft',
                        title: 'Tarea final',
                        type: 'activities',
                        minutes: 0,
                        content:
                            '<div class="sm-section-content">' +
                            '   <div class="  ">' +
                            '      <h3><span style="color:#ef5987;">Un experimento paso a paso</span><br><br></h3>' +
                            '      <p>Has aprendido muchas cosas sobre las plantas y ya sabes analizar los resultados de un experimento. ¡Es el momento de extraer conclusiones!<br></p>' +
                            '   </div>' +
                            '   <div class="  ">' +
                            '      <p><span style="color:#ef5987;"><strong>Paso 1:</strong></span>&nbsp;Recupera las fotografías y los datos del experimento sobre la germinación de las judías que has realizado a lo largo de esta unidad. Ordena las fotografías y organiza los datos en una tabla como esta.</p>' +
                            '   </div>' +
                            '   <div class="  ">' +
                            '      <table border="1" cellpadding="1" cellspacing="1" height="66" width="582" style="width: 582px;">' +
                            '         <tbody>' +
                            '            <tr>' +
                            '               <td style="border-color: rgb(255, 255, 255); width: 291px;"><br></td>' +
                            '               <td style="border-color: rgb(239, 89, 135); width: 145px; text-align: center; vertical-align: middle; background-color: rgb(251, 214, 225);">Nevera</td>' +
                            '               <td style="border-color: rgb(239, 89, 135); width: 145px; height: 50px; text-align: center; vertical-align: middle; background-color: rgb(251, 214, 225);">Habitación</td>' +
                            '            </tr>' +
                            '            <tr>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center; vertical-align: middle; width: 291px; background-color: rgb(251, 214, 225);">Temperatura (°C)</td>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span></td>' +
                            '               <td style="border-color: rgb(239, 89, 135); width: 145px; height: 50px; text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span></td>' +
                            '            </tr>' +
                            '            <tr>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center; vertical-align: middle; width: 291px; background-color: rgb(251, 214, 225);">N.º semillas germinadas</td>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span></td>' +
                            '               <td style="border-color: rgb(239, 89, 135); width: 145px; height: 50px; text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span></td>' +
                            '            </tr>' +
                            '            <tr>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center; vertical-align: middle; width: 291px; background-color: rgb(251, 214, 225);">Tiempo de germinación (días)</td>' +
                            '               <td style="border-color: rgb(239, 89, 135); text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span></td>' +
                            '               <td style="border-color: rgb(239, 89, 135); width: 145px; height: 50px; text-align: center;"><span style="color: rgb(239, 89, 135);">●●●</span><br><br></td>' +
                            '            </tr>' +
                            '         </tbody>' +
                            '      </table>' +
                            '      <p><br></p>' +
                            '   </div>' +
                            '   <div class="  ">' +
                            '      <p><span style="line-height: 21.99995994567871px; color: rgb(239, 89, 135);"><span style="font-family: seropro-bold;">Paso 2:</span></span><span style="line-height: 21.99995994567871px;">&nbsp;Lleva a clase tus resultados y compáralos con los de un compañero. ¿Son iguales o diferentes? ¿Por qué? Explicad vuestras conclusiones al resto de la clase.</span></p>' +
                            '   </div>' +
                            '</div>' +
                            '<div class="sm-section-content">' +
                            '   <div class="sm-media-actions-textblock" style="margin-left: 15px; position: relative; z-index: 1;">' +
                            '      <a data-smreference="ff687921-f667-416b-9025-362b0bc8c561" class="sm-media-button " data-teacher-action="false" href="javascript:;" title="¿Cómo has trabajado?">' +
                            '      <span class="sm-ui sm-icon-interactive"></span>' +
                            '      </a>' +
                            '   </div>' +
                            '   <div id="id2983a6cb20f8f3f7a4fc47172a883fec" class="sm-panel  sm-text-seroregular">' +
                            '      <style>#id2983a6cb20f8f3f7a4fc47172a883fec{border-color: #ef5987;}</style>' +
                            '      <h2><strong>Valora lo aprendido</strong></h2>' +
                            '      <p>¿Cómo has trabajado en esta tarea?<br></p>' +
                            '   </div>' +
                            '</div>'
                    }
                ]
            }
        ]
      }
    ]
  }
];