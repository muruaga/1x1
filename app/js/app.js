(function($) {

  var App =  {
    mode: 'scroll', // 'scroll', 'secuence', 'glass'
    config: {
    },
    init: function (_config) {
      $.extend(this.config, _config);
      this.create();
      this.show();
      this.events();
    },

    create: function () {

    },

    show: function () {
      if (library[0].theme) {
        $('html').attr('data-theme', library[0].theme);
      }
      $('.app').show();
    },

    events: function () {
      $(document).on('OnShowView', function (event, selector) {
        $('.view').fadeOut();
        $(selector).fadeIn();
      });
    }

  };


  $(document).ready(function () {
    App.init();
    feather.replace();
  });
})(jQuery);
var AppUtil = {
  animationClick: function($element, animation, callback){
    $element.addClass('animated ' + animation);
    window.setTimeout( function(){
      $element.removeClass('animated ' + animation);
      if (typeof callback === 'function') {
        callback();
      }
    }, 150);
  }
};

@@include('view/scripts.js')
@@include('editor/editor.add.inline.js')
@@include('editor/editor.add.interactive.js')
@@include('editor/editor.runner.js')