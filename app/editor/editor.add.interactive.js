;(function ($, window, document, undefined) {

  'use strict';

  window['MediumInsert']['Templates']['src/js/templates/zip-fileupload.hbs']= Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return '<input name="file_upload" type="file">';
  },"useData":true});


  /** Default values */
  var pluginName = 'mediumInsert',
    addonName = 'Interactive', // first char is uppercase
    defaults = {
      label: '<span class="fa fa-plus-circle"></span>',
      fileUploadOptions: { // See https://github.com/blueimp/jQuery-File-Upload/wiki/Options
        url: 'http://demo.grupo-sm.es/service/upload.php',
        acceptFileTypes: /(\.|\/)(zip)$/i
      },
      fileDeleteOptions: {},
      messages: {
        acceptFileTypesError: 'This file is not in a supported format: ',
        maxFileSizeError: 'This file is too big: '
      }
    };

  /**
   * Custom Addon object
   *
   * Sets options, variables and calls init() function
   *
   * @constructor
   * @param {DOM} el - DOM element to init the plugin on
   * @param {object} options - Options to override defaults
   * @return {void}
   */

  function Interactive (el, options) {
    this.el = el;
    this.$el = $(el);
    this.templates = window.MediumInsert.Templates;
    this.core = this.$el.data('plugin_'+ pluginName);

    this.options = $.extend(true, {}, defaults, options);

    this._defaults = defaults;
    this._name = pluginName;

    this.init();
  }

  /**
   * Initialization
   *
   * @return {void}
   */

  Interactive.prototype.init = function () {
    this.events();
  };

  /**
   * Event listeners
   *
   * @return {void}
   */

  Interactive.prototype.events = function () {

  };

  /**
   * Get the Core object
   *
   * @return {object} Core object
   */
  Interactive.prototype.getCore = function () {
    return this.core;
  };

  /**
   * Add custom content
   *
   * This function is called when user click on the addon's icon
   *
   * @return {void}
   */


  Interactive.prototype.add = function () {
    var that = this,
        $file = $(this.templates['src/js/templates/zip-fileupload.hbs']()),
        fileUploadOptions = {
          dataType: 'json',
          add: function (e, data) {
            $.proxy(that, 'uploadAdd', e, data)();
          },
          done: function (e, data) {
            $.proxy(that, 'uploadDone', e, data)();
          }
        };


    // Only add progress callbacks for browsers that support XHR2,
    // and test for XHR2 per:
    // http://stackoverflow.com/questions/6767887/
    // what-is-the-best-way-to-check-for-xhr2-file-upload-support
    if (new XMLHttpRequest().upload) {
      fileUploadOptions.progress = function (e, data) {
        $.proxy(that, 'uploadProgress', e, data)();
      };

      fileUploadOptions.progressall = function (e, data) {
        $.proxy(that, 'uploadProgressall', e, data)();
      };
    }

    $file.fileupload($.extend(true, {}, this.options.fileUploadOptions, fileUploadOptions));

    $file.click();
  };

  Interactive.prototype.uploadAdd = function (e, data) {
    var $place = this.$el.find('.medium-insert-active'),
      that = this,
      uploadErrors = [],
      file = data.files[0],
      acceptFileTypes = this.options.fileUploadOptions.acceptFileTypes,
      maxFileSize = this.options.fileUploadOptions.maxFileSize,
      previewImg;

    if (acceptFileTypes && !acceptFileTypes.test(file.name)) {
      uploadErrors.push(this.options.messages.acceptFileTypesError + file.name);
    } else if (maxFileSize && file.size > maxFileSize) {
      uploadErrors.push(this.options.messages.maxFileSizeError + file.name);
    }
    if (uploadErrors.length > 0) {
      if (this.options.uploadFailed && typeof this.options.uploadFailed === 'function') {
        this.options.uploadFailed(uploadErrors, data);
        return;
      }

      alert(uploadErrors.join('\n'));

      return;
    }

    this.core.hideButtons();

    // Replace paragraph with div, because figure elements can't be inside paragraph
    if ($place.is('p')) {
      $place.replaceWith('<div class="medium-insert-active">' + $place.html() + '</div>');
      $place = this.$el.find('.medium-insert-active');
      if ($place.next().is('p')) {
        this.core.moveCaret($place.next());
      } else {
        $place.after('<p><br></p>'); // add empty paragraph so we can move the caret to the next line.
        this.core.moveCaret($place.next());
      }
    }

    $place.addClass('medium-insert-interactive');

    if (data.autoUpload || (data.autoUpload !== false && $(e.target).fileupload('option', 'autoUpload'))) {
      data.process().done(function (data) {
          data.submit();
      });
    }
  };

  Interactive.prototype.uploadDone = function (e, data) {
    var $place = this.$el.find('.medium-insert-active'),
        that = this;

    $place.replaceWith(
      '<div class="medium-interactive">' +
        '<iframe class="" src="' + data.response().result.url + '"></iframe>' +
        '<span class="fullscreen"><i data-feather="maximize"></i></span>' +
      '</div>'
    );
    this.core.clean();
    this.sorting();
  };


  /**
   * Callback for upload progress events.
   * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progress
   *
   * @param {Event} e
   * @param {object} data
   * @return {void}
   */

  Interactive.prototype.uploadProgress = function (e, data) {
    var progress, $progressbar;

    if (this.options.preview) {
      progress = 100 - parseInt(data.loaded / data.total * 100, 10);
      $progressbar = data.context.find('.medium-insert-interactive-progress');

      $progressbar.css('width', progress + '%');

      if (progress === 0) {
        $progressbar.remove();
      }
    }
  };

  /**
   * Callback for global upload progress events
   * https://github.com/blueimp/jQuery-File-Upload/wiki/Options#progressall
   *
   * @param {Event} e
   * @param {object} data
   * @return {void}
   */

  Interactive.prototype.uploadProgressall = function (e, data) {
    var progress, $progressbar;

    if (this.options.preview === false) {
      progress = parseInt(data.loaded / data.total * 100, 10);
      $progressbar = this.$el.find('.medium-insert-active').find('progress');

      $progressbar
        .attr('value', progress)
        .text(progress);

      if (progress === 100) {
        $progressbar.remove();
      }
    }
  };

  /** Addon initialization */

  $.fn[pluginName + addonName] = function (options) {
    return this.each(function () {
      if (!$.data(this, 'plugin_' + pluginName + addonName)) {
        $.data(this, 'plugin_' + pluginName + addonName, new Interactive(this, options));
      }
    });
  };

})(jQuery, window, document);