(function($) {
  window.EditoRunner = {
    init: function(selector) {
      var $editorText = $(selector);
      var editor = new MediumEditor(selector, {
        toolbar: {
          /* These are the default options for the toolbar,
             if nothing is passed this is what is used */
          allowMultiParagraphSelection: true,
          buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote'],
          diffLeft: 0,
          diffTop: -10,
          firstButtonClass: 'medium-editor-button-first',
          lastButtonClass: 'medium-editor-button-last',
          relativeContainer: null,
          standardizeSelectionStart: false,
          static: false,
          /* options which only apply when static is true */
          sticky: false,
          updateOnEmptySelection: false
        },
        placeholder: {
          /* This example includes the default options for placeholder,
             if nothing is passed this is what it used */
          text: 'Texto del apunte',
          hideOnClick: true
        }
      });


      $editorText.mediumInsert({
        editor: editor,
        captionPlaceholder: 'Escriba el título para imagen (opcional)',
        addons: { // (object) Addons configuration
          images: {
            label: '<i data-feather="image"></i>',
            styles: {
              wide: {
                label: '<i data-feather="align-justify"></i>'
              },
              left: {
                label: '<i data-feather="align-left"></i>'
              },
              right: {
                label: '<i data-feather="align-right"></i>'
              },
              grid: {
                label: '<i data-feather="grid"></i>'
              }
            },
            actions: {
              remove: {
                label: '<i data-feather="trash-2"></i>'
              }
            },
            messages: {
              acceptFileTypesError: 'Este archivo no tiene un formato compatible: ',
              maxFileSizeError: 'Este archivo es demasiado grande: '
            }
          },
          embeds: {
            label: '<i data-feather="youtube"></i>',
            placeholder:'Pegue un enlace de YouTube, Vimeo, Facebook, Twitter o Instagram y presione Entrar',
            captionPlaceholder: 'Escriba el subtítulo (opcional)',
            styles: {
              wide: {
                label: '<i data-feather="align-justify"></i>'
              },
              left: {
                label: '<i data-feather="align-left"></i>'
              },
              right: {
                label: '<i data-feather="align-right"></i>'
              },
              grid: {
                label: '<i data-feather="grid"></i>'
              }
            },
            actions: {
              remove: {
                label: '<i data-feather="trash-2"></i>'
              }
            }
          },
          interactive: {
            label: '<i data-feather="airplay"></i>'
          }
        }
      });

      return editor;
    }
  }
})(jQuery);