// generated on 2016-10-04 using generator-webapp 2.2.0
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const wiredep = require('wiredep').stream;
const runSequence = require('run-sequence');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var fileinclude = require('gulp-file-include');

gulp.task('copy', () => {
  gulp.src('app/lib/flagstrap/css/flags.png')
    .pipe(gulp.dest('dist/styles'));
});

gulp.task('styles', ['copy'], () => {
  return gulp.src('app/styles/*.css')
    .pipe($.sourcemaps.init())
    .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(reload({stream: true}));
});

gulp.task('scripts', () => {
  return gulp.src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(reload({stream: true}));
});

function lint(files, options) {
  return gulp.src(files)
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint(options))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js', {
    fix: true
  })
  .pipe(gulp.dest('app/scripts'));
});

gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js', {
    fix: true,
    env: {
      mocha: true
    }
  })
  .pipe(gulp.dest('test/spec'));
});

gulp.task('css', [], () => {
  return gulp.src('app/**/*.css')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'app/'
    }));
});

gulp.task('html_normal', ['styles', 'scripts'], () => {
  return gulp.src(['app/**/*.html'])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'app/'
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('html', ['html_normal'], () => {
  return gulp.src(['dist/**/*.html', 'dist/**/*.js', 'dist/**/*.css'])
    .pipe($.useref({searchPath: ['dist']}))
    //.pipe($.if('*.js', $.uglify()))
    // Use Google Closure Compiler instead...
    .pipe($.if('*.css', $.cssnano({safe: true})))
    .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true, removeComments: true})))
    // IMPORTANTE: Preservamos COMENTARIOS para Thymeleaf!! Aquellos que empiecen con /*[ cumplen
    // con toda la casuística...
    .pipe($.if('*.html', $.minifyInline({js:{compress:false,output:{comments: /^\/*\[/}}})))
    .pipe(gulp.dest('dist'));
});

gulp.task('htmlnomin', ['html_normal'], () => {
  return gulp.src(['dist/**/*.html', 'dist/**/*.js', 'dist/**/*.css'])
    .pipe($.useref({searchPath: ['dist']}))
    .pipe(gulp.dest('dist'));
});

gulp.task('htmlServe', ['styles', 'scripts'], () => {
  return gulp.src(['app/**/*.html'])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: 'app/'
    }))
    .pipe(gulp.dest('.tmp'))
    .pipe(reload({stream: true}));
});

gulp.task('images', () => {
  return gulp.src('app/images/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('activities', () => {
  return gulp.src('app/activities/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/activities'));
});

gulp.task('file', () => {
  return gulp.src('file/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/file'));
});

gulp.task('service', () => {
  return gulp.src('service/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/service'));
});

gulp.task('icons', () => {
  return gulp.src('app/icons/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/icons'));
});

gulp.task('img', () => {
  return gulp.src('app/img/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/img'));
});

gulp.task('resources', () => {
  return gulp.src('app/resources/**/*')
    .pipe($.cache($.imagemin()))
    .pipe(gulp.dest('dist/resources'));
});

gulp.task('fonts', () => {
  return gulp.src('./bower_components/glyphicons/fonts/**/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('fontsapp', () => {
  return gulp.src('app/fonts/**/*.{eot,svg,ttf,woff,woff2}')
    .pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('dist/fonts'));
});

gulp.task('extras', () => {
  return gulp.src([
    'app/*',
    '!app/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'));
});

gulp.task('clean', del.bind(null, ['.tmp', 'dist', 'distapp']));

// Similar al build -> Sirve 1! JS completo y uglificado
gulp.task('serve', () => {
  runSequence(['clean', 'wiredep'], ['css', 'htmlServe', 'styles', 'scripts', 'fonts', 'fontsapp'], () => {
    browserSync({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });
  });
});

// Similar al build -> Sirve 1! JS completo y uglificado
gulp.task('dev', () => {
  runSequence(['clean', 'wiredep'], ['css', 'htmlServe', 'styles', 'scripts', 'fonts', 'fontsapp'], () => {
    browserSync({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });
    gulp.watch('app/**/*.html', ['htmlServe']).on('change', reload);
    gulp.watch('app/**/*.css', ['htmlServe']).on('change', reload);
    gulp.watch('app/**/*.js', ['wiredep', 'scripts']).on('change', reload);
    gulp.watch('app/fonts/**/*', ['fonts']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', () => {
  browserSync({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['dist']
    }
  });
});

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('app/*.html')
    .pipe(wiredep({
      exclude: ['bootstrap.js'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('build', ['lint', 'css', 'html', 'images', 'icons', 'img', 'fonts', 'fontsapp', 'extras', 'file', 'service'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('buildnomin', ['lint', 'css', 'htmlnomin', 'images', 'icons', 'img', 'resources', 'fonts', 'fontsapp', 'extras', 'file', 'service'], () => {
  return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  runSequence(['clean', 'wiredep'], 'build');
});


gulp.task('debug', () => {
  runSequence(['clean', 'wiredep'], 'buildnomin');
});
